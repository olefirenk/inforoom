FROM mhart/alpine-node

RUN apk add --no-cache make gcc g++ python

RUN npm install -g yarn

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

ADD . .

RUN yarn install

EXPOSE 3000
CMD ["npm", "run", "production"]