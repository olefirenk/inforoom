import React from 'react'
import {head} from 'react-isomorphic-render'
import {preload} from 'react-isomorphic-render/redux'
import LoadingBar from 'react-redux-loading-bar'
import Header from '../App/Header'
import Footer from '../App/Footer'
import {StickyContainer, Sticky} from 'react-sticky'
import MiniHeader from '../App/Header/MiniHeader'

@preload(({dispatch, fetchData }) => (
    dispatch(fetchData('/rubrics', 'rubrics'))
))
export default class Main extends React.Component {

    render() {
        const title = 'Inforoom'
        // const description = 'A generic web application boilerplate'

        const meta = [
            // <meta charset='utf-8'/>
            {
                charset: 'utf-8'
            },

            // <meta name='...' content='...'/>
            {
                name: 'viewport',
                content: 'width=1575px'
            },

            // <meta property='...' content='...'/>
            {
                property: 'og:title',
                content: 'Inforoom'
            }, {
                property: 'og:description',
                content: 'Inforoom'
            }, {
                property: 'og:locale',
                content: 'ru-RU'
            }
        ]

        return (
            <div className='app'>
              <StickyContainer>
                <LoadingBar updateTime={50} maxProgress={95} progressIncrease={5} style={{
                        top: 0,
                        height: '2px'
                }}/> {head(title, meta)}
                <div className='content'>
                  <Header/>
                  <Sticky stickyClassName='miniheader-sticky-active' stickyStyle={{padding:'0'}} className='miniheader-sticky'>
                    <MiniHeader/>
                  </Sticky>
                  {this.props.children}
                </div>
                <Footer/>
              </StickyContainer>
            </div>
        )
    }
}
