import React from 'react'
import {Field, reduxForm} from 'redux-form'
import Social from '../Social'
import { EmailValidation , PasswordValidationRequeired} from 'utils/Validation/Validation'
import { normalizePassword } from 'utils/Normalize/Normalize'

const validate = values => {
    const errors = {}

    if (!PasswordValidationRequeired(values._password, 6, 100)) {
        errors._password = 'Длина пароля от 6 символов, без пробелов'
    }

    if (!EmailValidation.test(values._username)) {
        errors._username = 'Неправильный email'
    }

    return errors
}

const renderField = ({ input,type,meta: {touched,error,warning},classInput,placeholder }) => (
    <div className='registration-field'>
        <input {...input} className={classInput} placeholder={placeholder} type={type}/>
        {touched && ((error && <span className='registration--warning'>{error}</span>) || (warning && <span className='registration--warning'>{warning}</span>))}
    </div>
)

const Login = (props) => {

    const {error, submitting, handleSubmit, off, on} = props

    return (
        <div className='enter open'>
            <div onClick={() => {
                off('login'),
                off('shadowblock')
            }} className='icon__close'>
                <i className='sprite sprite-close'></i>
            </div>
            <p className='enter__head'>Войти на сайт</p>
            <p className='enter__text'>чтобы создавать трансляции, делать анонсы, оставлять комментарии и многое другое</p>
            <Social/>
            <div className='enter__line'>
                <span>Или</span>
            </div>
            <form className='enter__form' onSubmit={handleSubmit}>
                <Field component={renderField} type='email' name='_username' placeholder='Email' />
                <Field
                    component={renderField}
                    type='password'
                    name='_password'
                    placeholder='Введите пароль'
                    normalize={normalizePassword}
                />
                <div className='enter__remember'>
                    <Field component='input' type='checkbox' name='remember' id='remember'/>
                    <label htmlFor='remember'>Запомнить меня</label>
                    <span className='enter__link enter__remember'>Забыли пароль?</span>
                </div>
                {error && <p className='login-error'>{error}</p>}
                <button className='enter__button' type='submit' disabled={submitting}>
                    {submitting ?
                        <div className='loader-wrap'>
                            <div className='loader'></div>
                        </div>:
                        'Войти'
                    }
                </button>
                <p>Нет учетной записи?
                     <span className='enter__link' onClick={() => {
                        off('login'),
                        on('shadowblock'),
                        on('registrationfirst')
                    }}> Зарегистрируйте</span>
                </p>
            </form>
        </div>
    )
}

export default reduxForm({form: 'login', validate})(Login)
