import React, {Component, PropTypes} from 'react'
import queryString from 'query-string'
import config from '../../../../configuration'

const providers = {
    facebook: {
        url: 'https://www.facebook.com/v2.8/dialog/oauth',
        params:{
            client_id: '638765256285504',
            scope: 'email',
            display: 'popup',
        },
        check: `${config.api.baseUrl}/login/check-facebook`
    },
    google: {
        url: 'https://accounts.google.com/o/oauth2/v2/auth',
        params:{
            client_id: '192572615605-lf8bk6qjql8r5ba6346jpepoa2egudng.apps.googleusercontent.com',
            scope: 'profile email'
        },
        check: `${config.api.baseUrl}/login/check-google`
    }
}

export default class OAuth2 extends Component{
    static propTypes = {
        provider: PropTypes.string.isRequired,
        onSuccess: PropTypes.func.isRequired,
        onError: PropTypes.func.isRequired
    }

    openWindow = ({url, params}) => {
        const _params = {...params, response_type: 'code', redirect_uri: params.redirect_uri || `${document.location.origin}/`}
        let query = ''
        let i = 0
        for(let name in _params){
            query += `${i>0?'&':'?'}${name}=${_params[name]}`
            i++
        }
        const popup = window.open(`${url}${query}`,'Social Auth','scrollbars=no,toolbar=no,location=no,titlebar=no,directories=no,status=no,menubar=no,top=100,left=100,width=600,height=500');
        this.listenPopup({popup, _params})
    }

    listenPopup({popup, _params}){
        if (popup.closed) {
            console.log('canceled')
        } else {
            let code;

            try{
                code = popup.location.search;
            } catch (e) {
                //
            }

            if (code) {
                this.getToken({
                    code: queryString.parse(code).code,
                    redirect_uri: _params.redirect_uri
                })
                popup.close();
            } else {
                setTimeout(this.listenPopup.bind(this, {popup, _params}), 0);
            }
        }
    }

    async getToken({code, redirect_uri}){
        const {provider} = this.props
        try{
            const response = await fetch(`${providers[provider].check}?code=${code}&redirectUri=${redirect_uri}`, {
                method: 'GET',
                mode: 'cors'
            }).then(data=>data.json())
            this.props.onSuccess(response)
        }
        catch (e){
            this.props.onError(e)
        }
    }

    render(){
        const {provider} = this.props

        const login = () => {
            this.openWindow(providers[provider])
        }

        return(
            <button className={this.props.className} type='button' onClick={login}>
                {this.props.children}
            </button>
        )
    }
}
