import React, {Component} from 'react'
import OAuth2 from './OAuth2'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Auth } from 'react-security-fetcher'
import { getAccount } from 'actions/Security/Security'
import { offModal } from 'actions/App/modals.js'

@connect((state)=>({
    modals: state.modals
}), (dispatch)=>({
    actions: bindActionCreators({
        offModal,
        getAccount
    }
    , dispatch)
}))

export default class Social extends Component {
    render() {
        const { actions } = this.props
        const handleSuccess = ({token}) =>{
            Auth.setToken(token)
            actions.getAccount()
            actions.offModal('shadowblock')
            actions.offModal('login')
            actions.offModal('registrationfirst')
        }
        const handleError = (error) =>{
            console.log(error)
        }
        return(
            <div className='enter__social'>
                <OAuth2 className='enter__social__button enter__social__fb' provider='facebook' onSuccess={handleSuccess} onError={handleError}>
                    <i className='sprite sprite-fb2'></i>
                    <span>Вход через Facebook</span>
                </OAuth2>
                <OAuth2 className='enter__social__button enter__social__google' provider='google' onSuccess={handleSuccess} onError={handleError}>
                    <i className='sprite sprite-google2'></i>
                    <span>Вход через Google</span>
                </OAuth2>
            </div>
        )
    }
}
