import React, {Component} from 'react'
import moment from 'moment'
import noFoto from 'images/icon/registration.png'
import {BASE_URL} from 'constants/App/App'
import AnnonceEdit from 'components/Common/Annonce/Edit'

export default class Annonce extends Component {

    _AnnonceEditSuccess = () => {
        this.props.off(`AnnonceEdit${this.props.annonceForm.initial.id}`)
        this.props.off('shadowblock')
        this.props.actions.getAnnonceUser(this.props.idUser)
    }

    render() {
        const {actions, data, on, modals, off, rubrics ,type} = this.props

        return (
              <div className='myannonce' key={data.id}>
                  {modals[`AnnonceEdit${data.id}`]
                      ? <AnnonceEdit off={off} rubrics={rubrics} on={on} initialValues={{
                              ...data,
                              startsAt: moment(data.startsAt).format('DD/MM/YYYY hh:mm'),
                              streamUrl: data.streamUrl ? `/api/stream_urls/${data.streamUrl.id}`:null,
                              rubric: data.rubric ? `/api/rubrics/${data.rubric.id}`:null,
                              cover: data.cover ? `/api/images/${data.cover.id}`:null
                             }}
                             loadPhoto={actions.loadPhoto}
                             id={data.id}
                             onSubmit={actions.edit.bind(null, data.id)}
                             onSubmitSuccess={this._AnnonceEditSuccess}/>
                      : null}
                  <p className='time__day'>{moment(data.startsAt).locale('ru').format('DD MMMM YYYY в H:mm')}</p>
                  <i className='annonce-img'><img src={data.cover ? `${BASE_URL}${data.cover.path}`:noFoto} width='100%' height='82px'/>
                    {data.rubric ?
                        <p className='tag'>{data.rubric.name}</p>
                        :null
                    }
              </i>
                  <p className='about'>{data.title}</p>
                  <div className='myannonce__info'>
                      <p>
                          <i className='sprite sprite-annonce-subscribe'></i>
                          <span className='subscribe-count'>{data.countFollowing}</span>
                      </p>
                      <p>
                          <i className='sprite sprite-icon-comments'></i>
                          <span className='comments-count'>534</span>
                      </p>
                  </div>
                  {type === 'edit'
                    ? null
                    :  <button onClick={() => {
                          on('shadowblock'),
                          on(`AnnonceEdit${data.id}`)
                      }}>Редактировать</button>
                  }
              </div>
        )
    }
}
