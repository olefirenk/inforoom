import React, { Component } from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {onModal} from 'actions/App/modals'
import {Auth} from 'react-security-fetcher'

@connect((state) => ({
    modals: state.modals
}), (dispatch) => ({
    actionsModal: bindActionCreators({
        onModal
    }, dispatch),
    dispatch
}))

export default class LikedButton extends Component {

    static propTypes= {
            isLike: React.PropTypes.bool.isRequired,
            id: React.PropTypes.number.isRequired
    }

    constructor(props) {
        super(props)

        this.state = {
            isLike: props.isLike
        }
    }

    _likedbutton = async() => {
        try {
            const {actions, id} = this.props
            await actions.StreamSubscribe(id)
            this.setState({isLike: true})

        }
        catch (e) {
            console.log('error liked', new Error(e))
        }
    }

    _unlikedbutton = async() => {
        try {
            const {actions, id} = this.props
            await actions.StreamUnsubscribe(id)
            this.setState({isLike: false})
        }
        catch (e) {
            console.log('error liked', new Error(e))
        }
    }

    render() {
        const {isLike} = this.state
        const {actionsModal} =this.props

        return (
            <div className='button__liked'>
                {isLike
                ? <div
                    className='liked-button'
                    onClick={
                        Auth.isAuthenticated()
                        ? this._unlikedbutton
                        : () => {
                              actionsModal.onModal('shadowblock')
                              actionsModal.onModal('login')
                        }}>
                        <span>
                          <i className='sprite sprite-liked'></i>
                          <span>В избранном</span>
                        </span>

                    </div>
                : <div
                    className='unliked-button'
                    onClick={
                        Auth.isAuthenticated()
                        ? this._likedbutton
                        : () => {
                              actionsModal.onModal('shadowblock')
                              actionsModal.onModal('login')
                        }}>
                     <span>
                        <i className='sprite sprite-icon-star'></i>
                        <span>В избранное</span>
                    </span>
                  </div>
                }
            </div>
        )
    }
}
