import React from 'react'

export default class DeleteButton extends React.Component {

  static propTypes= {
            actions: React.PropTypes.func.isRequired,
            id: React.PropTypes.number.isRequired
  }

  constructor(props) {
      super(props)

      this.state = {
          isFetching: false
      }
  }

  componentWillUnMount = () => {
    this.setState({isFetching: false})
  }

  _unsubscribe = async() => {
     try {
         const {actions, id} = this.props
         this.setState({isFetching: true})
         await actions.StreamUnsubscribe(id)
         await actions.StreamLiked(id)
     }
     catch (e) {
         console.log('error unliked', new Error(e))
     }

  }
    render() {
        return (
            <button className='button-delete' onClick={this._unsubscribe}>
                {this.state.isFetching ?
                  <div className='loader-wrap'>
                      <div className='loader'>
                      </div>
                  </div>:
                  <span>
                     <i className='sprite sprite-unsubscribe'></i>
                     Удалить
                  </span>
                }
            </button>
        )
    }
}
