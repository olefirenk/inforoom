import React, {Component} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {onModal} from 'actions/App/modals'
import {Auth} from 'react-security-fetcher'

@connect((state) => ({
    modals: state.modals
}), (dispatch) => ({
    actionsModal: bindActionCreators({
        onModal
    }, dispatch),
    dispatch
}))

export default class WatchingButton extends Component {

    static propTypes= {
            isWatch: React.PropTypes.bool.isRequired,
            id: React.PropTypes.number.isRequired,
            count: React.PropTypes.number.isRequired
    }

    constructor(props) {
      super(props)

      this.state = {
          isWatch: props.isWatch,
          count: props.count,
      }
  }

    _watching = async () => {
        try {
            const {actions, id} = this.props
            await actions.watching(id)
            this.setState({isWatch: true, count: this.state.count + 1})
        }
        catch (e) {
            console.log('error watching', new Error(e))
        }
    }

    _unwatching = async () => {
        try {
            const {actions, id} = this.props
            await actions.unwatching(id)
            this.setState({isWatch: false, count: this.state.count - 1})
        }
        catch (e) {
            console.log('error watching', new Error(e))
        }
    }
    render() {
        const {isWatch, count} = this.state
        const {actionsModal} = this.props

        return (
          <div className='will-watch'>
              {isWatch
                ? <button
                    className='unwatching-button'
                    onClick={
                      Auth.isAuthenticated()
                      ? this._unwatching
                      : () => {
                          actionsModal.onModal('shadowblock')
                          actionsModal.onModal('login')
                    }}
                  >
                      <span>
                        <i className='sprite sprite-annonce-subscribe'></i>
                        Буду смотреть
                      </span>
                    </button>
                : <button
                    className='watching-button'
                    onClick={Auth.isAuthenticated()
                      ? this._watching
                      : () => {
                        actionsModal.onModal('shadowblock')
                        actionsModal.onModal('login')
                    }}
                  >
                    <span>
                      <i className='sprite sprite-watch2'></i>
                      Буду смотреть
                    </span>
                  </button>
              }
              <i className='sprite sprite-annonce-subscribe'></i>
              <span className='subscribe-count'>{count}</span>
          </div>
        )
    }
}
