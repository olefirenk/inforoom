import React, {Component} from 'react'

export default class MoreResults extends Component {

    constructor(props) {
        super(props)

        this.state = {
            isFetching: false
        }
    }

    _moreLoad = async () => {

        let {loadMore, params, keyData, dispatch, url} = this.props

        this.setState({ isFetching: true})
        const response = await loadMore(
            {url},
            {
                type: params.get('type'),
                page: params.get('page') + 1,
                limit: params.get('limit')
            },
            {query: params.get('search') || ''}
        )
        dispatch({
            type: '@FETCH_DATA/ADD_LIST',
            payload: {
                key: keyData,
                data: response,
                params : {...params.toObject(), page: params.get('page') + 1}
            }
        })
        this.setState({isFetching: false })
    }

    render() {
        const {isRender} = this.props
        return (
            isRender ?
                <div className='search__add__button'>
                    <button type='button' onClick={this._moreLoad}>
                        {this.state.isFetching ?
                            <div className='loader-wrap'>
                                <div className='loader'>
                                </div>
                            </div>:
                            'Еще результаты'
                        }
                    </button>
                </div>:
                null
        )
    }
}
