import React, {Component} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {onModal} from 'actions/App/modals'
import {Auth} from 'react-security-fetcher'

@connect((state) => ({
    modals: state.modals
}), (dispatch) => ({
    actionsModal: bindActionCreators({
        onModal
    }, dispatch),
    dispatch
}))

export default class Subscribe extends Component {

    static propTypes= {
            isSubscribe: React.PropTypes.bool.isRequired,
            id: React.PropTypes.number.isRequired
    }

    constructor(props) {
        super(props)
        this.state = {
            isSubscribe: props.isSubscribe,
            isFetching: false
        }
    }

    _subscribe = async () => {
        try {
            const {actions, id} = this.props
            this.setState({isFetching: true})
            await actions.subscribe(id)
            this.setState({isSubscribe: true, isFetching: false})
        }
        catch (e) {
            console.log('error subscribe', new Error(e))
        }

    }

    _unsubscribe = async () => {
        try {
            const {actions, id} = this.props
            this.setState({isFetching: true})
            await actions.unsubscribe(id)
            this.setState({isSubscribe: false, isFetching: false})
        }
        catch (e) {
            console.log('error unsubscribe', new Error(e))
        }
    }

    render() {

        const {isSubscribe} = this.state
        const {actionsModal} = this.props
        return (
            <div className='button__subscribe' >
                {isSubscribe
                 ?  <button
                        className='unsubscribe-button'
                        onClick={
                            Auth.isAuthenticated()
                            ? this._unsubscribe
                            : () => {
                                  actionsModal.onModal('shadowblock')
                                  actionsModal.onModal('login')
                            }}>
                     {this.state.isFetching ?
                         <div className='loader-wrap'>
                             <div className='loader'>
                             </div>
                         </div>:
                         <span>
                            <i className='sprite sprite-unsubscribe2'></i>
                            Подписан
                         </span>
                      }
                    </button>
                  : <button
                        className='subscribe-button'
                        onClick={
                            Auth.isAuthenticated()
                            ? this._subscribe
                            : () => {
                                  actionsModal.onModal('shadowblock')
                                  actionsModal.onModal('login')
                            }}>
                        {this.state.isFetching ?
                            <div className='loader-wrap'>
                                <div className='loader'>
                                </div>
                            </div>:
                            <span>
                                <i className='sprite sprite-icon-subscribe'></i>
                                Подписаться
                            </span>
                        }
                    </button>
                }
            </div>
        )
    }
}
