import React from 'react'
import moment from 'moment'
import TranslationEdit from 'components/Common/Translation/Edit'
import StreamPlayer from 'components/Common/StreamPlayer'

export default class Translation extends React.Component {

    _TranslationEditSuccess = () => {
        this.props.off(`TranslationEdit${this.props.data.id}`)
        this.props.off('shadowblock')
        this.props.actions.getStreamUser(this.props.idUser)
    }
    render() {
        const {data, actions, on, modals ,off , rubrics} = this.props
        return (
            <div className='mytranslation' key={data.id}>
              {modals[`TranslationEdit${data.id}`]
                ? <TranslationEdit
                  data={data}
                  off={off}
                  rubrics={rubrics}
                  on={on}
                  onSubmit={actions.edit.bind(null, data.id)}
                  onSubmitSuccess={this._TranslationEditSuccess}
                  initialValues={{
                        title: data.title,
                        description: data.description,
                        startsAt: '',
                        streamUrl: `/api/stream_urls/${data.streamUrl.id}`,
                        rubric: data.rubric ? `/api/rubrics/${data.rubric.id}`:null,
                        cover: data.cover ? `/api/images/${data.cover.id}`:null
                  }}/>
              : null}
              <StreamPlayer key={data.id} id={data.id} url={data.streamUrl} data={data} />
                <p className='about'>{data.title}</p>
                {data.rubric && <p className='tag'>{data.rubric.name}</p>}
                <div className='mytranslation__info'>
                    <p className='time__day'>{moment(data.startAt).locale('ru').format('DD MMMM YYYY в H:mm')}</p>
                    <p>
                        <i className='sprite sprite-icon-watch'></i>
                        <span className='watch'>{data.viewsCount}</span>
                        <i className='sprite sprite-icon-comments'></i>
                        <span className='comments'>734</span>
                    </p>
                </div>
                {/*<button onClick={() => {
                    on('shadowblock'),
                    on(`TranslationEdit${data.id}`)
                }}>Редактировать</button>*/}
            </div>
        )
    }
}
