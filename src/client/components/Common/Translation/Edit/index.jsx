import React from 'react'
import {Field, reduxForm} from 'redux-form'

const validate = values => {
    const errors = {}

    if (!((values.title.length !== 0) && (values.title.length > 4) && (values.title.length < 51))) {
        errors.title = 'Заголовок должен содержать 5-50 символов'
    }

    return errors
}

const renderField = ({ input, label, type, meta: { touched, error, warning }, classWrap, classInput, placeholder, id}) => (
  <div className={classWrap}>
    <label htmlFor={id}>{label}<span>*</span></label>
    <input {...input} className={classInput} placeholder={placeholder} type={type} id={id}/>
    {touched && ((error && <span className='streamedit--validate'>{error}</span>) || (warning && <span className='streamedit--validate'>{warning}</span>))}
  </div>
)

const TranslationEdit = (props) => {

    const {rubrics, error, submitting, handleSubmit, off, data} = props

    return (
        <div className='stream-edit'>
            <div className='icon__close' onClick={() => {
                off('shadowblock'),
                off(`TranslationEdit${data.id}`)
            }}>
                <i className='sprite sprite-close'></i>
            </div>
            <div className='stream-edit__head stream-live__head'>
                <i></i>
                <h2>
                    Редактирование трансляции
                </h2>
            </div>
            <div className='hotlive__form'>
                <form onSubmit={handleSubmit}>
                    <Field
                        component={renderField}
                        type='text'
                        placeholder=''
                        id='hotlive__form__header2'
                        name='title'
                        label='Заголовок'
                        classWrap='hotlive__form__header hotlive__form__block'
                        classInput=''
                    />
                    <div className='hotlive__form__rubric hotlive__form__block'>
                        <label htmlFor='hotlive__form__rubric'>
                            Рубрика<span>*</span>
                        </label>
                        <Field name='rubric' component='select' id='hotlive__form__rubric'>
                            {rubrics
                                ? rubrics.Items.map(rubric => {
                                    return <option value={`/api/rubrics/${rubric.id}`} key={rubric.id}>{rubric.name}</option>
                                })
                                : null
}
                        </Field>
                    </div>
                    <div className='hotlive__form__description hotlive__form__block'>
                        <label htmlFor='hotlive__form__header'>Описание</label>
                        <Field name='description' id='hotlive__form__header' component='textarea'/>
                    </div>
                    <p className='hotlive__form__required'>* Поля обязательные для редактирования</p>
                    <div className='hotlive__form__price'>
                        <div className='start-translation__button start-translation__button__live'>
                            {error && <p className='edit--error'>{error}</p>}
                            <button type='submit' disabled={submitting}>
                                {submitting ?
                                    <div className='loader-wrap'>
                                        <div className='loader'></div>
                                    </div>:
                                    'Сохранить'
                                }
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}
export default reduxForm({form: 'TranslationEdit', validate})(TranslationEdit)
