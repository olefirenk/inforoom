import React, { Component } from 'react'
import Subscribe from 'components/Common/Buttons/Subscribe'
import Translation from 'components/Common/User/StreamOther'
import noFoto from 'images/icon/registration.png'
import {BASE_URL} from 'constants/App/App.js'
import {Link} from 'react-router'

export default class UserSubscribe extends Component {
  render() {
    const {data, streams, actions} = this.props
    return (
      <div className='mysubscribe'>
        <div className='mysubscribe__channel'>
          <div className='mysubscribe__name__photo'>
            <img src={data.avatarPhoto
                ? `${BASE_URL}${data.avatarPhoto.path}`
            : noFoto}/>
          </div>
          <Link to={`id${data.id}`} className='mysubscribe__channel__name'>
            {data.firstname} {data.lastname}
          </Link>
          <div className='mysubscribe__channel__button'>
            <Subscribe isSubscribe={data.follow} id={data.id} actions={actions} />
          </div>
        </div>
        <div className='mysubscribe__video'>
          {streams
            ? streams.Items.map((stream, key) => {
              if (stream.createdBy.id === data.id)  {
                return (
                  <Translation
                    key = {key}
                    idUser = {data.id}
                    actions = {actions}
                    data = {stream}
                    type ='subscribe'
                  />)
              }
            })
                : null
          }
        </div>
      </div>
)
  }
}
