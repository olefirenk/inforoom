import React, {Component} from 'react'
import {Auth} from 'react-security-fetcher'

const render = (Component)=>class RenderComponent extends React.Component {
    constructor(){
        super();
        this.state = {
            mount: false
        }
    }
    componentDidMount = () =>{
        this.setState({mount: true})
    }
    render(){
        return(
            <div>
                {this.state.mount ? <Component {...this.props}/>: null}
            </div>
        )
    }
}

@render
export default class StreamWindow extends Component {

    render() {

        const {data} = this.props

        return (
            <div>
                {data ?
                    <div>
                        <p>{data.title}</p>
                        <object type='application/x-shockwave-flash' data={require('utils/rtmp/RtmpPublisher.swf')} width='550' height='400'>
                            <param name='movie' value={require('utils/rtmp/RtmpPublisher.swf')} />
                            <param name='FlashVars' value={`streamer=${data.streamUrl.baseUrl}&file=${data.streamUrl.url}?bearer=${Auth.getToken()}`} />
                        </object>
                    </div>
                    :
                    <p>Нет ссылки</p>
                }

            </div>

        )
    }
}
