import React from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import Hls from 'hls.js'
import {BASE_URL} from 'constants/App/App'
import {streamPlay, streamStop} from 'actions/Stream/streamPlay'
import ReactPlayer from 'react-player'
let hls = new Hls()

@connect(() => ({}), (dispatch) => ({
    actions: bindActionCreators({
        streamPlay,
        streamStop
    }, dispatch)
}))

export default class StreamPlayer extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            url: null,
            type: null,
            onLoad: false,
            play: false
        }
    }

    componentWillMount = () => {

        const {url} = this.props

        if (typeof url != 'undefined') {
            switch (url.type) {
                case 'mp4':
                    this.setState({
                        url: url.url,
                        type: 'video/mp4'
                    })
                    break
                case 'youtube':
                    this.setState({
                        url: url.url,
                        type: 'youtube'
                    })
                    break
            }
        }
    }

    componentWillUnmount = () => {
        if (Hls.isSupported()) {
            hls.destroy()
        }
    }

    _startStream = (streamId, url) => {
        if (Hls.isSupported()) {
          let video = document.getElementById(streamId)

          hls.loadSource(url)
          hls.attachMedia(video)
          this.setState({onLoad: true})
          hls.on(Hls.Events.MANIFEST_PARSED,function() {
              hls.startLoad()
          })
        }
    }

    _playStream = () => {
          hls.startLoad()
    }


    _onPlay = (click) => {
        const {id, url} = this.props
        const {type, onLoad} = this.state

        this.setState({play: true})
        if (type !== 'video/mp4' && type !== 'youtube') {
            if (onLoad) {
                this._playStream()
            } else {
                this._startStream(`stream${id}`, url.url)
            }
        }

        if (click === 'container') {
            let video = document.getElementById(`stream${id}`)
            video.play()
        }
        //
    }

    _onPause = () => {
        const {type} = this.state

        this.setState({play: false})
        if (type !== 'video/mp4' && type !== 'youtube') {
            hls.stopLoad()
        }
        // let video = document.getElementById(`stream${this.props.id}`)
        // video.pause()
    }



    render() {

        const {id, data} = this.props

        let poster = null
        if (data && data.cover) {
            poster = `${BASE_URL}${data.cover.path}`
        }

        return (
            this.state.type !== 'youtube' ?
            <div className='videoContainer' onClick={this._onPlay.bind(null, 'container')}>
                {!this.state.play && <div className='videoShadow'><span></span></div>}
                    <video
                        className='video__player'
                        controls
                        id={`stream${id}`}
                        poster={poster}
                        preload='none'
                        onPlay={this._onPlay}
                        onPause={this._onPause}
                    >
                        {this.state.type &&
                            <source
                             src={this.state.url}
                             type={this.state.type} />
                        }
                    </video>
            </div>:
            <ReactPlayer
                className='video__youtube'
                url={this.state.url}
                playing={false}
                controls
            />
        )
    }
}
