import React, {Component} from 'react'
import moment from 'moment'
import {BASE_URL} from 'constants/App/App.js'
import noFoto from 'images/icon/registration.png'
import {Link} from 'react-router'

import StreamPlayer from 'components/Common/StreamPlayer'
import DeleteButton from 'components/Common/Buttons/Delete'
import WatchComments from 'components/MiniComponents/WatchComments'
import LikedButton from 'components/Common/Buttons/Liked'

export default class Translation extends Component {

    shouldComponentUpdate = (nextProps) => {
        if (this.props.data != nextProps.data) {
            return true
        }
        return false
    }
    render() {
        const { data, actions, type, idUser} = this.props
        return (
          <div className='mytranslation-search'>
            <StreamPlayer key={data.id} id={data.id} url={data.streamUrl} data={data}/>
            <Link to={`/stream/${data.id}`}>
                <p className='about'>{data.title}</p>
            </Link>
            {data.rubric && <p className='tag'>{data.rubric.name}</p>}
            {data.createdBy ?
                <div className='chats-user'>
                    <div className='chats-user__photo'>
                        <img src={data.createdBy.avatarPhoto
                                    ? `${BASE_URL}${data.createdBy.avatarPhoto.path}`
                                    : noFoto}
                            width='41'
                            height='40'
                        />
                    </div>
                    <Link to={`/id${data.createdBy.id}`}  className='chats-user__info'>
                        <p>
                            <span className='info__name'>{data.createdBy.fullName}</span>
                        </p>
                        <p className='time'>
                            <span className='time__day'>{moment(data.startsAt).locale('ru').format('DD MMMM YYYY в H:mm')}</span>
                        </p>
                    </Link>
                </div>:
                null
            }
            <div className='mytranslation__info'>
                <div className='mytranslation__info__liked'>
                    <WatchComments key={data.id} data={data}/>
                    {(() => {
                        switch (type) {
                            case 'button-delete':
                              return <DeleteButton id={data.id} actions={actions}/>
                            default:
                              return <LikedButton isLike={data.follow} id={data.id} actions={actions} />
                            }
                    })()}
                </div>
            </div>
          </div>
        )
    }
}
