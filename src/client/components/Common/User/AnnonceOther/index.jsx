import React, {Component} from 'react'
import {BASE_URL} from 'constants/App/App.js'
import moment from 'moment'
import noFoto from 'images/icon/registration.png'
import {Link} from 'react-router'
import WatchingButton from 'components/Common/Buttons/WillWatching'

export default class Annonce extends Component {
    render() {
        const {data, actions } = this.props
        return (
            <div className='channel__annonce__main'>
                <Link to={`/annonce/${data.id}`}>

                    <p className='time__day'>
                        <i className='sprite sprite-annonce2'></i>
                        {moment(data.createdAt).locale('ru').format('DD MMMM YYYY в H:mm')}
                    </p>
                    <i className='annonce-img'><img src={data.cover
                    ? `${BASE_URL}${data.cover.path}`
                    : noFoto} width='100%' height='82px'/>
                    <p className='tag'>{data.rubric? data.rubric.name:null}</p>
                    </i>
                    <p className='about'>{data.title}</p>
                </Link>
                <div className='channel__annonce__info-wrap'>
                    {data.createdBy ?
                        <Link to={`/id${data.createdBy.id}`} className='annonce-user'>
                            <i><img src={data.createdBy.avatarPhoto
                                        ? `${BASE_URL}${data.createdBy.avatarPhoto.path}`
                                        : noFoto}/></i>
                            <p>{data.createdBy.fullName}</p>
                        </Link>:
                        null
                    }
                    <WatchingButton isWatch={data.follow} id={data.id} count={data.countFollowing} actions={actions} />
                </div>
            </div>
        )
    }
}
