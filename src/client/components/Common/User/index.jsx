import React, {Component} from 'react'
import {Link} from 'react-router'
import Subscribe from 'components/Common/Buttons/Subscribe'
import {BASE_URL} from 'constants/App/App'
import noFoto from 'images/icon/registration.png'
import moment from 'moment'

export default class User extends Component {

    render() {

        const { data, actions } = this.props

        return (
            <div className='search__channel' key={data.id}>
                <div className='search__channel__subscribe'>
                    <Link to={`id${data.id}`} className='subscribe__photo'>
                        <i><img src={data.avatarPhoto ? `${BASE_URL}${data.avatarPhoto.path}`:noFoto}/></i>
                    </Link>
                    <div className='subscribe__name'>
                        <Link to={`/id${data.id}`} className='author'>{data.firstname} {data.lastname}</Link>
                        {data.city ?
                            <p className='location'>
                                {data.city.cityName}, {data.city.country ? data.city.country.countryName: null}
                            </p>:
                            null
                        }
                    </div>
                    <Subscribe isSubscribe={data.follow}  id={data.id} actions={actions} />
                    <div className='subscribe__info'>
                        <p>
                            <i className='sprite sprite-annonce-subscribe'></i>
                            <span className='subscribe-count'>{data.countFollowerUsers}</span>
                        </p>
                        <p>
                            <i className='sprite sprite-watch'></i>
                            <span className='comments-count'>534</span>
                        </p>
                    </div>
                    <div className='subscribe__duration'>
                        <p>
                          На сайте c {moment(data.createdAt).locale('ru').format('LL')}<br/>({moment(data.createdAt, 'YYYYMMDD').locale('ru').fromNow(true)})
                        </p>
                    </div>
                </div>
            </div>
        )
    }
}
