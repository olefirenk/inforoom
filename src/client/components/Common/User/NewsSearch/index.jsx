import React, {Component} from 'react'
import {Link} from 'react-router'
import moment from 'moment'

import noFoto from 'images/icon/registration.png'
import {BASE_URL} from 'constants/App/App.js'
export default class NewsList extends Component {

    render() {
        const {data} = this.props
        return (
            <Link to={`/news/${data.id}`} className='search__news'>
              <div className='search__news__photo'>
                <i>
                  {data.cover
                    ?   <img src={data.cover
                                ? `${BASE_URL}${data.cover.path}`
                    : noFoto}/>
                                : null
                  }
                </i>
              </div>
              <div className='search__news__head'>
                <span className='time__day'>{moment(data.createdAt).locale('ru').format('DD MMMM YYYY в H:mm')}</span>
                <div className='news__head'>{data.title}</div>
                <div className='news__text' dangerouslySetInnerHTML={{__html: data.content}}></div>
              </div>
            </Link>
        )
    }
}
