import React,{Component} from 'react'
import moment from 'moment'
import {Link} from 'react-router'
import StreamPlayer from 'components/Common/StreamPlayer'
import LikedButton from 'components/Common/Buttons/Liked'

export default class Translation extends Component {

    render() {
        const { data, actions} = this.props
        return (
          <div className='mytranslation mytranslation-other'>
            <StreamPlayer key={data.id} id={data.id} url={data.streamUrl} data={data}/>
            <Link to={`/stream/${data.id}`}>
              <p className='about'>{data.title}</p>
            </Link>
            {data.rubric && <p className='tag'>{data.rubric.name}</p>}
            <div className='mytranslation__info'>
              <p className='time__day'>{moment(data.startsAt).locale('ru').format('DD MMMM YYYY в H:mm')}</p>
              <p>
                <i className='sprite sprite-icon-watch'></i>
                <span className='watch'>{data.viewsCount}</span>
                <i className='sprite sprite-icon-comments'></i>
                <span className='comments'>734</span>
              </p>
            </div>
            <LikedButton isLike={data.follow} id={data.id} actions={actions} />
          </div>
        )
      }
    }
