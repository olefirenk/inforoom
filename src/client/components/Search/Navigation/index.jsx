import React, {Component} from 'react'
import {Link} from 'react-router'

export default class SearchTranslation extends Component {

    render() {

        const {streams, news, users, annonces} = this.props
        let count = {
            streams: streams ? streams.totalItems: 0,
            news: news ? news.totalItems: 0,
            users: users ? users.totalItems: 0,
            annonces: annonces ? annonces.totalItems: 0
        }
      

        return (
            <div className='search__result'>
              <div className='search__result__list'>
                <ul>
                  <li>
                    <Link to='/search/translation' activeClassName='active'>Трансляции{streams ? <sup>{count.streams}</sup>:null}
                    </Link>
                  </li>
                  <li >
                    <Link to='/search/news' activeClassName='active'>Новости{news ? <sup>{count.news}</sup>:null}
                    </Link>
                  </li>
                  <li>
                    <Link to='/search/annonce' activeClassName='active'>Анонсы{annonces ? <sup>{count.annonces}</sup>:null}
                    </Link>
                  </li>
                  <li>
                    <Link to='/search/user' activeClassName='active'>Пользователи{users ? <sup>{count.users}</sup>:null}
                    </Link>
                  </li>
                </ul>
              </div>
            </div>
        )
    }
}
