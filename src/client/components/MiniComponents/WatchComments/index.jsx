import React, { Component } from 'react'
import {Link} from 'react-router'

export default class WatchComments extends Component {

    render() {
        const {data} = this.props
        return (
          <div className='watchcomments'>
            <p>
              <i className='sprite sprite-icon-watch'></i>
              <span className='watch'>{data.viewsCount}</span>
            </p>
            <p>
              <Link to={`/stream/${data.id}`}>
                <i className='sprite sprite-icon-comments'></i>
                <span className='comments'>{data.thread.countComments}</span>
              </Link>
            </p>
          </div>
        )
    }
}
