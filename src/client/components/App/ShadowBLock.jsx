import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {onModal, offModal, toggleModal} from '../../actions/App/modals'

@connect((state) => ({modals: state.modals}), (dispatch) => ({
    actions: bindActionCreators({
        onModal,
        toggleModal,
        offModal
    }, dispatch)
}))

export default class Header extends Component {

    _offShadow = () => {
        let modalName
        let {modals, actions} = this.props
        if (modals.registrationsecond)
         {null}
        else  if (modals.shadowblock) {
            for (modalName in modals) {
                if (modals[modalName]) {
                    modals[modalName] = !modals[modalName]
                }
            }
            actions.offModal('shadowblock')
        }


    }

    render() {

        return (
            <div onClick={this._offShadow.bind(null)} className='shadowblock'></div>
        )
    }
}
