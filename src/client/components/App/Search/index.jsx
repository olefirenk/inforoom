import React from 'react'

export default class Search extends React.Component {
    constructor() {
        super()
        this.state = {
            searchTop: '',
            IsSearch: false,
        }
        this._handleClick = this._handleClick.bind(this)
        this._goSearch = this._goSearch.bind(this)
        this._handleKeyPress = this._handleKeyPress.bind(this)
        this._toggleSearch = this._toggleSearch.bind(this)
    }
    _toggleSearch = () => {
        if (this.state.IsSearch) {
            if (this.state.searchTop) {
                this._goSearch()
            } else this.setState({IsSearch: false})
        } else {
            this.setState({IsSearch: true})
        }
    }

    _handleClick = (e) => {
        this.setState({searchTop: e.target.value})
    }

    _goSearch = () => {
        const {actions: {goto, change}} = this.props
        change('searchList', 'query', this.state.searchTop)
        this.setState({searchTop: ''})
        goto('/search/translation')
    }
    _handleKeyPress = (e) => {
        if (e.charCode == 13) {
            const {actions: {goto, change}} = this.props
            change('searchList', 'query', this.state.searchTop)
            this.setState({searchTop: ''})
            goto('/search/translation')
        }
    }
    render() {
        const {IsSearch} = this.state
        const {type} = this.props
        return (
            <div className='header2__search'>
                {!(type == 'MainHeader')
                    ? <input type='text' value={this.state.searchTop} onChange={this._handleClick} onKeyPress={this._handleKeyPress} />
                : (IsSearch)
                    ? <input type='text' className='open' value={this.state.searchTop} onChange={this._handleClick} onKeyPress={this._handleKeyPress} />
                    : null

}
                {type == 'MainHeader'
                    ? <i onClick={this._toggleSearch} className='sprite sprite-icon-search'></i>
                    : <i onClick={this._goSearch}>
                        <i  className='sprite sprite-icon-search2'></i>
                      </i>
}
            </div>
        )
    }
}
