import React from 'react'
import {Field, reduxForm} from 'redux-form'

const SearchList = (props) => {

    const {handleSubmit, submitting} = props

    return (
        <form className='search__input' onSubmit={handleSubmit}>
            <Field component='input' name='query' />
            <button type='submit'>
                {submitting ?
                    <div className='loader-wrapeer'>
                        <div className='loader'></div>
                    </div>:
                    <i className='sprite sprite-icon-search'></i>
                }

            </button>
        </form>
    )
}

export default reduxForm({form: 'searchList'})(SearchList)
