import React, {Component} from 'react'

export default class Checkbox extends Component {
    render() {
        const {
            type,
            input: {
                value,
                onChange
            }
        } = this.props

        return (
            <div className={type === 'create'
                ? 'registration__sex'
            : 'main__sex'}>
              <label>Пол{type === 'create'
                ? <i>*</i>
              : null}</label>
              <button type='button' className={value == true
                    ? 'active'
              : ''} onClick={() => onChange(true)}>Мужской</button>
              <button type='button' className={value == false
                    ? 'active'
                    : ''} onClick={() => onChange(false)}>Женский</button>
            </div>
        )
    }
}
