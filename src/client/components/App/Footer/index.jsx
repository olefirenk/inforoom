import React, {Component} from 'react'
import {Link} from 'react-router'

export default class Footer extends Component {

    render() {
        return (
            <div className='footer'>
              <div className='main-container'>
                <div className='footer-top'>
                  <div className='footer-top__info'>
                    <Link to=''>
                      <i className='sprite sprite-footer-rss'></i>RSS-ленты</Link>
                    {/*
                            <Link to=''>
                                <i className='sprite sprite-footer-mobile'></i>
                                Мобильная версия
                    </Link>*/}
                  </div>
                  <div className='footer-top__logo'>
                    <p><i className='sprite sprite-footer-logo'></i>
                    </p>
                    <p>&copy; 2015 Info Room. All rights reserved.</p>
                  </div>
                  <div className='footer-top__socials'>
                    <p>Мы в соцсетях</p>
                    <div className='social-button'>
                      <ul>
                        <li>
                          <Link to=''><i className='sprite sprite-fb'></i></Link>
                        </li>
                        <li>
                          <Link to=''><i className='sprite sprite-tw'></i></Link>
                        </li>
                        <li>
                          <Link to=''><i className='sprite sprite-vk'></i></Link>
                        </li>
                        <li>
                          <Link to=''><i className='sprite sprite-youtube'></i></Link>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div className='footer-bottom'>
                  <div className='footer-bottom__app'>
                    <button className='app-ios'>
                      <i className='sprite sprite-icon-apple2'></i>
                      <span>Доступно<br/>на <strong>App Store</strong>
                      </span>
                    </button>
                    <button className='app-android'>
                      <i className='sprite sprite-icon-android2'></i>
                      <span>Доступно<br/>на <strong>Google Play</strong>
                      </span>
                    </button>
                  </div>
                  <div className='footer-bottom__navigation footer-bottom__navigation1'>
                    <ul>
                      <li>
                        <Link to=''>Трансляции</Link>
                      </li>
                      <li>
                        <Link to=''>Новости</Link>
                      </li>
                      <li>
                        <Link to=''>Самые популярные</Link>
                      </li>
                      <li>
                        <Link to=''>Самые комментируемые</Link>
                      </li>
                      <li>
                        <Link to=''>Анонсы</Link>
                      </li>
                    </ul>
                  </div>
                  <div className='footer-bottom__navigation footer-bottom__navigation2'>
                    <ul>
                      <li>
                        <Link to='/rubrics/policy'>Политика</Link>
                      </li>
                      <li>
                        <Link to='/rubrics/economy'>Экономика</Link>
                      </li>
                      <li>
                        <Link to='/rubrics/finance'>Финансы</Link>
                      </li>
                      <li>
                        <Link to='/rubrics/markets'>Компании и рынки</Link>
                      </li>
                      <li>
                        <Link to='/rubrics/science'>Наука</Link>
                      </li>
                      <li>
                        <Link to='/rubrics/sport'>Спорт</Link>
                      </li>
                    </ul>
                  </div>
                  <div className='footer-bottom__navigation footer-bottom__navigation3'>
                    <ul>
                      <li>
                        <Link to=''>О нас</Link>
                      </li>
                      <li>
                        <Link to=''>Партнеры</Link>
                      </li>
                      <li>
                        <Link to=''>Контакты</Link>
                      </li>
                      <li>
                        <Link to=''>Реклама на сайте</Link>
                      </li>
                      <li>
                        <Link to=''>Политика конфиденциальности</Link>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
        )
    }
}
