import React, {Component} from 'react'
import AutoComplete from 'components/App/Form/Autocomplete'

export default class Location extends Component {
    constructor(props){
        super(props)
        this.state = {
            country: props.country||null,
            city: {cityName: props.cityName, id: props.id}||null
        }
    }

    render(){
        const {actions: {fetchData}, database} = this.props
        const countrySearch = (q) =>{
            fetchData('/countries', 'databaseCountries', 'GET', {params: {countryName: q}})
        }
        const citySearch = (q) => {
            fetchData('/cities', 'databaseCities', 'GET', {params: {cityName: q, country_id: this.state.country.id}})
        }

        return (
            <div className='public__location'>
                <AutoComplete
                    getItems={countrySearch}
                    items={database.countries.Items}
                    selectedItem={this.state.country}
                    name={'countryName'}
                    placeholder='Страна'
                    className='public__location__country'
                    id='location__country'
                    onSelect={(item)=>{
                        this.setState({country: item})
                        this.props.handleSave(null)
                    }}
                />
                {this.state.country ?
                    <AutoComplete
                        getItems={citySearch}
                        items={database.cities.Items}
                        selectedItem={this.state.city}
                        name={'cityName'}
                        placeholder='Город'
                        className='public__location__country public__location__city'
                        id='location__city'
                        onSelect={(item)=>{
                            this.setState({city: item})
                            this.props.handleSave(item)
                        }}
                    />
                    : null
                }
            </div>
        )
    }
}
