import React, {Component} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {redux as fetchData} from 'react-security-fetcher'
import LocationList from 'components/App/Form/Location/List'

@connect(state=>{
    const {databaseCities = {response: {Items: []}}, databaseCountries = {response: { Items: []}}} = state.fetchData
    return {
    database: {
        cities: databaseCities.response||{Items: []},
        countries: databaseCountries.response||{Items: []}
    }
}}, dispatch=>({
    actions: bindActionCreators({
        fetchData
    }, dispatch)
}))

export default class Location extends Component {

    constructor(props){
        super(props);
        this.state = {
            value: props.input.value
        }
    }

    render(){

        return(
            <LocationList
                actions={this.props.actions}
                database={this.props.database}
                handleSave={(item)=>{
                    this.setState({value:{item}})
                    let city
                    if (item) {
                        city = item
                    } else {
                        city = null
                    }
                    this.props.input.onChange(city)
                }}
                {...this.props.input.value}
            />
        )
    }
}
