import React from 'react'
import {Field} from 'redux-form'
import {normalizePhone} from 'utils/Normalize/Normalize'

const renderPhoneField = ({ input, type, meta: { touched, error, warning }, classWrap, classInput, placeholder, id}) => (
  <div className={classWrap}>
    <input {...input} className={classInput} placeholder={placeholder} type={type} id={id}/>
    {touched && ((error && <span className='phone--validate'>{error}</span>) || (warning && <span className='input--validate'>{warning}</span>))}
  </div>
)

export default class Phone extends React.Component{
    render(){
        const {fields} = this.props
        return (
            <div className='public__contact'>
              <label htmlFor='contact__number'>Контакты</label>
              {fields.map((item, key)=>(
                <div className='phone-wrap' key={key}>
                  <i className='sprite sprite-phone'></i>
                  <Field
                    component={renderPhoneField}
                    type='text'
                    placeholder='Телефон'
                    id='contact__number'
                    name={item}
                    className='contact__number'
                    id='contact__number'
                    normalize={normalizePhone}
                    classWrap='phone--wrap'
                  />
                  {key != 0 ?
                    <div className='remove__field' onClick={() => fields.remove(key)}>
                      <span>X</span>
                    </div>:
                            null
                  }
                </div>
              ))}
              <div className='add__field' onClick={() => fields.push(null)}>
                <i className='sprite sprite-icon-include'></i>
                <span>Добавить еще</span>
              </div>
            </div>
        )
    }
}
