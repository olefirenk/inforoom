import React from 'react'
import {Field} from 'redux-form'
import {normalizePhone} from 'utils/Normalize/Normalize'

const renderPhoneField = ({ input, type, meta: { touched, error, warning }, classWrap, classInput, placeholder, id}) => (
  <div className={classWrap}>
    <input {...input} className={classInput} placeholder={placeholder} type={type} id={id}/>
    {touched && ((error && <span className='input--validate'>{error}</span>) || (warning && <span className='input--validate'>{warning}</span>))}
  </div>
)

export default class Phone extends React.Component{
    render(){
        const {fields} = this.props
        return (
            <div className='public__socials'>
                <label htmlFor='social__network'>Социальные сети</label>
                {fields.map((item, key)=>(
                    <div key={key}>
                        <Field component='select' className='social__network' id='social__network' placeholder='Выберите соц. сеть' name='socials'>
                            <option value=''></option>
                            <option value=''></option>
                            <option value=''></option>
                        </Field>
                        <Field component='input' className='social__link' id='social__link' placeholder='Укажите ссылку на страницу' name='link'/>
                    </div>
                ))}
                <div className='add__field' onClick={() => fields.push(null)}>
                    <i className='sprite sprite-icon-include'></i>
                    <span>Добавить еще</span>
                </div>
            </div>
        )
    }
}
