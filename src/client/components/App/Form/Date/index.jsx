import InputElement from 'react-input-mask'

export const DateTimeField = ({ input, label, type, meta: { touched, error, warning }, classWrap, classInput, placeholder, id, key}) => (
    <div className={classWrap} key={key}>
      <label htmlFor={id}>{label}<span>*</span></label>
      <InputElement {...input} mask='99/99/9999 99:99' className={classInput} placeholder={placeholder} type={type} id={id} />
      {touched && ((error && <span className='stream--validate'>{error}</span>) || (warning && <span className='stream--validate'>{warning}</span>))}
    </div>
)
