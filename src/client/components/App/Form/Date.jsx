import React from 'react'
import InputElement from 'react-input-mask'

export const DateField = ({ input, label, type, meta: { touched, error, warning }, classWrap, classInput, placeholder, id}) => (
  <div className={classWrap}>
    <label htmlFor={id}>{label}</label>
    <InputElement {...input} mask='99/99/9999' className={classInput} placeholder={placeholder} type={type} id={id} />
    {touched && ((error && <span className='input--validate'>{error}</span>) || (warning && <span className='input--validate'>{warning}</span>))}
  </div>
)
