import React from 'react'
import Dropzone from 'react-dropzone'
import noFoto from 'images/icon/registration.png'
import {BASE_URL} from 'constants/App/App.js'

export default class AddPhoto extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            avatar: this.props.src
                ? {
                    path: `${BASE_URL}${this.props.src.path}`
                }
                : {
                    path: noFoto
                }
        }
    }

    uploadPhoto = (files) => {
        for (let i = 0; i < files.length; i++) {
            let data = new FormData()
            data.append('file', files[i])
            this.props.loadPhoto(data).then(data => {
                this.props.input.onChange(`/api/images/${data.id}`)
                this.setState({
                    avatar: {
                        path: `${BASE_URL}${data.path}`
                    }
                })
            })
        }
    }

    render() {

        return (
                <div className='add__photo'>
                    <div className='photo__img'>
                        <img src={this.state.avatar.path}/>
                    </div>
                    <Dropzone onDrop={this.uploadPhoto} style={{
                        width: '100%'
                    }}>
                        <div className='add__field'>
                            <i className='sprite sprite-icon-include'></i>
                            <span>Добавить фото</span>
                        </div>
                    </Dropzone>
                </div>

        )

    }
}
