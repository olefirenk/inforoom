import React, {Component} from 'react'
import {Link} from 'react-router'
import onClickOutside from 'react-onclickoutside-decorator'

export default class Menu extends Component {

    _handleClose = () => {
        this.props.off('burgerMenu')
        this.props.off('burgerMenu2')
    }

    render() {
      const {off} = this.props
        return (
                <BurgerMenu
                    off={off}
                    onClickOutside={this._handleClose}
                />
        )
    }
}

@onClickOutside
class BurgerMenu extends Component {
    render() {
      const {off} = this.props
        return (
            <div className='header__navigation open'>
              <div >
                <ul className='navigation'>
                  <li>
                    <Link to='/streams' onClick={() => off('burgerMenu')}>Трансляции</Link>
                  </li>
                  <li>
                    <Link to='/annonces' onClick={() => off('burgerMenu')}>Анонсы</Link>
                  </li>
                  <li>
                    <Link to='/news' onClick={() => off('burgerMenu')}>Новости</Link>
                  </li>
                  {/* <li>
                    <Link to='/users' onClick={() => off('burgerMenu')}>Пользователи</Link>
                  </li> */}
                  <li>
                    <Link to='/search/calendar' onClick={() => off('burgerMenu')}>Календарь</Link>
                  </li>
                  <li>
                    <Link to='' onClick={() => off('burgerMenu')}>О нас</Link>
                  </li>
                  <li>
                    <Link to='' onClick={() => off('burgerMenu')}>Контакты</Link>
                  </li>

                  <li>
                    <Link to='' onClick={() => off('burgerMenu')}>Реклама на сайте</Link>
                  </li>
                </ul>
                <div className='triangle'></div>
              </div>
            </div>
        )
    }
}
