import React, {Component} from 'react'
import {Link} from 'react-router'

export default class Breadcrumps extends Component {
    render() {
      const { rubrics } = this.props
        return (
            <div className='header-breadcrumps'>
              <div className='main-container'>
                <ul>
                  {rubrics && rubrics.Items.length > 0 && rubrics.Items.map(item =>
                    <li key={item.id}>
                      <Link
                          to={`/rubrics/${item.id}`}
                          activeClassName='active'
                          onlyActiveOnIndex={true}
                         >
                             {item.name}
                     </Link>
                    </li>
                  )
                  }
                </ul>
              </div>
            </div>
        )
    }
}
