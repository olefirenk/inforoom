import React, {Component} from 'react'
import {Link} from 'react-router'
import Search from '../../App/Search'
import noFoto from 'images/icon/registration.png'
import ModalApproved from '../../Stream/Modals/Approved'
import CreateStream from 'pages/Stream/Create'
import BurgerMenu from './BurgerMenu'
import LoginProfile from './LoginProfile'
import Breadcrumps from './Breadcrumps'
import Registration from '../../User/Form/RegistrationFirst'
import RegistrationSecond from '../../User/Form/RegistrationSecond'
import ModalLogin from '../../Security/Form/Login'
import Contract from '../../User/Modals/Contract'
import {BASE_URL} from 'constants/App/App.js'
import ShadowBLock from '../../App/ShadowBLock'
import ModalStreamLive from 'components/Stream/HotLive/Live'
import Scroll from 'react-scroll'

export default class HeaderOther extends Component {

    _handleSubmitSuccessLogin = () => {
        this.props.actions.offModal('shadowblock')
        this.props.actions.offModal('login')
    }

    _handleSubmitSuccessRegFirst = () => {
        this.props.actions.offModal('registrationfirst')
        this.props.actions.onModal('shadowblock')
        this.props.actions.onModal('registrationsecond')
    }

    _handleSubmitSuccessRegSecond = () => {
        this.props.actions.offModal('shadowblock')
        this.props.actions.offModal('registrationsecond')
    }

    render() {
        const {
            user,
            actions,
            actions: {
                login,
                register,
                edit
            },
            modals,
            location,
            rubrics
        } = this.props
        const Element = Scroll.Element
        return (
            <div className='header2'>
                <Element name='ScrolltoID'></Element>
                {modals.shadowblock
                    ? <ShadowBLock/>
                    : null}
                {modals.registrationfirst
                    ? <Registration on={actions.onModal} off={actions.offModal} onSubmit={register} onSubmitSuccess={this._handleSubmitSuccessRegFirst}/>
                    : null
}
                {modals.registrationsecond
                    ? <RegistrationSecond
                        actions={actions}
                        on={actions.onModal}
                        off={actions.offModal}
                        toggle={actions.toggleModal}
                        modals={modals}
                        initialValues={{
                            firstname: '',
                            lastname: '',
                            sex: true
                        }}
                        onSubmit={edit.bind(null, user.id)}
                        onSubmitSuccess={this._handleSubmitSuccessRegSecond}
                    />
                    : null
}
                {modals.login
                    ? <ModalLogin on={actions.onModal} off={actions.offModal} open={modals.login} onSubmit={login} onSubmitSuccess={this._handleSubmitSuccessLogin}/>
                    : null
}
                {modals.stream
                    ? <CreateStream off={actions.offModal}/>
                    : null
}
                {modals.streamApproved
                    ? <ModalApproved off={actions.offModal} open={modals.stream}/>
                    : null
}
                {modals.streamLive
                    ? <ModalStreamLive on={actions.onModal} off={actions.offModal}/>
                    : null
                }
                {modals.contract
                    ? <Contract on={actions.onModal} off={actions.offModal}/>
                    : null
}
                <div className='header2-wrap header-container'>
                    {modals.burgerMenu
                        ? <BurgerMenu off={actions.offModal}/>
                        : null
}
                    <div className='header2__burger' onClick={() => actions.toggleModal(modals.burgerMenu, 'burgerMenu')}>
                        <i className='sprite sprite-burger-menu2'></i>
                    </div>
                    <div className='header2__logo'>
                        <Link to='/'>
                            <i className='sprite sprite-header2-logo'></i>
                        </Link>
                    </div>
                    <Search type='otherHeader' actions={actions}/>
                    <div className='header2__translation'>
                        <button onClick={() => {
                            actions.onModal('stream'),
                            actions.onModal('shadowblock')
                        }}>
                            <i className='sprite sprite-header-translation'></i>Создать трансляцию
                        </button>
                    </div>
                    {!(user)
                        ? <div className='header2__login'>
                                <button onClick={() => {
                                    actions.onModal('shadowblock'),
                                    actions.onModal('login')
                                }}>Войти</button>
                            </div>
                        : <div className='header2__authorized'>
                            <div className='header2__authorized-wrap' onClick={() => actions.toggleModal(modals.loginProfile, 'loginProfile')}>
                                <div className='header2__authorized__photo'>
                                    <img src={user.avatarPhoto
                                        ? `${BASE_URL}${user.avatarPhoto.path}`
                                        : noFoto}/>
                                </div>
                                <div className='header2__authorized__login'>{user.firstname} {user.lastname}</div>
                            </div>
                            {modals.loginProfile
                                ? <LoginProfile actions={actions} modals={modals}/>
                                : null
                            }
                        </div>
}
                    <div className='header2__language'>
                        <div className='language'>Рус<i className='triangle'></i>
                        </div>
                    </div>
                </div>
                {((location.pathname.indexOf('/user') == 0) || (location.pathname.indexOf('/id') == 0))
                    ? null
                    : <Breadcrumps rubrics={rubrics} />
}
            </div>
        )
    }
}
