import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import HeaderMain from './main'
import HeaderOther from './other'
import {loadPhoto} from 'actions/Photos/loadPhoto'
import {goto} from 'react-isomorphic-render/redux'
import {onModal, offModal, toggleModal} from 'actions/App/modals'
import {login, register, logout} from 'actions/Security/Security'
import {edit} from 'actions/User/edit'
import { change } from 'redux-form'
@connect((state) => (
  {
    modals: state.modals,
    user: state.authentication.user,
    location: state.router.location,
    rubrics: state.fetchData.rubrics ? state.fetchData.rubrics.response : {Items:[]}
}), (dispatch) => ({
    actions: bindActionCreators({
        register,
        edit,
        login,
        logout,
        onModal,
        offModal,
        toggleModal,
        loadPhoto,
        goto,
        change
    }, dispatch)
}))

export default class Header extends Component {
    render() {
        const {modals, location, user, rubrics, actions} = this.props

        return (
            <div>
              {((!user) && (location.pathname === '/'))
                ? <HeaderMain
                    modals={modals}
                    user={user}
                    actions={actions}
                    location={location}
                    rubrics={rubrics}
                  />
                : <HeaderOther
                        modals={modals}
                        user={user}
                        actions={actions}
                        location={location}
                        rubrics={rubrics}
                    />
}
            </div>
        )
    }
}
