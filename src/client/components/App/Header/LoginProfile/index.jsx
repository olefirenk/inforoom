import React, {Component} from 'react'
import {Link} from 'react-router'
import Pay from 'components/User/Modals/Pay'
import onClickOutside from 'react-onclickoutside-decorator'

export default class Menu extends Component {

    _handleClose = () => {
        this.props.actions.offModal('loginProfile')
        this.props.actions.offModal('loginProfile2')
    }

    render() {
      const {modals, actions } = this.props
        return (
                <LoginProfile
                    actions={actions}
                    modals={modals}
                    onClickOutside={this._handleClose}    
                />
        )
    }
}

@onClickOutside
class LoginProfile extends Component {
    render() {
        const {modals, actions} = this.props
        return (
            <div className='header2__authorized__login-profile'>
                {modals.pay
                    ? <Pay off={actions.offModal}/>
                    : null
                }
                <div className='triangle'></div>
                <div className='login-profile__balance'>
                    <div className='balance__head'>Баланс</div>
                    <div className='balance__main'>
                        <span className='summ'>200</span>
                        UAH
                        <button onClick={() => {
                            actions.onModal('shadowblock'),
                            actions.onModal('pay')
                        }}>Пополнить</button>
                    </div>
                </div>
                <div className='login-profile__link'>
                    <ul>

                        <li>
                            <Link to='/user/profile' activeClassName='active' onClick={()=>actions.offModal('loginProfile')}>
                                <i className='sprite sprite-profile2'></i>
                                Мой профиль
                            </Link>
                        </li>
                        <li>
                            <Link to='/user/annonce' activeClassName='active' onClick={()=>actions.offModal('loginProfile')}>
                                <i className='sprite sprite-annonce'></i>
                                Мои анонсы
                            </Link>
                        </li>
                        <li>
                            <Link to='/user/translation' activeClassName='active' onClick={()=>actions.offModal('loginProfile')}>
                                <i className='sprite sprite-translation'></i>
                                Мои трансляции
                            </Link>
                        </li>
                        <li>
                            <Link to='/user/watch' activeClassName='active' onClick={()=>actions.offModal('loginProfile')}>
                                <i className='sprite sprite-watch'></i>
                                Буду смотреть
                            </Link>
                        </li>
                        <li>
                            <Link to='/user/subscribe' activeClassName='active' onClick={()=>actions.offModal('loginProfile')}>
                                <i className='sprite sprite-subscribe'></i>
                                Мои подписки
                            </Link>
                        </li>
                        <li>
                            <Link to='/user/liked' activeClassName='active' onClick={()=>actions.offModal('loginProfile')}>
                                <i className='sprite sprite-liked'></i>
                                Избранное
                            </Link>
                        </li>
                    </ul>
                </div>
                <div className='login-profile__logout'>
                    <button onClick={() => {
                        actions.logout(),
                        actions.offModal('loginProfile'),
                        actions.goto('/')
                    }}>Выйти</button>
                </div>
            </div>
        )
    }
}
