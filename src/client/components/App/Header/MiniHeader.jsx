import React, {Component} from 'react'
import {Link} from 'react-router'
import Search from '../../App/Search'
import noFoto from 'core/assets/images/icon/registration.png'
import {onModal, offModal, toggleModal} from 'actions/App/modals'
import {logout} from 'actions/Security/Security'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {loadPhoto} from 'actions/Photos/loadPhoto'
import BurgerMenu from './BurgerMenu'
import LoginProfile2 from './LoginProfile'
import {login, register} from 'actions/Security/Security'
import {edit} from 'actions/User/edit'
import {BASE_URL} from 'constants/App/App.js'
import ShadowBLock from '../../App/ShadowBLock'
import { change } from 'redux-form'
import {goto} from 'react-isomorphic-render/redux'

@connect((state) => ({modals: state.modals, user: state.authentication.user, location: state.router.location}), (dispatch) => ({
    actions: bindActionCreators({
        register,
        edit,
        login,
        logout,
        onModal,
        offModal,
        toggleModal,
        change,
        goto,
        loadPhoto
    }, dispatch)
}))

export default class HeaderOther extends Component {

    render() {
        const {user, actions, modals} = this.props
        return (
            <div className='header2 miniheader'>
                {modals.shadowblock
                    ? <ShadowBLock/>
                    : null}
                <div className='header2-wrap miniheader-wrap header-container'>
                    {modals.burgerMenu2
                        ? <BurgerMenu off={actions.offModal}/>
                        : null
}
                    <div className='header2__burger' onClick={() => actions.toggleModal(modals.burgerMenu2, 'burgerMenu2')}>
                        <i className='sprite sprite-burger-menu2'></i>
                    </div>
                    <div className='header2__logo miniheader__logo'>
                        <Link to='/'>
                            <i className='sprite sprite-miniheader-logo'></i>
                        </Link>
                    </div>
                    <Search type='otherHeader' actions={actions}/>
                    <div className='header2__translation'>
                        <button onClick={user
                            ? () => {
                                actions.onModal('shadowblock'),
                                actions.onModal('stream')
                            }
                            : () => {
                                actions.onModal('shadowblock'),
                                actions.onModal('login')
                            }}>
                            <i className='sprite sprite-header-translation'></i>Создать трансляцию
                        </button>
                    </div>
                    {!(user)
                        ? <div className='header2__login'>
                                <button onClick={() => {
                                    actions.onModal('shadowblock'),
                                    actions.onModal('login')
                                }}>Войти</button>
                            </div>
                        : <div className='header2__authorized'>
                            <div className='header2__authorized-wrap' onClick={() => actions.toggleModal(modals.loginProfile2, 'loginProfile2')}>
                                <div className='header2__authorized__photo'>
                                    <img src={user.avatarPhoto
                                        ? `${BASE_URL}${user.avatarPhoto.path}`
                                        : noFoto}/>
                                </div>
                                <div className='header2__authorized__login'>{user.firstname} {user.lastname}</div>
                            </div>
                            {modals.loginProfile2
                                ? <LoginProfile2 actions={actions} modals={modals} />
                                : null
}
                        </div>
}
                    <div className='header2__language'>
                        <div className='language'>Рус<i className='triangle'></i>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
