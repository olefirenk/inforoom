import React, {Component} from 'react'
import BurgerMenu from './BurgerMenu'
import Search from '../Search'
import ModalLogin from '../../Security/Form/Login'
import Registration from '../../User/Form/RegistrationFirst'
import RegistrationSecond from '../../User/Form/RegistrationSecond'
import Contract from '../../User/Modals/Contract'
import Breadcrumps from './Breadcrumps'
import ShadowBLock from '../../App/ShadowBLock'
import Scroll from 'react-scroll'

export default class HeaderMain extends Component {
    _handleSubmitSuccessLogin = () => {
        this.props.actions.offModal('shadowblock')
        this.props.actions.offModal('login')
    }

    _handleSubmitSuccessRegFirst = () => {
        this.props.actions.offModal('registrationfirst')
        this.props.actions.onModal('shadowblock')
        this.props.actions.onModal('registrationsecond')
    }

    _handleSubmitSuccessRegSecond = () => {
        this.props.actions.offModal('shadowblock')
        this.props.actions.offModal('registrationsecond')
    }

    render() {

        const {
            rubrics,
            user,
            actions,
            modals,
            actions: {
                login,
                register,
                edit
            }
        } = this.props
        const Element = Scroll.Element
        return (
            <div>
              {modals.shadowblock && <ShadowBLock/>}
              {modals.login
                ? <ModalLogin
                  on={actions.onModal}
                  off={actions.offModal}
                  open={modals.login}
                  onSubmit={login}
                  onSubmitSuccess={this._handleSubmitSuccessLogin}
                  initialValues={{_password: ''}}
                  />
                    : null
              }
              {modals.registrationfirst
                ? <Registration
                  on={actions.onModal}
                  off={actions.offModal}
                  onSubmit={register}
                  onSubmitSuccess={this._handleSubmitSuccessRegFirst}
                  />
                    : null
              }
              {modals.registrationsecond
                ? <RegistrationSecond
                  actions={actions}
                  on={actions.onModal}
                  off={actions.offModal}
                  modals={modals}
                  toggle={actions.toggleModal}
                  initialValues={{
                            firstname: '',
                            lastname: '',
                            sex: true
                  }}
                  onSubmit={edit.bind(null, user.id)}
                  onSubmitSuccess={this._handleSubmitSuccessRegSecond}
                  />
                    : null
              }
              {modals.contract
                ? <Contract on={actions.onModal} off={actions.offModal}/>
                    : null
              }
              <div className='header'>
                <Element name='ScrolltoID'></Element>
                <div className='header-container'>
                  <div className='header__top'>
                    <div className='header__top__column1'>
                      <i onClick={() => actions.toggleModal(modals.burgerMenu, 'burgerMenu')} className='sprite sprite-burger-menu'></i>
                      {modals.burgerMenu
                        ? <BurgerMenu off={actions.offModal} />
                                    : null
                      }
                      <button className='app-ios'>
                        <i className='sprite sprite-icon-apple'></i>
                        <span>Доступно<br/>на App Store</span>
                      </button>
                      <button className='app-android'>
                        <i className='sprite sprite-icon-android'></i>
                        <span>Доступно<br/>на Google Play</span>
                      </button>
                    </div>
                    <div className='header__top__column2'>
                      <Search type='MainHeader' actions={actions}/>
                      <button type='button' onClick={() => {
                        actions.onModal('shadowblock'),
                        actions.onModal('login')
                      }}>Войти</button>
                      <div className='language'>Рус<i className='triangle'></i>
                      </div>
                    </div>
                  </div>
                  <div className='header__main-logo'>
                    <i className='sprite sprite-header-logo'></i>
                  </div>
                  <div className='header__translation'>
                    <button onClick={user
                      ? () => actions.onModal('stream')
                      : () => {
                        actions.onModal('shadowblock'),
                        actions.onModal('login')
                      }}>
                      <i className='sprite sprite-header-translation'></i>Создать трансляцию
                    </button>
                  </div>
                </div>
              </div>
              <Breadcrumps rubrics={rubrics}/>
            </div>
        )
    }
}
