import React, {Component} from 'react'
import {Link} from 'react-router'
import moment from 'moment'
import {bindActionCreators} from 'redux'
import {preload} from 'react-isomorphic-render/redux'
import {connect} from 'react-redux'
import  {getNewsByRubric} from '../../../actions/News/getNewsByRubric'

// @preload(getNewsByRubric())
//
// @connect(state => ({
//     news: state.fetchData.newsByRubric && state.fetchData.newsByRubric.response,
// }), (dispatch) => ({
//     actions: bindActionCreators({
//     }, dispatch)
// }))
export default class RubricsNews extends Component {

    render() {

        const {news, type} = this.props

        return (
            <div className='news'>
              {type === 'no-title'?
                null
                  : <h2>Новости</h2>
              }
              <div className='news-wrap'>
                <div className='news__top-wrap'>
                  <div className='news__head'>Последние публикации</div>
                  { news ?
                    news.Items.map((data, key) => {
                      return (
                        <div key={key} className='news__next-wrap'>
                          <div className='news__next'>
                            <Link to={`/news/${data.id}`}>
                              <span className='news__next__time'>
                                <span className='time__day'>{moment(data.createdAt).locale('ru').format('DD MMMM YYYY в H:mm')}</span>
                                <i className='sprite sprite-news-video news-video'></i>
                              </span>
                              <p>{data.title}</p>
                            </Link>
                          </div>
                        </div>
                      )}): null
                  }
                  <div className='news__button-all'>
                    <Link to=''>Все новости</Link>
                  </div>
                </div>
              </div>
            </div>
        )
    }
}
