import React, {Component} from 'react'
import {Link} from 'react-router'
import StreamPlayer from 'components/Common/StreamPlayer'
export default class Popular extends Component {
    render() {
        const {streams} =this.props
        let data = streams.Items.slice(0,3)
        return (
            <div className='popular'>
              <h2>
                <i className='sprite sprite-icon-watch'></i>
                Самые популярные
              </h2>
              <div className='popular-wrap'>
                <div className='populars'>
                  {data
                    ?   data.map((item, key) => {
                      return (
                        <div key={key} className='popular'>
                          <div className='popular-video'>
                            <StreamPlayer key={item.id} id={item.id} url={item.streamUrl} data={item}/>
                          </div>
                          <div className='popular-describe'>
                            <Link to={`stream/${item.id}`} className='about'>{item.title}</Link>
                            <p>
                              <Link to={`/id${item.createdBy.id}`}>
                                <span className='author'>{item.createdBy.fullName}</span>
                              </Link>
                              <span className='watch'>
                                <i className='sprite sprite-icon-watch'></i>
                                {item.viewsCount}
                              </span>
                            </p>
                          </div>
                        </div>
                      )
                    })
                        :   null
                  }
                </div>
              </div>
            </div>
        )
    }
}
