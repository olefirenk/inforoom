import React, {Component} from 'react'
import {Link} from 'react-router'
import noFoto from 'images/icon/registration.png'
import {BASE_URL} from 'constants/App/App.js'
import moment from 'moment'

export default class Announcements extends Component {

    render() {
        const {annonces ,type} = this.props
        let annoncesLast = annonces.Items.slice(0,3)
        return (
            <div className='announcements'>
              { type === 'annonce-head'
                    ? null
                      : <h2>Анонсы</h2>
              }
              <div className='announcements-wrap'>
                <div className='announcements'>
                  {annoncesLast ?
                    annoncesLast.map((annonce, key) => {
                      return (
                        <div key={key} className='announcement'>
                          <Link to={`/annonce/${annonce.id}`}>
                            <div className='announcement-photo'>
                              <img src={annonce.createdBy.avatarPhoto
                                                    ? `${BASE_URL}${annonce.createdBy.avatarPhoto.path}`
                              : noFoto}/>
                              <span className='tag'>{annonce.rubric? annonce.rubric.name:null}</span>
                            </div>
                            <p className='about'>{annonce.title}</p>
                          </Link>
                          <div className='announcement-describe'>
                            <p className='time__day'>{moment(annonce.startsAt).locale('ru').format('DD MMMM YYYY в H:mm')}</p>
                            <Link to={`/id${annonce.createdBy.id}`}>
                              <p className='author'>{annonce.createdBy.fullName}</p>
                            </Link>
                          </div>

                        </div>
                      )}): null
                  }

                </div>
                <div className='announcementAll'>
                  <Link to='/search/annonce'>Все анонсы</Link>
                </div>
                </div>
            </div>
        )
    }
}
