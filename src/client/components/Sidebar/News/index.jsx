import React, {Component} from 'react'
import {Link} from 'react-router'
import moment from 'moment'

export default class News extends Component {

    render() {

        const {news, type} = this.props

        return (
            <div className='news'>
              {type === 'no-title'?
                null
                  : <h2>Новости</h2>
              }
              <div className='news-wrap'>
                <div className='news__top-wrap'>
                  <div className='news__head'>ТОП-темы</div>
                  <div className='news__top'>
                    <Link to=''>
                      <div className='news__top__pagination'>
                        <button className='news__button button-prev'>
                          <i className='sprite sprite-icon-prev-news'></i>
                        </button>
                        <button className='news__button button-next'>
                          <i className='sprite sprite-icon-next-news'></i>
                        </button>
                      </div>
                      <img src={require('images/news-img.jpg')}/>
                      <div className='news__top__description'>
                        <p>Байден: «Росія намагається підірвати суверенітет України у будь-який спосіб»</p>
                      </div>
                    </Link>
                  </div>
                  <div className='news__head'>Последние публикации</div>
                  { news ?
                    news.Items.map((data, key) => {
                      return (
                        <div key={key} className='news__next-wrap'>
                          <div className='news__next'>
                            <Link to={`/news/${data.id}`}>
                              <span className='news__next__time'>
                                <span className='time__day'>{moment(data.createdAt).locale('ru').format('DD MMMM YYYY в H:mm')}</span>
                                <i className='sprite sprite-news-video news-video'></i>
                              </span>
                              <p>{data.title}</p>
                            </Link>
                          </div>
                        </div>
                      )}): null
                  }
                  <div className='news__button-all'>
                    <Link to=''>Все новости</Link>
                  </div>
                </div>
              </div>
            </div>
        )
    }
}
