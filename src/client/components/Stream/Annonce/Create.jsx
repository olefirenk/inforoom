import React from 'react'
import {Field, reduxForm} from 'redux-form'
import AddPhoto from 'components/App/Photos/AddPhoto'
import InputElement from 'react-input-mask'
import moment from 'moment'

const validate = values => {
    const errors = {}

    if (!((values.title.length !== 0) && (values.title.length > 4) && (values.title.length < 51))) {
        errors.title = 'Заголовок должен содержать 5-50 символов'
    }

    let Date = moment(values.startsAt, 'DD/MM/YYYY hh:mm')
    let DateValid = !(values.startsAt.search(/_/g) != -1) && (values.startsAt.length != 0)
    let DateMinValid = moment().add(1, 'hours').valueOf()
    let DateMaxValid = moment().add(3, 'days').valueOf()

    let DateDurationValid = (Date.valueOf() > DateMinValid) && (Date.valueOf() < DateMaxValid)

    if (!DateValid) {
        errors.startsAt = 'Формат День/Месяц/Год 00:00'
    } else {
        if ((values.startsAt !== '') && (!DateDurationValid)) {
            errors.startsAt = 'Анонсировать можно максимум за три дня и минимум за 1 час до события'
        }
    }

    return errors
}

const renderField = ({ input, label, type, meta: { touched, error, warning }, classWrap, classInput, placeholder, id}) => (
  <div className={classWrap}>
    <label htmlFor={id}>{label}<span>*</span></label>
    <input {...input} className={classInput} placeholder={placeholder} type={type} id={id}/>
    {touched && ((error && <span className='stream--validate'>{error}</span>) || (warning && <span className='stream--validate'>{warning}</span>))}
  </div>
)

const renderDateTimeField = ({ input, label, type, meta: { touched, error, warning }, classWrap, classInput, placeholder, id}) => (
    <div className={classWrap}>
      <label htmlFor={id}>{label}<span>*</span></label>
      <InputElement {...input} mask='99/99/9999 99:99' className={classInput} placeholder={placeholder} type={type} id={id} />
      {touched && ((error && <span className='stream--validate'>{error}</span>) || (warning && <span className='stream--validate'>{warning}</span>))}
    </div>
)

const StreamAnnonce = (props) => {

    const {rubrics, error, handleSubmit, loadPhoto, submitting} = props

    return (
        <div className='start-translation__annonce'>
          <div className='start-translation__annonce__head'>
            <i className='sprite sprite-annonce-translation'></i>
            <h2>
              АНОНСИРУЕМАЯ<br/>
              ТРАНСЛЯЦИЯ
            </h2>
          </div>
          <p className='start-translation__text'>Анонсировать трансляцию можно максимум<br/>
            за три дня и минимум за 1 час до события.<br/>
          Анонс проходит модерацию.</p>
          <div className='hotlive__form'>
            <form onSubmit={handleSubmit}>
              <Field
                component={renderField}
                type='text'
                placeholder=''
                id='hotlive__form__header2'
                name='title'
                label='Заголовок'
                classWrap='hotlive__form__header hotlive__form__block'
                classInput=''
              />
              <div className='hotlive__form__rubric hotlive__form__block'>
                <label htmlFor='hotlive__form__rubric'>
                  Рубрика<span>*</span>
                </label>
                <Field name='rubric' component='select' id='hotlive__form__rubric'>
                  {rubrics
                    ? rubrics.Items.map(rubric => {
                      return <option value={`/api/rubrics/${rubric.id}`} key={rubric.id}>{rubric.name}</option>
                    })
                                : null
                  }
                </Field>
              </div>
              <Field
                component={renderDateTimeField}
                type='text'
                placeholder='День/Месяц/Год 00:00'
                id='hotlive__form__header'
                name='startsAt'
                label='Дата и время'
                classWrap='hotlive__form__header hotlive__form__block'
                classInput=''
              />
              <div className='hotlive__form__description hotlive__form__block'>
                <label htmlFor='hotlive__form__header'>Описание</label>
                <Field name='description' id='hotlive__form__header' component='textarea'/>
              </div>
              <Field name='cover' component={AddPhoto} loadPhoto={loadPhoto}/>
              <p className='hotlive__form__required'>* Поля обязательные для заполнения</p>
              <div className='hotlive__form__price'>
                <h2>Стоимость</h2>
                <div className='price__blocks'>
                  <p>БЕСПЛАТНО</p>
                  <p>Анонсируемые трансляции предоставляются без оплаты</p>
                </div>
                <div className='start-translation__button'>
                  {error && <p className='error'>{error}</p>}
                  <button type='submit' disabled={submitting}>
                    {submitting
                      ? <div className='loader-wrap'>
                        <div className='loader'></div>
                      </div>
                    : 'Опубликовать анонс'}
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
    )
}
export default reduxForm({form: 'streamAnnonce', validate})(StreamAnnonce)
