import React, {Component} from 'react'
import Stream from '../Item'
import MoreResults from 'components/Common/Buttons/MoreResults'
import Scroll from 'react-scroll'

export default class StreamList extends Component {

    render() {
        const {data, actions, params, dispatch} = this.props
        const Link = Scroll.Link
        return (
            <div className='articles'>
              <div className='articles-wrap'>
                {data
                  ?   data.map((item, key) => {
                    return <Stream key={key} data={item} actions={actions} />
                  })
                    :   null
                }
              </div>
              <div className='articles__button'>
                <MoreResults
                  isRender={true}
                  params={params}
                  loadMore={actions.loadData}
                  url='/streams'
                  keyData='streams'
                  dispatch={dispatch}
                />
                <Link to='ScrolltoID' duration={500} smooth={true} offset={-35}>Наверх
                  <i className='sprite sprite-icon-arrow'></i>
                </Link>
                </div>
            </div>
        )
    }
}
