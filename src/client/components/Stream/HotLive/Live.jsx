import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {offModal} from 'actions/App/modals'
import StreamWindow from 'components/Common/StreamPlayer/Live'

@connect((state) => ({
    modals: state.modals,
    streamLive: state.fetchData.streamLive.response
}), (dispatch) => ({
    actions: bindActionCreators({
        offModal
    }, dispatch)
}))

export default class StreamLive extends Component {

    render() {

        const {actions, streamLive} = this.props

        return (
            <div className='stream_live'>
              <div className='icon__close' onClick={() => {
                actions.offModal('shadowblock'),
                actions.offModal('streamLive')
              }}>
                <i className='sprite sprite-close'></i>
              </div>
              <StreamWindow data={streamLive} />
            </div>
        )
    }
}
