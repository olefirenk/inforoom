import React from 'react'
import {Field, reduxForm} from 'redux-form'

const validate = values => {
    const errors = {}

    if (!((values.title.length !== 0) && (values.title.length > 4) && (values.title.length < 51))) {
        errors.title = 'Заголовок должен содержать 5-50 символов'
    }

    if (values.rubric == null) {
        errors.rubric = 'Выберите рубрику'
    }

    return errors

}

const renderField = ({ input, label, type, meta: { touched, error, warning }, classWrap, classInput, placeholder, id}) => (
  <div className={classWrap}>
    <label htmlFor={id}>{label}<span>*</span></label>
    <input {...input} className={classInput} placeholder={placeholder} type={type} id={id}/>
    {touched && ((error && <span className='stream--validate'>{error}</span>) || (warning && <span className='stream--validate'>{warning}</span>))}
  </div>
)

const StreamLive = (props) => {

    const {rubrics, actions, error, handleSubmit, submitting} = props
    return (
        <div className='start-translation__hotlive'>
          <h2>
            <i></i>
            <p>HOT LIVE</p>
          </h2>
          <p className='start-translation__text'>Мгновенная трансляция<br/>
          без анонсирования и модерации.</p>
          <div className='hotlive__form'>
            <form onSubmit={handleSubmit}>
              <Field
                component={renderField}
                type='text'
                placeholder=''
                id='hotlive__form__header'
                name='title'
                label='Заголовок'
                classWrap='hotlive__form__header hotlive__form__block'
                classInput=''
              />
              <div className='hotlive__form__rubric hotlive__form__block'>
                <label htmlFor='hotlive__form__rubric'>
                  Рубрика<span>*</span>
                </label>
                <Field name='rubric' component='select'>
                  {rubrics
                    ? rubrics.Items.map(rubric => {
                      return <option value={`/api/rubrics/${rubric.id}`} key={rubric.id}>{rubric.name}</option>
                    })
                                : null
                  }
                </Field>
              </div>
              <div className='hotlive__form__description hotlive__form__block'>
                <label htmlFor='hotlive__form__header'>Описание</label>
                <Field name='description' id='hotlive__form__header' component='textarea'/>
              </div>
              <p className='hotlive__form__required'>* Поля обязательные для заполнения</p>
              <div className='hotlive__form__price'>
                <h2>Стоимость</h2>
                <div className='price__blocks'>
                  <div className='price__block1'>
                    <p>
                      <span className='cost'>20</span>
                    UAH</p>
                    <p>за одну Hot Live трансляцию</p>
                  </div>
                  <div className='price__block2'>
                    <p>Ваш баланс</p>
                    <p>
                      <span className='balance'>5</span>
                    UAH</p>
                    <button type='button' className='refill' onClick={() => {
                      actions.offModal('stream'),
                      actions.onModal('pay')
                    }}>Пополнить</button>
                  </div>
                  <div className='price__block2 price__block2-other' hidden>
                    <p>Ваш баланс</p>
                    <p>
                      <span className='balance'>200</span>
                    UAH</p>
                  </div>
                </div>
                <div className='start-translation__button' hidden>
                  <button type='submit'>Начать трансляцию</button>
                </div>
                <div className='start-translation__button2' >
                  {error && <p className='stream-error'>{error}</p>}
                  <button type='submit'>
                    {submitting ?
                      <div className='loader-wrap'>
                        <div className='loader'></div>
                      </div>:
                                    'Начать трансляцию'
                    }
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
    )
}
export default reduxForm({form: 'streamLive', validate})(StreamLive)
