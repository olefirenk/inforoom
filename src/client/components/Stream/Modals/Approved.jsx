import React, {Component} from 'react'
import {redirect} from 'react-isomorphic-render/redux'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'

@connect(() => ({}), (dispatch) => ({
    actions: bindActionCreators({
        redirect
    }, dispatch)
}))

export default class Approved extends Component {

    _approved = () => {
        this.props.actions.redirect('/user/annonce')
        this.props.off('shadowblock')
        this.props.off('streamApproved')
    }

    render() {

        const {off} = this.props

        return (
            <div className='approved'>
              <div className='icon__close' onClick={() => {
                off('shadowblock'),
                off('streamApproved')
              }}>
                <i className='sprite sprite-close'></i>
              </div>
              <div className='approved__head'>Спасибо!</div>
              <div className='approved__text1'>Ваш анонс передан на модерацию</div>
              <div className='approved__text2'>По результату модерации мы отправим<br/>
              Вам email c уведомлением.</div>
              <div className='approved__send' onClick={this._approved.bind(null)}>Перейти к трансляциям</div>
            </div>
        )
    }
}
