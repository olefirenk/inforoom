import React, {Component} from 'react'
import {Link} from 'react-router'
import moment from 'moment'
import noFoto from 'images/icon/registration.png'
import {BASE_URL} from 'constants/App/App.js'
import LikedButton from 'components/Common/Buttons/Liked'
import StreamPlayer from 'components/Common/StreamPlayer'

export default class Item extends Component {
    render() {
        const {data, actions} = this.props

        return (
            <div className='article'>
              <div className='article__video'>
                <StreamPlayer key={data.id} id={data.id} url={data.streamUrl} data={data}/>
                <i className='sprite sprite-icon-inforoom'></i>
              </div>
              <div className='user__info'>
                <div className='user__info__photo'>
                  <img src={data.createdBy.avatarPhoto
                            ? `${BASE_URL}${data.createdBy.avatarPhoto.path}`
                  : noFoto}/>
                </div>
                <div className='user__info__text'>
                  <div className='info1'>
                    <p>
                      <Link to={`/stream/${data.id}`} className='describe'>{data.title}</Link>
                      <span className='tag'>{data.rubric? data.rubric.name:null}</span>
                    </p>
                    <p className='watch'>
                      <i className='sprite sprite-icon-watch'></i>
                      {data.viewsCount}</p>
                  </div>
                  <div className='info2'>
                    <p>
                      <Link to={`/id${data.createdBy.id}`}>
                        <span className='author'>{data.createdBy.fullName}</span>
                      </Link>
                      <i>
                        <span className='time__day'>{moment(data.startsAt).locale('ru').format('DD MMMM YYYY в H:mm')}</span>
                      </i>
                    </p>
                    <p>
                      <Link to={`stream/${data.id}`} className='comments'>
                        <i className='sprite sprite-icon-comments'></i>
                        {data.thread.countComments}
                      </Link>
                    </p>
                    <LikedButton isLike={data.follow} id={data.id} actions={actions} />
                  </div>
                </div>
              </div>
            </div>
        )
    }
}
