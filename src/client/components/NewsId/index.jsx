import React, {Component} from 'react'
import moment from 'moment'
import noFoto from 'images/icon/registration.png'
import {BASE_URL} from 'constants/App/App.js'
import Comments from 'components/Comments'

export default class Newsid extends Component {

    render() {
      const {data} = this.props
        return (
            <div className='newsid'>
              <h2>{data.title}</h2>
              <div className='newsid__photo'>
                {data.rubric && <p className='tag'>{data.rubric.name}</p>}
                <img src={data.cover
                        ? `${BASE_URL}${data.cover.path}`
                : noFoto}/>
                <div className='newsid__photo__signature'>
                  <p>{/*В Полтавской области произошел взрыв на нефтеперерабатывающем заводе*/}
                  </p>
                </div>
              </div>
              <div className='newsid__block'>
                <span className='time__day'>{moment(data.createdAt).locale('ru').format('DD MMMM YYYY в H:mm')}</span>
                <span className='comments'>
                  <i className='sprite sprite-icon-comments'></i>
                  <i>{data.thread.countComments}</i>
                </span>
                <span className='source'>Фото: gazeta.ru</span>
                </div>
                <div className='newsid__text' dangerouslySetInnerHTML={{__html: data.content}}></div>
                <Comments threadId={(data && data.thread) ? data.thread.id:null}  />
            </div>
        )
    }
}
