import React, {Component} from 'react'
import moment from 'moment'
import {BASE_URL} from 'constants/App/App'

export default class User extends Component {
    render() {
        const { data } = this.props

        return (
            <div className='chats-user'>
                <div className='chats-user__photo'>
                    <img src={`${BASE_URL}${data.getIn(['author', 'avatarPhoto', 'path'])}`}/>
                </div>
                <div className='chats-user__info'>
                    <p>
                        <span className='info__name'>{data.get('authorName')}</span>
                        {data.has('parentComment') ?
                            <span>
                                <i>></i>
                                <span className='info__reply'>{data.getIn(['parentComment', 'user_name'])}</span>
                            </span>:
                            null
                        }

                    </p>
                    <p className='time'>
                        <span className='time__day'>{moment(data.get('createdAt')).locale('ru').format('DD MMMM YYYY')}</span> в <span className='time__hour'>{moment(data.get('createdAt')).locale('ru').format('H:mm')}</span>
                    </p>
                </div>
            </div>
        )
    }
}
