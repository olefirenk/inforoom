import React, {Component} from 'react'

export default class Like extends Component {

    constructor() {
        super()
        this.state = {
            liking: false
        }
    }

    _like = async () => {
        const {actions:{like}, id} = this.props

        this.setState({liking: true})
        await like(id)
        this.setState({liking: false})
    }

    _unlike = async () => {
        const {actions:{unlike}, id} = this.props

        this.setState({liking: true})
        await unlike(id)
        this.setState({liking: false})
    }
    render() {
        const { follow, likes } = this.props
        return (
            follow ?
                    <div className='like_wrapper' onClick={this._unlike}>
                      {this.state.liking ?
                        <div className='loader-wrapper'>
                          <div className='loader'></div>
                        </div>:
                        <div>
                          <span className='like_count'>{likes}</span>
                          <i className='sprite sprite-icon-like2'></i>
                        </div>
                      }
                    </div>:
                    <div className='like_wrapper' onClick={this._like}>
                      {this.state.liking ?
                        <div className='loader-wrapper'>
                          <div className='loader'></div>
                        </div>:
                        <div>
                          <span className='like_count'>{likes}</span>
                          <i className='sprite sprite-icon-like'></i>
                        </div>
                        }
                    </div>
        )
    }
}
