import React, {Component} from 'react'
import User from './User'
import Like from './Like'
import {Auth} from 'react-security-fetcher'

export default class Item extends Component {

    render() {
        const { data, handleAnswer, actions } = this.props

        return (
            <div className='chats__inline__user'>
              <div className='user__about'>
                <User data={data}/>
                {Auth.isAuthenticated() &&
                  <div className='user__about__like'>
                    <Like
                      follow={data.get('follow')}
                      likes={data.get('likes')}
                      id={data.get('id')}
                      actions={actions}
                    />
                    <span
                      data-id={`${data.get('id')}`}
                      data-answer={`${data.get('authorName')}`}
                      className='like__reply'
                      onClick={handleAnswer}
                    >Ответить</span>
                  </div>
                }

              </div>
              <div className='chats__inline__text'>
                <p>{data.get('body')}</p>
              </div>
            </div>
        )
    }
}
