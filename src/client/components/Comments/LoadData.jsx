import React from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {redux as fetchData} from 'react-security-fetcher'

@connect(state=>({
    state: state
}), dispatch=>({
    actions: bindActionCreators({fetchData}, dispatch),
    dispatch
}))

export default (promise) => (Component) => class LoadData extends React.Component{
    constructor(){
        super();
        this.state = {
            loaded: false
        }
    }

    componentDidMount = async () => {

        try{
            const {actions: {fetchData}, dispatch, state} = this.props
            await promise({fetchData, dispatch, getState: ()=>state, props: this.props})
            this.setState({
                loaded: true
            })
        }
        catch (e){
            console.error(e)
        }
        this.mount = true
    }

    // componentWillReceiveProps = async (props) => {
    //     if(this.mount){
    //         try{
    //             const {actions: {fetchData}, dispatch, state} = props
    //             await promise({fetchData, dispatch, getState: ()=>state, props: this.props})
    //             this.mount && this.setState({
    //                 loaded: true
    //             })
    //         }
    //         catch (e){
    //             console.error(e)
    //         }
    //     }
    // }

    componentWillUnmount = () => {
        this.mount = false
    }

    render(){
        return this.state.loaded ?
            <Component {...this.props}/>:
            <div className='loader-wrap'>
              <div className='loader'></div>
            </div>
    }
}
