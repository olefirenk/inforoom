import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {fetchData} from 'react-security-fetcher'
import {reset, focus} from 'redux-form'
import {Map} from 'immutable'
import {onModal} from 'actions/App/modals'
import InlineSVG from 'svg-inline-react'
import LoadData from './LoadData'
import ListComments from './List'
import MoreComments from './More'
import CommentForm from './Form'

const like = (id) => async (dispatch) => {

    try {
        await fetchData(`/comments/${id}/follow`, 'GET')
        dispatch({
            type: 'thread/likePost',
            payload: id
        })

    }
    catch (e) {
        console.log('like comment err', e);
        throw e
    }
}

const unlike = (id) => async (dispatch) => {
    try {
        await fetchData(`/comments/${id}/unfollow`, 'GET')
        dispatch({
            type: 'thread/unlikePost',
            payload: id
        })
    }
    catch (e) {
        console.log('unlike comment err', e);
        throw e
    }
}

@LoadData(async ({dispatch, props})=>{

    const response = await fetchData('/comments', 'GET', {
        params:{
            thread: `${props.threadId}`,
            ['order[createdAt]']: 'asc',
            page: 1,
            limit: 3
        }
    })

    dispatch({
        type: 'thread/loadList',
        payload: response
    })
})

@connect(() => ({}), (dispatch) => ({
    actions: bindActionCreators({
        like,
        unlike
    }, dispatch)
}))

export default class Comments extends Component {

    constructor() {
        super()
        this.state = {
            parentComment: null,
            answerName: ''
        }
    }

    _handleCommentSubmit = async (form, dispatch) => {
        const {threadId} = this.props
        const {parentComment} = this.state
        let params = {
            body: form.body,
            thread: `/api/threads/${threadId}`
        }

        if (parentComment !== null) {
            params.parentComment = `/api/comments/${parentComment}`
        }

        const response = await fetchData('/comments', 'POST', {params})
        dispatch({
            type: 'thread/unshiftPost',
            payload: response
        })
        this._removeAnswer()

    }

    _handleAnswer = (event) => {
        const {dispatch} = this.props
        this.setState({
            parentComment: event.target.getAttribute('data-id'),
            answerName: event.target.getAttribute('data-answer')
        })
        dispatch(focus('commentPost', 'body'))
    }

    _removeAnswer = () => {
        const {dispatch} = this.props

        this.setState({
            parentComment: null,
            answerName: ''
        })
        dispatch(reset('commentPost'))
    }

    _openLogin = () => {
        this.props.dispatch(onModal('login'))
        this.props.dispatch(onModal('shadowblock'))
    }

    render() {

        const {state: {thread, authentication: {user}}, actions, threadId, dispatch} = this.props
        const {answerName} = this.state
        const userImmutable = Map(user)

        return (
            <div className='chats'>
              <div className='chats-wrap'>
                {thread.get('totalItems') > 3 ?
                  <MoreComments
                    count={thread.get('totalItems')}
                    threadId={threadId}
                    dispatch={dispatch}
                  />:
                        null
                }

                <ListComments
                  items={thread.get('Items')}
                  handleAnswer={this._handleAnswer}
                  user={userImmutable}
                  actions={actions}
                />
                {userImmutable.size > 0 ?
                  <div className='chats__add-comments'>
                    <div className='comment_add'>
                      <p className='comment__text'>Добавить комментарий</p>
                      {answerName !== '' ?
                        <p className='comment__answer'>
                          <InlineSVG
                            src={require('images/icon/close-button.svg')}
                            className='icon--close'
                            onClick={this._removeAnswer}
                          />
                          Ответить, {answerName}</p>:
                                    null
                      }
                    </div>
                    <CommentForm
                      onSubmit={this._handleCommentSubmit}
                    />
                  </div>:
                  <div className='chats__add-comments border--top'>
                    <p className='comment--auth'><span onClick={this._openLogin}>Войдите</span> на сайт, чтобы оставлять комментарии</p>
                  </div>
                }
                {/*<p className='comments-head'>Коментарии</p>*/}
              </div>
            </div>
        )
    }
}
