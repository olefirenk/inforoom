import React, {Component} from 'react'
import {fetchData} from 'react-security-fetcher'

export default class Comments extends Component {

    constructor() {
        super()
        this.state = {
            loading: false,
            loaded: false
        }
    }

    _loadAll = async () => {
        const {threadId, dispatch} = this.props
        this.setState({loading: true})
        const response = await fetchData('/comments', 'GET', {
            params:{
                thread: `${threadId}`,
                ['order[createdAt]']: 'asc'
            }
        })

        dispatch({
            type: 'thread/loadList',
            payload: response
        })
        this.setState({loading: false, loaded: true})
    }

    _hideAll = async () => {
        const {threadId, dispatch} = this.props
        this.setState({loading: true})
        const response = await fetchData('/comments', 'GET', {
            params:{
                thread: `${threadId}`,
                ['order[createdAt]']: 'asc',
                page: 1,
                limit: 3
            }
        })

        dispatch({
            type: 'thread/loadList',
            payload: response
        })
        this.setState({loading: false, loaded: false})
    }
    render() {
        const { count } = this.props
        return (
            <div className='chats__other-comments'>
              {this.state.loaded ?
                <button className='btn--all' onClick={this._hideAll}>
                  {this.state.loading ?
                    <div className='loader-wrap'>
                      <div className='loader'></div>
                    </div>:
                            'Скрыть комментарии'
                  }
                </button>:
                <button className='btn--all' onClick={this._loadAll}>
                  {this.state.loading ?
                    <div className='loader-wrap'>
                      <div className='loader'></div>
                    </div>:
                            `Показать все ${count} комменатриев`
                  }
                </button>
              }
              {/*<div className='hide__comments'>
                    <Link to=''>Скрыть комментарии
                        <i className='sprite sprite-icon-comments3'></i>
                    </Link>
              </div>*/}
            </div>
        )
    }
}
