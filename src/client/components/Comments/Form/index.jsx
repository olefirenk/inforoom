import React, {Component} from 'react'
import {reduxForm, Field} from 'redux-form'
import InlineSVG from 'svg-inline-react'

@reduxForm({
    form: 'commentPost'
})

export default class Post extends Component {
    render() {

        const {handleSubmit, submitting} = this.props

        return (
            <form className='comment_form' onSubmit={handleSubmit}>
              <Field name='body' component='textarea' className='comment__input' />
              <div className='btn_wrapper'>
                <div className='btn__send' onClick={handleSubmit}>
                  {submitting ?
                    <div className='loader-wrap'>
                      <div className='loader'>
                      </div>
                    </div>:
                    <div className='btn--center'>
                      <span className='btn__name'>Отправить</span>
                      <InlineSVG
                        src={require('images/icon/send.svg')}
                        className='btn__icon'
                      />
                    </div>
                  }
                    </div>
                </div>
            </form>
        )
    }
}
