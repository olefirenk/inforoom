import React, {Component} from 'react'
import Comment from './Comment'

export default class List extends Component {
    render() {
        const { items, handleAnswer, user, actions } = this.props

        return (
            <div className='chats__inline'>
              <div className='chats__inline__users'>
                {items.map(item=> {
                  return <Comment
                    data={item}
                    key={item.get('id')}
                    handleAnswer={handleAnswer}
                    user={user}
                    actions={actions}
                  />
                })}
                </div>
            </div>
        )
    }
}
