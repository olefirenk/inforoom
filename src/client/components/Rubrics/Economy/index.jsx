import React, {Component} from 'react'
import Annonces from 'components/Common/User/AnnonceOther'

export default class MainAnnonces extends Component {

    render() {
        const {actions, users, annonces} = this.props
        return (
            <div className='main-annonces-wrap'>
              <h2>Анонсы</h2>
              <div className='main-annonces'>
                {annonces ?
                  annonces.Items.map((annonce,key)=>{
                    return <Annonces
                      key={key}
                      user={users}
                      data={annonce}
                      actions={actions}
                      type='SearchAnnonce'
                    />
                  }):
                    null
                }
              </div>
            </div>
        )
    }
}
