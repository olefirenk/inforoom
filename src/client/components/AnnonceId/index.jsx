import React, {Component} from 'react'
import moment from 'moment'
import WatchingButton from 'components/Common/Buttons/WillWatching'
import SocialButton from 'pages/SocialButton'
import Comments from 'components/Comments'
import Annonce from 'components/Sidebar/Annonce'
import noFoto from 'images/icon/registration.png'
import {BASE_URL} from 'constants/App/App.js'
import Subscribe from 'components/Common/Buttons/Subscribe'
import LastStreams from 'components/AnnonceId/LastStreams'
import {Link} from 'react-router'

export default class MainAnnonceId extends Component {

    render() {
      const {streams, annonce, annonces, actions} =this.props
      let stream = streams.Items.slice(0,3)
        return (
            <div className='annonceid-wrap'>
                <h2>Анонсы</h2>
                <div className='annonceid-wrap__main'>
                    <div className='annonceid-wrap__main__column'>
                        <div className='annonceid'>
                            <p className='annonceid__head'>Анонс трансляции</p>
                            <p className='annonceid__caption'>{annonce.title}</p>
                            <i className='annonceid__photo'>
                                <img src={annonce.createdBy.avatarPhoto
                                ? `${BASE_URL}${annonce.createdBy.avatarPhoto.path}`
                                : noFoto}/>
                                <p className='tag'>{annonce.rubric.name}</p>
                            </i>
                            <div className='annonceid__info'>
                                <p className='time__day'>
                                    <i className='sprite sprite-annonce-translation'></i>
                                    {moment(annonce.createdAt).locale('ru').format('DD MMMM YYYY в H:mm')}
                                </p>
                                <WatchingButton isWatch={annonce.follow} id={annonce.id} count={annonce.countFollowing} actions={actions} />
                                <p>
                                    <i className='sprite sprite-icon-comments'></i>
                                    <span className='comments-count'>{annonce.thread.countComments}</span>
                                </p>
                            </div>
                            <div className='annonceid__user'>
                                <div className='annonceid__user__photo'>
                                    <i><img src={annonce.createdBy.avatarPhoto
                                        ? `${BASE_URL}${annonce.createdBy.avatarPhoto.path}`
                                        : noFoto}/>
                                    </i>
                                </div>
                                <div className='annonceid__user__info'>
                                    <p>
                                      <Link className='author' to={`/id${annonce.createdBy.id}`}>
                                        {annonce.createdBy.fullName}
                                      </Link>
                                      <span>21 июля 2015 в 12:14</span></p>

                                    <Subscribe isSubsribe={annonce.createdBy.follow} id={annonce.createdBy.id} actions={actions} />
                                </div>
                            </div>
                            <div className='annonceid__text'>
                                {annonce.description}
                            </div>
                            <SocialButton/>
                        </div>
                        <div className='streamid-wrap'>
                            <p className='annonceid__head'>Последние трансляции {annonce.createdBy.fullName}</p>
                            { stream
                                ? stream.map((stream, key) => {
                                    return <LastStreams key={key} data={stream}/>
                                })
                                : null
                            }

                        </div>
                        <p className='annonceid__head commentid__head'>Коментарии</p>
                        <Comments threadId={(annonce && annonce.thread) ? annonce.thread.id:null}  />
                    </div>
                    <div className='annonceid-wrap__main__column'>
                        <p className='annonceid__head annonceid__other'>Последние анонсы</p>
                        <Annonce annonces={annonces} type='annonce-head'/>
                    </div>
                </div>
            </div>
        )
    }
}
