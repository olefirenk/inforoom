import React, {Component} from 'react'
import WatchComments from 'components/MiniComponents/WatchComments'
import {Link} from 'react-router'

export default class LastStreams extends Component {

    render() {
        const {data} = this.props
        return (
              <div className='streamid'>
                  <Link to={`/annonce/${data.id}`}>
                  <div className='streamid__video'>
                      <i><img src={require('images/translation-main.jpg')}/></i>
                  </div>
                  <p className='streamid__header'>{data.title}</p>
                  </Link>
                  <div className='streamid__info'>
                      <p className='streamid__tag'>{data.rubric.name}</p>
                      <WatchComments key={data.id} data={data}/>
                  </div>
              </div>
        )
     }
}
