import React, {Component} from 'react'
import {Link} from 'react-router'
import StreamOther from '../../../components/Common/User/StreamOther'
import moment from 'moment'
import * as calendarActions from '../../../actions/Calendar/actionsCalendar'
import {bindActionCreators} from 'redux'
import {preload} from 'react-isomorphic-render/redux'
import {connect} from 'react-redux'


// @preload(({dispatch, fetchData, getState ,parameters}) =>Promise.all([
//   dispatch(fetchData('/streams', 'streamsCalendar', 'GET', {params: {
//       type: 'web-cast',
//        ['startsAt[after]']: encodeURIComponent(moment().startOf('day').format()),
//        ['startsAt[before]']: encodeURIComponent(moment().endOf('day').format()),
//       ['order[startsAt]']: 'asc',
//       page:1,
//       limit:9,
//   }}))
// ]))

@connect(state => ({
streamsCalendar:state.fetchData.streamsCalendar.response,
}),
(dispatch) => ({
    actions: bindActionCreators(
      calendarActions
    , dispatch)
})
)




export default class StreamsList extends Component {
  constructor(props) {
      super(props)
      this.state={
        day:this.props.day
        // prevDay:moment(this.props.day).subtract(1,'days').format('Do'),
        // chosenDay:moment(this.props.day).format('MMMM Do YYYY'),
        // nextDay:moment(this.props.day).add(1,'days').format('Do')
    }
  }
  componentWillReceiveProps= (next)=>{
   next.day!=this.props.day && this.setState({day:next.day})
  }
  shpouldComponentUpdate=(next)=>{
    console.log('shpouldComponentUpdate',moment(next.day),moment(this.state.day))
  return true
  }
  nextDayLoad = (e)=>{
    let day=moment(this.state.day).add(1,'days');
    this.props.actions.loadStreamsForDay(day)
        this.setState({day})
  }
  prevDayLoad = (e)=>{
    let day=moment(this.state.day).subtract(1,'days');
    this.props.actions.loadStreamsForDay(day)
    this.setState({day})

  }

    render() {

      const {data, actions, type,items,isLoading,streamsCalendar:{Items}} = this.props
      const { day } = this.state

      let prevDay=moment(day).subtract(1,'days').format('Do')
      let chosenDay=moment(day).format('MMMM Do YYYY')
      let nextDay=moment(day).add(1,'days').format('Do')
        return (
          <div className="calendar__streams-list">
            <div className='calendar__streams-list__navigation'>
              <div>
                <button className="calendar__streams-list__navigation__btn news_button button_next"
                  onClick={this.prevDayLoad}
                >
                  <i className="sprite sprite-icon-prev-news"></i>
                </button>
                <span className="calendar__streams-list__navigation__label">{prevDay}</span>
              </div>
              <div>{chosenDay}</div>
              <div>
                <span className="calendar__streams-list__navigation__label">{nextDay}</span>
                <button  className="calendar__streams-list__navigation__btn"
                  onClick={this.nextDayLoad}
                >
                  <i className="sprite sprite-icon-next-news"></i>
                </button>
              </div>
            </div>
            <div className='mytranslation-wrap'>
              {Items && Items.length ?
                Items.map((stream, key)=>{
                  return(
                    <StreamOther
                      key = {key}
                      idUser = {stream.createdBy.id}
                      actions = {actions}
                      data = {stream}
                    />
                  )
                })
                :
                <span className='calendar__streams-list__nothing-to-show'>За эту дату ничего не найдено</span>
              }
            </div>
            {Items && Items.length ?  <div className='calendar__streams-list__navigation'>
              <div>
                <button className="calendar__streams-list__navigation__btn news_button button_next"
                  onClick={this.prevDayLoad}
                >
                  <i className="sprite sprite-icon-prev-news"></i>
                </button>
                <span className="calendar__streams-list__navigation__label">{prevDay}</span>
              </div>
              <div>{chosenDay}</div>
              <div>
                <span className="calendar__streams-list__navigation__label">{nextDay}</span>
                <button  className="calendar__streams-list__navigation__btn"
                  onClick={this.nextDayLoad}
                >
                  <i className="sprite sprite-icon-next-news"></i>
                </button>
              </div>
            </div> : null
            }

          </div>
        )
    }
}
