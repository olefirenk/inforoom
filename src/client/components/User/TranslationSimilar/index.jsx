import React, {Component} from 'react'
import Translation from 'components/Common/User/StreamSearch'

export default class TranslationSimilar extends Component {

    render() {
      const {streams, actions} = this.props
      return (
        <div className='streamid__wrap'>
          <div className='streamid__head'>Похожие трансляции</div>
            {streams && streams.Items.length > 0 ?
                streams.Items.map((stream, key)=>{
                    return <Translation
                              key={key}
                              data={stream}
                              actions={actions}
                              type='streamSimilar'
                              />
                }):
                null
            }
        </div>
      )
    }
  }
