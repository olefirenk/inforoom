import React, {Component} from 'react'
import {Link} from 'react-router'

import SocialButton from 'pages/SocialButton'
import moment from 'moment'
import noFoto from 'images/icon/registration.png'
import {BASE_URL} from 'constants/App/App.js'
import LikedButton from 'components/Common/Buttons/Liked'
import Subscribe from 'components/Common/Buttons/Subscribe'
import StreamPlayer from 'components/Common/StreamPlayer'
import Comments from 'components/Comments'

export default class Translation extends Component {

    render() {
      const {data, actions, type} = this.props
        return (
            <div className='translation'>
                <h2>Трансляции</h2>
                {data.length != 0 ?
                    <div className='translation-wrap'>
                        <div className='translation__web'>
                            <StreamPlayer key={data.id} id={data.id} url={data.streamUrl} data={data}/>
                        </div>
                        <div className='user__info'>
                            <div className='user__info__photo'>
                                <img src={data.createdBy.avatarPhoto
                                    ? `${BASE_URL}${data.createdBy.avatarPhoto.path}`
                                    : noFoto}/>
                            </div>
                            <div className='user__info__text'>
                                <div className='info1'>
                                    <p>
                                        <Link to={`/stream/${data.id}`} className='describe'>{data.title}</Link>
                                        {data.rubric ?
                                            <span className='tag'>{data.rubric.name}</span>
                                            :null
                                        }
                                    </p>
                                    <p className='watch'>
                                        <i className='sprite sprite-icon-watch'></i>
                                        {data.viewsCount}
                                    </p>
                                </div>
                                <div className='info2'>
                                    <div>
                                        <Link to={`/id${data.createdBy.id}`}>
                                            <span className='author'>{data.createdBy.fullName}</span>
                                        </Link>
                                        <i>
                                            <span className='time__day'>{moment(data.startsAt).locale('ru').format('DD MMMM YYYY в H:mm')}</span>
                                        </i>
                                    </div>
                                    <div>
                                        <Link to={`stream/${data.id}`} className='comments'>
                                            <i className='sprite sprite-icon-comments'></i>
                                            {data.thread.countComments}
                                        </Link>
                                        <LikedButton isLike={data.follow} id={data.id} actions={actions} />
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div className='translation__about__button'>
                            <Subscribe isSubscribe={data.createdBy.follow}  id={data.createdBy.id} actions={actions} />
                            <SocialButton/>
                        </div>
                        <div className='translation__description'>
                            <p>
                                {data.description}
                            </p>
                        </div>
                    </div>:
                    <p>Нет трасляций...</p>
                }
                { type === 'streamId'
                ?  null
                :  <Comments threadId={(data && data.thread) ? data.thread.id:null} />
                }
            </div>
        )
    }
}
