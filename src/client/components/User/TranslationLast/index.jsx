import React, {Component} from 'react'
import Translation from 'components/Common/User/StreamSearch'

export default class TranslationLast extends Component {

    render() {
      const {streams, actions, data} = this.props
      return (
        <div className='streamid__wrap'>
          <div className='streamid__head'>Последние трансляции {data.createdBy.fullName}</div>
            {streams && streams.Items.length > 0 ?
                streams.Items.map((stream, key)=>{
                    return <Translation
                              key={key}
                              data={stream}
                              actions={actions}
                              type='streamsLast'
                              idUser={stream.createdBy.id}
                              />
                }):
                null
            }
        </div>
      )
    }
  }
