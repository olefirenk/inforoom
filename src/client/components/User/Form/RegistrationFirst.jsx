import React from 'react'
import {Field, reduxForm} from 'redux-form'
import Social from 'components/Security/Social'
import { EmailValidation , PasswordValidationRequeired} from 'utils/Validation/Validation'

const validate = values => {
    const errors = {}

    if (!PasswordValidationRequeired(values.plainPassword, 6, 100)) {
        errors.plainPassword = 'Длина пароля от 6 символов, без пробелов'
    }

    if (!PasswordValidationRequeired(values.plainPassword1, 6, 100)) {
      errors.plainPassword1 = 'Длина пароля от 6 символов, без пробелов'
    }

    if (!EmailValidation.test(values.email)) {
      errors.email = 'Неправильный email'
    }

    if (values.plainPassword != values.plainPassword1) {
        errors.plainPassword1 = 'Пароли не совпадают'
    }
    return errors
}

const renderField = ({
    input,
    type,
    meta: {
        touched,
        error,
        warning
    },
    classInput,
    placeholder
}) => (
    <div className='registration-field'>
      <input {...input} className={classInput} placeholder={placeholder} type={type}/>
      {touched && ((error && <span className='registration--warning'>{error}</span>) || (warning && <span className='registration--warning'>{warning}</span>))}
    </div>
)

const RegistrationFirst = (props) => {

    const {
        error,
        submitting,
        handleSubmit,
        off,
        on
    } = props

    return (
        <div className='enter open'>
          <div onClick={() => {
            off('registrationfirst'),
            off('shadowblock')
          }} className='icon__close'>
            <i className='sprite sprite-close'></i>
          </div>
          <p className='enter__head'>Создайте аккаунт</p>
          <p className='enter__text'>чтобы создавать трансляции, делать анонсы, оставлять комментарии и многое другое</p>
          <Social/>
          <div className='enter__line'>
            <span>Или</span>
          </div>
          <form className='enter__form' onSubmit={handleSubmit}>
            <Field component={renderField} type='email' name='email' placeholder='Email' required/>
            <Field component={renderField} type='password' name='plainPassword' placeholder='Введите пароль' minLength='6' required/>
            <Field component={renderField} type='password' name='plainPassword1' placeholder='Повторите пароль' minLength='6' required/> {error && <p className='register--error'>{error}</p>}
            <button className='enter__button' type='submit' disabled={submitting}>
              {submitting
                ? <div className='loader-wrap'>
                  <div className='loader'></div>
                </div>
                        : 'Регистрация'
              }
            </button>
            <p>Есть учетная запись?
              <span className='enter__link enter__registration2' onClick={() => {
                off('registrationfirst'),
                on('shadowblock'),
                on('login')
              }}> Войдите</span>
            </p>
          </form>
        </div>
    )
}

export default reduxForm({form: 'registrationfirst', validate})(RegistrationFirst)
