import React, {Component} from 'react'
import {Field} from 'redux-form'
export default class EditForm extends Component {
    render() {

        return (
            <div className='userinfo__about__public'>
              <div className='public__location'>
                <label htmlFor='location__country'>Локация</label>
                <Field component='input' className='location__country' id='location__country' placeholder='Страна' name='location'/>
                <Field component='input' className='location__city' id='location__city' placeholder='Город' name='city'/>
                <label htmlFor='location__about'>О себе</label>
                <Field component='textarea' name='about' className='location__about' id='location__about' rows='9' placeholder='Произвольная информация'/>
              </div>
              <div className='public__socials'>
                <label htmlFor='social__network'>Социальные сети</label>
                <Field component='input' className='social__network' id='social__network' placeholder='Выберите соц. сеть' name='social'/>
                <Field component='input' className='social__link' id='social__link' placeholder='Укажите ссылку на страницу' name='link'/>
                <div className='add__field'>
                  <img src={require('core/assets/images/icon/icon-include.png')}/>
                  <span>Добавить еще</span>
                </div>
              </div>
              <div className='public__web'>
                <label htmlFor='web__site'>Веб сайты</label>
                <div className='web-wrap'>
                  <img src={require('core/assets/images/icon/website.png')}/>
                  <Field component='input' className='web__site' id='web__site' placeholder='Укажите сайт' name='website'/>
                </div>
              </div>
              <div className='public__contact'>
                <label htmlFor='contact__number'>Контакты</label>
                <div className='phone-wrap'>
                  <img src={require('core/assets/images/icon/phone.png')}/>
                  <Field component='input' className='contact__number' id='contact__number' placeholder='Телефон' name='phone'/>
                </div>
                <div className='add__field'>
                  <img src={require('core/assets/images/icon/icon-include.png')}/>
                  <span>Добавить еще</span>
                </div>
                <div className='email-wrap'>
                  <img src={require('core/assets/images/icon/email.png')}/>
                  <Field component='input' className='contact__email' id='contact__email' placeholder='Публичный email' name='public-email'/>
                </div>
              </div>
            </div>
        )
    }
}
