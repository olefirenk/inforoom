import React from 'react'
import {Field, FieldArray, reduxForm} from 'redux-form'
import Avatar from '../../App/Photos/Avatar'
import Checkbox from '../../App/Checkbox'
import {DateValidation, EmailValidation , PasswordValidation, UrlValidation, PhoneValid} from 'utils/Validation/Validation'
import Location from 'components/App/Form/Location'
import Phone from 'components/App/Form/Phone'
// import Social from 'components/App/Form/Social'
import {DateField} from 'components/App/Form/Date'
import {normalizeNum} from 'utils/Normalize/Normalize'

const validate = values => {
  const errors = {}

  if (PasswordValidation(values.plainPassword, 6)) {
    errors.plainPassword = 'Пароль должен содержать минимум 6 символа'
  }

  if (!EmailValidation.test(values.email)) {
    errors.email = 'Неправильный email'
  }

  if (!DateValidation.test(values.birthday)) {
    errors.birthday = 'Формат День/Месяц/Год'
  }

  if (values.about.length >= 400) {
    errors.about = 'Максимальное количество символов 400 знаков'
  }
  const phonesErrors = {}
  const phonesArrayErrors = []

  values.phones.forEach((field, index)=>{
      if (PhoneValid(field)) {
          phonesErrors._error = 'Неправильный номер'
          phonesArrayErrors[index] = phonesErrors
      }
  })

  if (phonesArrayErrors.length) {
      errors.phones = phonesArrayErrors
    }

  if ((values.website != null) && (values.website != '') && (!UrlValidation.test(values.website))) {
    errors.website = 'Неправильная ссылка'
  }

  if ((values.publicEmail != null) && (values.publicEmail != '') && (!EmailValidation.test(values.publicEmail))) {
    errors.publicEmail = 'Неправильный email'
  }

  return errors
}

const renderField = ({ input, label, type, meta: { touched, error, warning }, classWrap, classInput, placeholder, id}) => (
  <div className={classWrap}>
    <label htmlFor={id}>{label}</label>
    <input {...input} className={classInput} placeholder={placeholder} type={type} id={id}/>
    {touched && ((error && <span className='input--validate'>{error}</span>) || (warning && <span className='input--validate'>{warning}</span>))}
  </div>
)

const textField = ({ input, label, type, meta: { touched, error, warning }, classWrap, classInput, placeholder, id}) => (
  <div className={classWrap}>
    <label htmlFor={id}>{label}</label>
    <textarea {...input} className={classInput} placeholder={placeholder} rows='9' type={type} id={id}/>
    {touched && ((error && <span className='about--validate'>{error}</span>) || (warning && <span className='input--validate'>{warning}</span>))}
  </div>
)

const renderUrlField = ({ input, label, type, meta: { touched, error, warning }, classWrap, classInput, placeholder, id}) => (
  <div className={classWrap}>
    <label htmlFor={id}>{label}</label>
    <div className='web-wrap'>
        <i className='sprite sprite-website'></i>
            <input {...input} className={classInput} placeholder={placeholder} type={type} id={id}/>
            {touched && ((error && <span className='input--validate'>{error}</span>) || (warning && <span className='input--validate'>{warning}</span>))}
    </div>

  </div>
)

const renderSingleField = ({ input, type, meta: { touched, error, warning }, classWrap, classInput, placeholder, id}) => (
  <div className={classWrap}>
    <i className='sprite sprite-email'></i>
    <input {...input} className={classInput} placeholder={placeholder} type={type} id={id}/>
    {touched && ((error && <span className='input--validate'>{error}</span>) || (warning && <span className='input--validate'>{warning}</span>))}
  </div>
)

const UserForm = (props) => {

    const {error, submitting, handleSubmit, user, actions} = props

    return (
        <form className='userinfo' onSubmit={handleSubmit}>
            <div className='triangle'></div>
            <div className='userinfo__about'>
                <div className='userinfo__about__main'>
                    <div className='main__name'>
                        <p>{user.firstname} {user.lastname}</p>
                    </div>
                    <Field name='avatarPhoto' component={Avatar} loadPhoto={actions.loadPhoto} src={user.avatarPhoto}/>
                    <Field
                        component={renderField}
                        type='email'
                        placeholder='Email'
                        id='email__user'
                        name='email'
                        label='Email'
                        classWrap='main__email'
                        classInput=''
                    />
                    <Field name='sex' component={Checkbox} type='edit'/>
                    <Field
                        component={DateField}
                        type='text'
                        placeholder='День / Месяц / Год'
                        id='birth__user'
                        name='birthday'
                        label='Дата рождения'
                        classWrap='main__birth'
                        classInput=''
                        normalize={normalizeNum}
                    />
                    <Field
                        component={renderField}
                        type='text'
                        placeholder='Введите пароль'
                        id='password__user'
                        name='plainPassword'
                        label='Пароль'
                        classWrap='main__password'
                        classInput=''
                    />
                </div>
                <div className='public__head'>
                    <p>Публичная информация профиля</p>
                </div>
                <div className='userinfo__about__public'>
                    <label htmlFor='location__country' className='location__country'>Локация</label>
                    <Field component={Location} name='city'/>
                    <label htmlFor='location__about'>О себе</label>
                    <Field
                        component={textField}
                        name='about'
                        classInput='location__about'
                        classWrap='about--wrap'
                        id='location__about'
                        rows='9'
                        placeholder='Произвольная информация'
                    />
                    {/*<FieldArray component={Social} name='socialUrls'/>*/}
                    <Field
                        component={renderUrlField}
                        type='text'
                        placeholder='Укажите сайт'
                        id='web__site'
                        name='website'
                        label='Веб сайты'
                        classWrap='public__web'
                        classInput=''
                    />
                    <FieldArray component={Phone} name='phones'/>
                    <div className='public__contact'>
                        <Field
                            component={renderSingleField}
                            type='text'
                            placeholder='Публичный email'
                            id='contact__email'
                            name='publicEmail'
                            classWrap='email-wrap'
                            classInput='contact__email'
                        />
                    </div>
                </div>
            </div>
            <div className='userinfo__save-button'>
                {error && <p className='profile-error'>{error}</p>}
                <button type='submit' disabled={submitting}>
                    {submitting ?
                        <div className='loader-wrap'>
                            <div className='loader'></div>
                        </div>:
                        'Сохранить изменения'
                    }
                </button>
            </div>
        </form>
    )
}
export default reduxForm({form: 'userForm', validate})(UserForm)
