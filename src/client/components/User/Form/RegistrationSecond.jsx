import React from 'react'
import {Field, reduxForm, formValueSelector} from 'redux-form'
import { connect } from 'react-redux'
import Avatar from '../../App/Photos/Avatar'
import Checkbox from '../../App/Checkbox'
import {DateValidation} from 'utils/Validation/Validation'
import InputElement from 'react-input-mask'
import {normalizeName} from 'utils/Normalize/Normalize'
import {LengthValidation} from 'utils/Validation/Validation'

const validate = values => {
  const errors = {}

  if (LengthValidation(values.firstname, 2, 40)) {
    errors.firstname = 'Имя должно содержать 2-40 символов'
  }
  if (LengthValidation(values.lastname, 2, 40)) {
    errors.lastname = 'Фамилия должна содержать 2-40 символов'
  }

  if (!DateValidation.test(values.birthday)) {
    errors.birthday = 'Формат День/Месяц/Год'
  }
  return errors
}

const warn = values => {
  const warnings = {}

  if ((values.firstname !== '') && (values['firstname'].length > 40) && (values['firstname'].length <= 2)) {
    warnings.firstname = 'Имя должно содержать 2-40 символов'
  }

  if ((values.lastname !== '') && (values['lastname'].length > 40) && (values['lastname'].length <= 2)) {
    warnings.lastname = 'Фамилия должна содержать 2-40 символов'
  }

  if (!DateValidation.test(values.birthday)) {
    warnings.birthday = 'Формат День/Месяц/Год'
  }
  return warnings
}

const renderField = ({ input, label, type, meta: { touched, error, warning }, classWrap, classInput, placeholder, id}) => (
  <div className={classWrap}>
    <label htmlFor={id}>{label}<i>*</i></label>
    <input {...input} className={classInput} placeholder={placeholder} type={type} id={id}/>
    {touched && ((error && <span className='registrationSecond--validate'>{error}</span>) || (warning && <span className='input--validate'>{warning}</span>))}
  </div>
)

const DateField = ({ input, label, type, meta: { touched, error, warning }, classWrap, classInput, placeholder, id}) => (
  <div className={classWrap}>
    <label htmlFor={id}>{label}<i>*</i></label>
    <InputElement {...input} mask='99/99/9999' className={classInput} placeholder={placeholder} type={type} id={id} />
    <span>Непубличная информация</span>
    {touched && ((error && <span className='registrationDate--validate'>{error}</span>) || (warning && <span className='input--validate'>{warning}</span>))}
  </div>
)

let Registration = (props) => {

    const {error, submitting, valid, agreement, handleSubmit, actions, off, on} = props

    return (
        <div className='registration open'>
            <form onSubmit={handleSubmit}>
                <p className='registration__head'>Укажите персональные данные</p>
                <p className='registration__text'>а также публичную информацию отображаемую<br/>
                    на странице Вашего профиля</p>

                <Field component={Avatar} loadPhoto={actions.loadPhoto} name='avatarPhoto'/>
                <Field
                    component={renderField}
                    type='text'
                    placeholder='Отображаемое имя профиля'
                    id='registration__firstname'
                    name='firstname'
                    label='Имя'
                    classWrap='registration__name'
                    classInput=''
                    normalize={normalizeName}
                />
                <Field
                    component={renderField}
                    type='text'
                    placeholder='Отображаемая фамилия профиля'
                    id='registration__lastname'
                    name='lastname'
                    label='Фамилия'
                    classWrap='registration__name'
                    classInput=''
                    normalize={normalizeName}
                />
                <Field name='sex' component={Checkbox} type='create'/>
                <Field
                    component={DateField}
                    type='text'
                    placeholder='День / Месяц / Год'
                    id='registration__birthday'
                    name='birthday'
                    label='Дата рождения'
                    classWrap='registration__birth'
                    classInput=''
                />
                <p className='registration__field-require'>* Поля обязательные для заполнения</p>
                <div className='add-information'>
                    {/*<p className='add-information__head' onClick={() => {
                        toggle(modals.editForm, 'editForm')
                    }}>
                        Добавить публичную информацию профиля
                        <i className='sprite sprite-add-profile'></i>
                    </p>
                    {modals.editForm ? <EditForm/>: null}*/}
                    <div className='add-information__agreement'>
                        <Field component='input' type='checkbox' name='agreement' id='agreement'/>
                        <label htmlFor='agreement'>Отмечая этот пункт я соглашаюсь с
                            <span className='enter__link' onClick={() => {
                                off('registrationsecond'),
                                on('contract')
                            }}> Пользовательским соглашением</span>
                        </label>
                        <div className='add-information__agreement__button'>
                            {error && <p className='register--error'>{error}</p>}
                            <button type='submit' disabled={!valid || !agreement || submitting}>
                                {submitting ?
                                    <div className='loader-wrap'>
                                        <div className='loader'></div>
                                    </div>:
                                    'Готово'
                                }
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    )
}

Registration = reduxForm({form: 'registrationsecond', warn, validate})(Registration)

const selector = formValueSelector('registrationsecond')
Registration = connect (
    state => {
        const agreement = selector(state, 'agreement')
        return {
            agreement
        }
    }
)(Registration)

export default Registration
