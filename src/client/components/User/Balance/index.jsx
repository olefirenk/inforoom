import React, {Component} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {onModal, offModal} from '../../../actions/App/modals'
import Pay from '../Modals/Pay'
import {logout} from '../../../actions/Security/Security'
import { goto } from 'react-isomorphic-render/redux'
@connect((state) => ({modals: state.modals}), (dispatch) => ({
    actions: bindActionCreators({
        logout,
        onModal,
        offModal,
        goto
    }, dispatch)
}))

export default class Balance extends Component {

    render() {
        const {actions, modals} = this.props
        return (
            <div>
                {modals.pay
                    ? <Pay off={actions.offModal}/>
                    : null
}
                <div className='userinfo__balance-wrap'>
                    <div className='userinfo__balance'>
                        <p>Баланс
                            <i>
                                200 UAH
                            </i>
                            <button onClick={() => {
                                actions.onModal('shadowblock'),
                                actions.onModal('pay')
                            }}>Пополнить</button>
                        </p>
                    </div>
                    <div className='userinfo__user'>
                        <button onClick={() => {
                            actions.logout(),
                            actions.offModal('loginProfile'),
                            actions.goto('/')
                        }}>Выйти</button>
                    </div>

                </div>

            </div>
        )
    }
}
