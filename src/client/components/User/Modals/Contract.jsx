import React, {Component} from 'react'

export default class Contract extends Component {

    render() {
        const {off, on} = this.props
        return (
            <div className='contract'>
                <div className='contract-wrap'>
                    <h2>УСЛОВИЯ ИСПОЛЬЗОВАНИЯ ВЕБ-САЙТА INFOROOM.COM</h2>
                    <p>Внимательно прочитайте эти Условия использования прежде, чем начинать пользоваться веб-сайтом (“Сайтом””).</p>
                    <p className='contract__condition'>ПРИНЯТИЕ УСЛОВИЙ</p>
                    <ol>
                        <li>Вы выражаете согласие с изложенными ниже положениями и условиями, а также любыми изменениями и дополнениями в условия, которые время от времени может вносить Shutterstock, Inc. (“Shutterstock”) (в совокупности — “Условия” использования”). Если вы не согласны с какими-либо положениями и условиями, изложенными в Условиях использования, не пользуйтесь этим Сайтом.</li>
                        <li>Shutterstock оставляет за собой право время от времени изменять данные Условия использования. Если вы продолжаете пользование Сайтом, то подразумевается ваше согласие с подобными изменениями. Доступ и пользование вами Сайтом будут регулироваться текущей версией Условий использования, правилами и руководствами, размещенными на Сайте во время использования его вами. Регулярно проверяйте ссылку “Условия использования” на главной странице shutterstock.com, чтобы убедиться в том, что вам известна последняя версия Условий. Если вы нарушите любые из Условий использования, ваша лицензия на доступ к сайту или его использование будет автоматически приостановлена.</li>
                        <p className='list-head'>ИНТЕЛЛЕКТУАЛЬНАЯ СОБСТВЕННОСТЬ; ОГРАНИЧЕННАЯ ЛИЦЕНЗИЯ ДЛЯ ПОЛЬЗОВАТЕЛЕЙ</p>
                        <li>Весь контент на данном Сайте, включая, в частности, Изображения, Видео, Музыку и соответствующие метаданные (в совокупности — “Контент Shutterstock”), а также подборка и форма размещения Контента Shutterstock защищены законами об авторских правах, товарными знаками, патентами, коммерческими тайнами и другими законами и договорами, относящимися к интеллектуальной собственности. Любое несанкционированное использование Контента Shutterstock может нарушать эти законы, а также Условия использования. За исключением случаев, прямо оговоренных в этом документе или в отдельных лицензионных соглашениях между вами и Shutterstock, Shutterstock не дает никаких выраженных или подразумеваемых разрешений на пользование Сайтом, Услугами Сайта (определенными ниже) или любым Контентом Shutterstock. Вы обязуетесь воздержаться от следующих действий по отношению к Сайту и к любому Контенту Shutterstock: копирование</li>
                        <li>Весь контент на данном Сайте, включая, в частности, Изображения, Видео, Музыку и соответствующие метаданные (в совокупности — “Контент Shutterstock”), а также подборка и форма размещения Контента Shutterstock защищены законами об авторских правах, товарными знаками, патентами, коммерческими тайнами и другими законами и договорами, относящимися к интеллектуальной собственности. Любое несанкционированное использование Контента Shutterstock может нарушать эти законы, а также Условия использования. За исключением случаев, прямо оговоренных в этом документе или в отдельных лицензионных соглашениях между вами и Shutterstock, Shutterstock не дает никаких выраженных или подразумеваемых разрешений на пользование Сайтом, Услугами Сайта (определенными ниже) или любым Контентом Shutterstock. Вы обязуетесь воздержаться от следующих действий по отношению к Сайту и к любому Контенту Shutterstock: копирование</li>
                    </ol>
                </div>
                <div className='contract-button'>
                    <span className='enter__link' onClick={() => {
                        off('contract'),
                        on('registrationsecond')
                    }}>Назад к регистрации</span>
                </div>
            </div>
        )
    }
}
