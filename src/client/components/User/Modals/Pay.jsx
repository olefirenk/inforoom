import React, {Component} from 'react'

export default class Pay extends Component {

    render() {
        const {off} = this.props
        console.log(off)
        return (
            <div className='pay open'>
                <div onClick={() => {
                    off('shadowblock'),
                    off('pay')
                }} className='pay_close'>
                    <img src={require('core/assets/images/icon/close.png')}/>
                </div>
                <h2>Выберите способ оплаты</h2>
                <div className='pay__method'>
                    <button className='pay__method__visa'>
                        <img src={require('core/assets/images/icon/VISA.png')}/>
                    </button>
                    <button className='pay__method__privat'>
                        <img src={require('core/assets/images/icon/privat.png')}/>
                    </button>
                    <button className='pay__method__webmoney'>
                        <img src={require('core/assets/images/icon/WM.png')}/>
                    </button>
                </div>
            </div>
        )
    }
}
