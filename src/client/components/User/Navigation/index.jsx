import React, {Component} from 'react'
import {Link} from 'react-router'

export default class Navigation extends Component {

    render() {
        return (
            <div>
              <div className='userpage-navigation'>
                <Link to='/user/profile' activeClassName='active'>
                  <i></i>Мой профиль
                </Link>
                <Link to='/user/annonce' activeClassName='active'>
                  <i></i>
                  Мои анонсы
                </Link>
                <Link to='/user/translation' activeClassName='active'>
                  <i></i>Мои трансляции</Link>
                <Link to='/user/watch' activeClassName='active'>
                  <i></i>Буду смотреть</Link>
                <Link to='/user/subscribe' activeClassName='active'>
                  <i></i>Мои подписки</Link>
                <Link to='/user/liked' activeClassName='active'>
                  <i></i>Избранное</Link>
                </div>
            </div>
        )
    }
}
