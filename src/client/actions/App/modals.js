import {openModal, closeModal} from 'redux-modals-state'

export function onModal(name) {
    return openModal(name)
}

export function offModal(name) {
    return closeModal(name)
}


export function toggleModal(state, name) {
      if (state) {
          return offModal(`${name}`)
      } else {
          return onModal(`${name}`)
      }
  }
