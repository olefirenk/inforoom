import { redux as fetchData } from 'react-security-fetcher'

export const getAll = () => async (dispatch) => {

    try {
        return await fetchData('/rubrics', 'rubrics')(dispatch)
    }
    catch (e) {

        console.log('get rubrics err', e);
        throw e
    }
}
