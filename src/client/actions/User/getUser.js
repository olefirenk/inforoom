import {redux as fetchData} from 'react-security-fetcher'

export const getUser = (id) => async (dispatch) => {
    try {
        return await fetchData(`/users/${id}`, 'user')(dispatch)
    }
    catch (e) {

        console.log('get user err', e);
        throw e
    }
}
