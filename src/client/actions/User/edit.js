import {fetchData} from 'react-security-fetcher'
import moment from 'moment'
import { SubmissionError } from 'redux-form'
import {Map} from 'immutable'

export const edit = (id, form) => async (dispatch) => {
    try {

        const formImBirthday = Map(form).update('birthday', (value) => moment(value, 'DD/MM/YYYY').format())

        let formImPhones
        let formIm

        if (formImBirthday.has('phones')) {
            formImPhones = formImBirthday.update('phones', (value)=>value.filter(phone=>{return phone != null}))
            formIm = formImPhones
            if (formImBirthday.has('city')) {
                formIm = formImPhones.update('city', (value) => {
                    if (value) {
                        return `/api/cities/${value.id}`
                    } else {
                        return value
                    }
                })
            }
        } else {
            formIm = formImBirthday
        }

        dispatch({
             type: 'ACCOUNT_SUCCESS',
             payload: await fetchData(`/users/${id}`, 'PUT', {params: formIm})
         })

    }
    catch (e) {
        throw new SubmissionError({_error: 'Неправильный формат данных'})
    }
}
