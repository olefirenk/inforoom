import {redux as fetchData} from 'react-security-fetcher'

export const usersSubscribe = () => async (dispatch) => {
    try {
        return await fetchData('/users?followerUsers=true', 'users')(dispatch)
    }
    catch (e) {

        console.log('get user err', e);
        throw e
    }
}
