import {redux as fetchData} from 'react-security-fetcher'

export const getAll = () => async (dispatch) => {
    try {
        return await fetchData('/users', 'users')(dispatch)
    }
    catch (e) {

        console.log('get users err', e);
        throw e
    }
}
