import {fetchData} from 'react-security-fetcher'
import {USER_SUBSCRIBE, USER_UNSUBSCRIBE} from 'constants/User/User'

export const subscribe = (id) => async (dispatch) => {
    try {
        const user = await fetchData(`/users/${id}/follow`, 'GET')
        dispatch({
             type: USER_SUBSCRIBE
         })
         return user
    }
    catch (e) {
      throw e
    }
}

export const unsubscribe = (id) => async (dispatch) => {
    try {
        const user = await fetchData(`/users/${id}/unfollow`, 'GET')
        dispatch({
             type: USER_UNSUBSCRIBE
         })
         return user
    }
    catch (e) {
      throw e
    }
}
