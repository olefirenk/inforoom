import {redux as fetchData} from 'react-security-fetcher'
import moment from 'moment'

export const loadStreamsForDay = (day=moment(),page=1,limit=25) => async (dispatch) => {
    return  await fetchData('/streams','streamsCalendar','GET', {params: {
    ['startsAt[after]']: encodeURIComponent(moment(day).startOf('day').format()),
    ['startsAt[before]']: encodeURIComponent(moment(day).endOf('day').format()),
    limit,
    page
  }})(dispatch)
}

export const loadCalendarData = (month=moment()) => async (dispatch) => {
    return  await fetchData('/streams/date','streamsByChosenMonth','GET', {params: {

    ['startsAt[after]']: encodeURIComponent(moment(month).startOf('month').format()),
    ['startsAt[before]']: encodeURIComponent(moment(month).endOf('month').format())
  }})(dispatch)
}
