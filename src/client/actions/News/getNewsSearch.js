import {redux as fetchData} from 'react-security-fetcher'

export const getNewsSearch = (form) => async (dispatch) => {
    try {
        return await fetchData('/news', 'news', 'GET', {params: {
            search: form.query,
            ['order[createdAt]']: 'desc'
        }})(dispatch)
    }
    catch (e) {

        console.log('get news err', e);
        throw e
    }
}
