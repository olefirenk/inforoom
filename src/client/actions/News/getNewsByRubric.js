import {redux as fetchData} from 'react-security-fetcher'

export const getNewsByRubric =()=> async ({fetchData,dispatch,parameters}) => {
  console.log('parameters',parameters)
        try {
        return await fetchData('/news', 'newsByRubric', 'GET', {params: {
            type: 'web-cast',
            ['order[createdAt]']: 'desc',
            rubric: parameters.id,
            page: 1,
            limit: 10
        }})(dispatch)
    }
    catch (e) {
        console.log('get streams err', e);
        throw e
    }
}
