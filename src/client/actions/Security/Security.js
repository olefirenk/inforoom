import { Auth, fetchData } from 'react-security-fetcher'
import {SubmissionError} from 'redux-form'

export const register = (form) => async (dispatch) => {
    try{
        let user = await fetchData('/users', 'POST', {params: form});

        const response = await fetchData('/login_check', 'POST', {params: {_username: form.email, _password: form.plainPassword}})
        Auth.setToken(response.token)
        Auth.setRefreshToken(response.refresh_token)
        dispatch({
           type: 'ACCOUNT_SUCCESS',
           payload: user
      })
    }
    catch (e){
        dispatch({
            type: 'ACCOUNT_ERROR',
            payload: e
        })
        throw new SubmissionError({_error: 'Ошибка при регистрации'})
    }
}

export const login = (form) => async (dispatch) => {
    try {
        const response = await fetchData('/login_check', 'POST', {params: form})
        Auth.setToken(response.token)
        Auth.setRefreshToken(response.refresh_token)
        await dispatch(getAccount())
    }
    catch (e) {
        dispatch({
            type: 'LOGIN_ERROR',
            payload: e
        })
        throw new SubmissionError({_error: 'Неправильный логин или пароль'})
    }

}

export const getAccount = () => async (dispatch) => {
    if(!Auth.isAuthenticated()){
        return;
    }
    const response = await fetchData('/users-self')

    try {
        dispatch({
            type: 'ACCOUNT_SUCCESS',
            payload: response
        })
    }
    catch (e){
        dispatch({
            type: 'ACCOUNT_ERROR',
            payload: e
        })
        throw e
    }

}

export const logout = () => (dispatch) => {
    Auth.logout()
    dispatch({
        type: 'LOGOUT_SUCCESS'
    })
}
