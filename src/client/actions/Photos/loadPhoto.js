import * as constants from '../../constants/Photos/Photos'
import { fetchData } from 'react-security-fetcher'

export const loadPhoto = (payload) => async (dispatch) => {

    try {
        let photo = await fetchData('/_uploader/gallery/upload', 'POST', {params: payload, type: 'form-data'})
        dispatch({
            type: constants.UPLOAD_PHOTO_REQUEST
        })
        return photo
    }
    catch (e) {

        console.log('post photo err', e);
        throw e.error
    }
}
