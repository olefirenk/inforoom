import {redux, fetchData} from 'react-security-fetcher'

export const loadData = ({url}, {type, page, limit}, form) => async () => {
    try {
        return await fetchData(`${url}`, 'GET', {params: {
            type,
            ['order[startsAt]']: type ? 'desc': null,
            page,
            limit,
            search: form.query
        }})
    }
    catch (e) {
        console.log(`get ${url} err`, e)
        throw e
    }
}

export const loadDataRedux = ({url, key}, {type, page, limit}, form) => async (dispatch) => {
    try {
        return await redux(`${url}`, key, 'GET', {params: {
            type,
            ['order[startsAt]']: type ? 'desc': null,
            page,
            limit,
            search: form.query
        }})(dispatch)
    }
    catch (e) {

        console.log(`get ${url} err`, e)
            throw e
        }
    }
