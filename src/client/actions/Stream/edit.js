import {fetchData} from 'react-security-fetcher'
import { SubmissionError } from 'redux-form'
import moment from 'moment'
import {Map} from 'immutable'

export const edit = (id, form) => async () => {

    for (let key in form) {
        if (form[key] == null) {
            delete form[key]
        }
    }

    let formIm
    if (form.startsAt === '') {
        formIm = Map(form)
    } else {
        formIm = Map(form).update('startsAt', (value) => moment(value, 'DD/MM/YYYY hh:mm').format())
    }

    try {
        return await fetchData(`/streams/${id}`, 'PUT', {params: formIm})
    }
    catch (e) {
        throw new SubmissionError({_error: 'Неправильный формат данных'})
    }
}
