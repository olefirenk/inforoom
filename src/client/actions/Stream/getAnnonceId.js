import {redux as fetchData} from 'react-security-fetcher'

export const getAnnonceId = (id) => async (dispatch) => {
    try {
        return await fetchData(`/streams/${id}`, 'annonceId', 'GET', {params: {
              type: 'preview',
              ['order[startsAt]']: 'asc'
          }})(dispatch)

    }
    catch (e) {

        console.log('get annonces err', e);
        throw e
    }
}
