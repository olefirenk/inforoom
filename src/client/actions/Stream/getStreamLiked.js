import {redux as fetchData} from 'react-security-fetcher'

export const getStreamLiked = (id) => async (dispatch) => {
      await fetchData('/streams', 'streamLiked', 'GET', {params: {
          type: 'web-cast',
          ['order[startsAt]']: 'desc',
          followedUsers: `${id}`
      }})(dispatch)
}
