import {redux as fetchData} from 'react-security-fetcher'

export const getStreamUser = (id) => async (dispatch) => {
    try {
        return await fetchData('/streams', 'streams', 'GET', {params: {
            type: 'web-cast',
            ['order[startsAt]']: 'desc',
            createdBy: `${id}`
        }})(dispatch)
    }
    catch (e) {

        console.log('get streams err', e);
        throw e
    }
}
