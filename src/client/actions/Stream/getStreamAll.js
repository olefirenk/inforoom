import {redux as fetchData} from 'react-security-fetcher'

export const getStreamAll = () => async (dispatch) => {
    try {
        return await fetchData('/streams', 'streams', 'GET', {params: {
            type: 'web-cast',
            ['order[startsAt]']: 'desc'
        }})(dispatch)
    }
    catch (e) {

        console.log('get streams err', e);
        throw e
    }
}
