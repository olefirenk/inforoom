import {redux as fetchData} from 'react-security-fetcher'
import moment from 'moment'
import { SubmissionError } from 'redux-form'
import {Map} from 'immutable'

export const create = (form) => async (dispatch) => {
    let formIm
    if (form.startsAt === '') {
        form.startsAt = moment().add(1, 'm').format()
        formIm = Map(form)
        console.log(formIm.get('startsAt'))
    } else {
        formIm = Map(form).update('startsAt', (value) => moment(value, 'DD/MM/YYYY hh:mm').format())
    }

    try {
        return await fetchData('/streams', 'streamLive', 'POST', {params: formIm})(dispatch)
    }
    catch (e) {

        console.log('post stream err', e)
        throw new SubmissionError({_error: 'Неправильный формат данных'})
    }
}
