import {redux, fetchData} from 'react-security-fetcher'

export const getAnnonceSearch = (page, limit, form) => async () => {
  try {
    return await fetchData('/streams', 'GET', {params: {
        type: 'preview',
        search: form.query,
        page,
        limit,
        ['order[startsAt]']: 'asc'
      }})
  }
  catch (e) {

        console.log('get annonce err', e);
        throw e
  }
}

export const getAnnonceSearchRedux = (page, limit, form) => async (dispatch) => {
  try {
    return await redux('/streams', 'annonces', 'GET', {params: {
        type: 'preview',
        search: form.query,
        page,
        limit,
        ['order[startsAt]']: 'asc'
      }})(dispatch)
  }
  catch (e) {

        console.log('get annonce err', e);
        throw e
  }
}
