import {redux as fetchData} from 'react-security-fetcher'

export const getAnnonceUser = (id) => async (dispatch) => {
    try {
        return await fetchData('/streams', 'annonceSearch', 'GET', {params: {
            type: 'preview',
            createdBy: `${id}`,
            ['order[startsAt]']: 'asc'
        }})(dispatch)
    }
    catch (e) {

        console.log('get annonces err', e);
        throw e
    }
}
