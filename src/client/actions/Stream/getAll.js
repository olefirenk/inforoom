import {redux as fetchData} from 'react-security-fetcher'

export const getAll = () => async (dispatch) => {
    try {
        return await fetchData('/streams', 'annonces', 'GET')(dispatch)
    }
    catch (e) {

        console.log('get streams err', e);
        throw e
    }
}
