import {redux as fetchData} from 'react-security-fetcher'

export const getStreamSimilar = (id) => async (dispatch) => {
    try {
        return await fetchData('/streams', 'streamsSimilar', 'GET', {params: {
            type: 'web-cast',
            ['order[startsAt]']: 'desc',
            rubric: `${id}`
        }})(dispatch)
    }
    catch (e) {

        console.log('get streamsSimilar err', e);
        throw e
    }
}
