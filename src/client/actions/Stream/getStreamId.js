import {redux as fetchData} from 'react-security-fetcher'

export const getStreamId = (id) => async (dispatch) => {
    try {
        return await fetchData(`/streams/${id}`, 'streamId', 'GET', {params: {
              type: 'web-cast'
          }})(dispatch)

    }
    catch (e) {

        console.log('get streams err', e);
        throw e
    }
}
