import {redux, fetchData} from 'react-security-fetcher'

export const streamPlay = (id) => async (dispatch) => {
    try {
        return await redux(`/streams/${id}/play`, 'streamPlay', 'GET')(dispatch)
    }
    catch (e) {

        console.log('stream Play err', e);
        throw e
    }
}

export const streamStop = (id) => async (dispatch) => {
    try {
        return await fetchData(`/streams/${id}/play-done`, 'GET')(dispatch)
    }
    catch (e) {

        console.log('stream Stop err', e);
        throw e
    }
}
