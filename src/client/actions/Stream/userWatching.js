import {fetchData} from 'react-security-fetcher'
import {USER_WATCHING, USER_UNWATCHING} from 'constants/User/User'

export const watching = (idAnnonce) => async (dispatch) => {
    try {
        const streams = await fetchData(`/streams/${idAnnonce}/follow`, 'GET')
        dispatch({
             type: USER_WATCHING
         })
         return streams
    }
    catch (e) {
      throw e
    }
}

export const unwatching = (idAnnonce) => async (dispatch) => {
    try {
        const streams = await fetchData(`/streams/${idAnnonce}/unfollow`, 'GET')
        dispatch({
             type: USER_UNWATCHING
         })
         return streams
    }
    catch (e) {
      throw e
    }
}
