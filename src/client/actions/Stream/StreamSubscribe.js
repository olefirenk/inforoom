import {fetchData} from 'react-security-fetcher'
import {STREAM_SUBSCRIBE, STREAM_UNSUBSCRIBE} from 'constants/Stream'

export const StreamSubscribe = (id) => async (dispatch) => {
    try {
        const streams = await fetchData(`/streams/${id}/follow`, 'GET')
        dispatch({
             type: STREAM_SUBSCRIBE
         })
         return streams
    }
    catch (e) {
      throw e
    }
}

export const StreamUnsubscribe = (id) => async (dispatch) => {
    try {
        const streams = await fetchData(`/streams/${id}/unfollow`, 'GET')
        dispatch({
             type: STREAM_UNSUBSCRIBE
         })
         return streams
    }
    catch (e) {
      throw e
    }
}
