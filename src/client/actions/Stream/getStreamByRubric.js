import {redux as fetchData} from 'react-security-fetcher'

export const getStreamByRubric =()=> async ({fetchData,dispatch,parameters}) => {
        try {
        return await fetchData('/streams', 'streamsByRubric', 'GET', {params: {
            type: 'web-cast',
            ['order[startsAt]']: 'desc',
            rubric: parameters.id
        }})(dispatch)
    }
    catch (e) {
        console.log('get streams err', e);
        throw e
    }
}
