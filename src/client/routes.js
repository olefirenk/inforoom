import React from 'react'
import {Router, Route, IndexRoute, IndexRedirect, browserHistory} from 'react-router'
import Layout from 'components/Layout'

import Main from 'pages/Main'

import User from 'pages/User'
import UserProfile from 'pages/User/Profile'
import UserAnnonce from 'pages/User/Annonce'
import UserTranslation from 'pages/User/Translation'
import UserSubscribe from 'pages/User/Subscribe'
import UserWatch from 'pages/User/Watch'
import UserLiked from 'pages/User/Liked'

import Profile from 'pages/Profile'

import Search from '../client/pages/Search'
import SearchTranslation from '../client/pages/Search/Translation'
import SearchUser from 'pages/Search/User'
import SearchNew from '../client/pages/Search/News'
import SearchAnnonce from 'pages/Search/Annonces'

import Annonceid from 'pages/AnnonceId'
import StreamId from 'pages/StreamId'
import Newsid from 'pages/NewsId'

import Rubrics from 'pages/Rubrics'
import Rubric from './pages/Rubrics/Rubric'

import News from '../client/pages/News'
import Streams from '../client/pages/Streams'
import Annonces from '../client/pages/Annonces'
// import Users from '../client/pages/Users'
import Calendar from '../client/pages/Search/Calendar'

export default function() {
    return (
        <div>
          <Router history={browserHistory}>
            <Route path='/' component={Layout}>
              <IndexRoute component={Main}/>
              <Route path='user' component={User}>
                <IndexRedirect to='profile' />
                <Route path='profile' component={UserProfile}/>
                <Route path='annonce' component={UserAnnonce} />
                <Route path='translation' component={UserTranslation} />
                <Route path='subscribe' component={UserSubscribe}/>
                <Route path='watch' component={UserWatch}/>
                <Route path='liked' component={UserLiked}/>
              </Route>
              <Route path='search' component={Search}>
                <IndexRedirect to='translation' />
                <Route path='translation' component={SearchTranslation}/>
                <Route path='user' component={SearchUser}/>
                <Route path='news' component={SearchNew}/>
                <Route path='annonce' component={SearchAnnonce}/>
                <Route path='calendar' component={Calendar}/>
              </Route>
              <Route path='news' component={News}/>
              <Route path='streams' component={Streams}/>
              <Route path='annonces' component={Annonces}/>
              {/* <Route path='users' component={Users}/> */}
              <Route path='id:id' component={Profile}/>
              <Route path='annonce/:id' component={Annonceid}/>
              <Route path='stream/:id' component={StreamId}/>
              <Route path='news/:id' component={Newsid}/>
              <Route path='rubrics' component={Rubrics}>
                <Route path=':id' component={Rubric}/>
              </Route>
            </Route>
          </Router>
        </div>
    )
}
