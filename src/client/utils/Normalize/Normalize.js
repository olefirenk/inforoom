export const normalizeName = (value) => {
  if (!value) {
    return value
  }
  const onlyLetter = value.replace(/\d/, '')

  return onlyLetter

}

export const normalizePassword = (value) => {
  if (!value) {
    return value
  }
  const onlyLetter = value.replace(/\s/, '')

  return onlyLetter

}

export const normalizeNum = (value) => {
  if (!value) {
    return value
  }
  const onlyLetter = value.replace(/[^\d/\/]/, '')

  return onlyLetter

}

export const normalizeDateTime = (value) => {
  if (!value) {
    return value
  }
  const onlyLetter = value.replace(/[^\d/\/:/]/, '')

  return onlyLetter

}

export const normalizePhone = (value) => {
  if (!value) {
    return value
  }
  const onlyLetter = value.replace(/[^\d\+]/, '')

  return onlyLetter

}
