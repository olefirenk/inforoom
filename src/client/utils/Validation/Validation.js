export const DateValidation = /^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/
export const EmailValidation = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
export const UrlValidation = /\(?(?:(http|https):\/\/)(?:((?:[^\W\s]|\.|-|[:]{1})+)@{1})?((?:www.)?(?:[^\W\s]|\.|-)+[\.][^\W\s]{2,4}|localhost(?=\/)|\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})(?::(\d*))?([\/]?[^\s\?]*[\/]{1})*(?:\/?([^\s\n\?\[\]\{\}\#]*(?:(?=\.)){1}|[^\s\n\?\[\]\{\}\.\#]*)?([\.]{1}[^\s\?\#]*)?)?(?:\?{1}([^\s\n\#\[\]]*))?([\#][^\s\n]*)?\)?/
export const PhoneValidation = /^[0-9\-\+]{9,15}$/

export const LengthValidation = (value, min, max) => {
    if ((value != null) && (value !== '') && ((value.length > max) || (value.length < min))) {
      return true
  }
}

export const PasswordValidation = (value, min) => {
    if ((value != null) && (value !== '') && (value.length < min)) {
      return true
  }
}

export const PasswordValidationRequeired = (value, min, max) => {
    if ( (value != null) && (value !== '') && (value.length < max) && (value.length >= min)) {
      return true
  }
}

export const PhoneValid = (value) => {
    if ((value != null) && (value !== '') && (!PhoneValidation.test(value))) {
      return true
  }
}
