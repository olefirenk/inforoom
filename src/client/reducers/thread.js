import {fromJS} from 'immutable'

export const thread = (state = {}, action) => {
    switch (action.type){
        case 'thread/loadList':
            return fromJS(action.payload)
        case 'thread/unshiftPost':{
            const stateUpdate = state
                                    .update('Items', (list) => {
                                        return list.push(fromJS(action.payload))
                                    })
                                    .update('totalItems', (value)=> {return value + 1})

            return stateUpdate
        }
        case 'thread/likePost':{
            let indexOf
            const stateUpdate = state.update('Items', (list) => {

                const post = list
                                .find((item, index)=>{
                                    indexOf = index
                                    return item.get('id') == action.payload
                                })
                                .set('follow', true)
                                .update('likes', (value)=> {return value + 1})

                return list.set(indexOf, post)
            })

            return stateUpdate

        }
        case 'thread/unlikePost':{
            let indexOf
            const stateUpdate = state.update('Items', (list) => {

                const post = list
                                .find((item, index)=>{
                                    indexOf = index
                                    return item.get('id') == action.payload
                                })
                                .set('follow', false)
                                .update('likes', (value)=> {return value - 1})

                return list.set(indexOf, post)
            })

            return stateUpdate

        }
        default:
            return state

    }
}
