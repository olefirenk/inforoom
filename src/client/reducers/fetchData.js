import {fetchData as fetchDataBase} from 'react-security-fetcher/reducers'
import {fromJS} from 'immutable'

export const fetchData = (state = {}, action) => {
    switch (action.type){
        case '@FETCH_DATA/REQUEST':
            return fetchDataBase(state, action)
        case '@FETCH_DATA/SUCCESS':
            return fetchDataBase(state, action)
        case '@FETCH_DATA/ERROR':
            return fetchDataBase(state, action)
        case '@FETCH_DATA/STREAM_UNLIKE':
            return {...state, streamLiked: {...state.streamLiked, response: {Items : state.streamLiked.response.Items.filter((stream) => {
                        return stream.id != action.payload
            })}}}

        case '@FETCH_DATA/ADD_LIST': {

            const {data:{Items, totalItems}, key, params} = action.payload
            const stateUpdate = fromJS(state)
                  .updateIn([`${key}`, 'response', 'Items'], (list)=>{
                      return list.concat(Items)
                  })
                  .setIn([`${key}`, 'response', 'totalItems'], totalItems)
                  .setIn([`${key}`, 'request', 'options', 'params'], params)
                  .toJS()

            return stateUpdate
        }
        default:
            return state
    }
}
