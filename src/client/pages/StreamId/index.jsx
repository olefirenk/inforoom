import React, {Component} from 'react'
import {preload} from 'react-isomorphic-render/redux'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import Translation from 'components/User/Translation'
import TranslationLast from 'components/User/TranslationLast'
import TranslationSimilar from 'components/User/TranslationSimilar'
import Comments from 'components/Comments'
import {subscribe, unsubscribe}  from 'actions/User/userSubscribe'
import {getStreamId} from 'actions/Stream/getStreamId'
import {StreamSubscribe,StreamUnsubscribe} from 'actions/Stream/StreamSubscribe'
import {getStreamUser} from 'actions/Stream/getStreamUser'
import {getStreamSimilar} from 'actions/Stream/getStreamSimilar'

@preload( async ({dispatch, parameters, getState, fetchData }) => {
    await dispatch(fetchData(`/streams/${parameters.id}`, 'streamId', 'GET', {params: {
        type: 'web-cast'
    }}))
    await dispatch(fetchData('/streams', 'streamsLast', 'GET', {params: {
        type: 'web-cast',
        ['order[startsAt]']: 'desc',
        createdBy: `${getState().fetchData.streamId.response.createdBy.id}`,
        page: 1,
        limit: 2
    }}))
    await dispatch(fetchData('/streams', 'streamsSimilar', 'GET', {params: {
        type: 'web-cast',
        ['order[startsAt]']: 'desc',
        rubric: `${getState().fetchData.streamId.response.rubric.id}`,
        page: 1,
        limit: 2
    }}))
})

@connect((state) => ({
    streamsLast: state.fetchData.streamsLast.response || {},
    streamId: state.fetchData.streamId.response || {},
    streamsSimilar: state.fetchData.streamsSimilar.response || {},
    }), (dispatch) => ({
    actions: bindActionCreators({
        subscribe,
        unsubscribe,
        getStreamId,
        StreamSubscribe,
        StreamUnsubscribe,
        getStreamUser,
        getStreamSimilar
    }, dispatch)
}))

export default class StreamId extends Component {

    render() {

        const {streamId, actions, streamsLast, streamsSimilar} = this.props

        return (
            <div>
                <div className='head-container'>
                    <div className='streamid-column1'>
                        <Translation data={streamId} actions={actions} type='streamId' />
                        <Comments threadId={(streamId && streamId.thread) ? streamId.thread.id:null} />
                    </div>
                    <div className='streamid-column2'>
                        <TranslationLast streams={streamsLast} data={streamId} actions={actions} />
                        <TranslationSimilar streams={streamsSimilar} data={streamId} actions={actions} />
                    </div>
                </div>
            </div>
        )
    }
}
