import React, {Component} from 'react'
import Stream from 'components/Common/User/StreamOther'

export default class List extends Component {
    render() {

        const { streams, actions, user} = this.props

        return (
            <div className='channel__translation'>
                <div className='channel__translation__head'>
                    <p>Трансляции</p>
                </div>
                {streams.totalItems != 0 ?
                    <div>
                        <div className='channel__translation__main'>
                            {streams ?
                                streams.Items.map((stream,key) => {
                                    return <Stream
                                      key={key}
                                      actions={actions}
                                      data={stream}
                                      idUser={user.id}
                                      />
                                }):
                                null
                            }
                        </div>
                        {/*<div className='channel__translation__all'>
                            <button>Все трансляции</button>
                        </div>*/}
                    </div>
                : <p className='is--available'>На данный момент нет трансляций...</p>
                }
            </div>
        )
    }
}
