import React, {Component} from 'react'
import {preload} from 'react-isomorphic-render/redux'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {onModal, offModal} from 'actions/App/modals'
import {getAnnonceUser} from 'actions/Stream/getAnnonceUser'
import StreamsList from './Streams/List'
import AnnonceList from './Annonce/List'
import LikedList from './Liked/List'
import UserInfo from './User/Info'
import UserAction from './User/Action'
import {getUser} from 'actions/User/getUser'
import {getAll as getUsers} from 'actions/User/getAll'
import {subscribe, unsubscribe}  from 'actions/User/userSubscribe'
import {watching, unwatching}  from 'actions/Stream/userWatching'
import {getAnnonceAll} from 'actions/Stream/getAnnonceAll'
import {getStreamAll} from 'actions/Stream/getStreamAll'
import {StreamSubscribe,StreamUnsubscribe} from 'actions/Stream/StreamSubscribe'
import {getStreamUser} from 'actions/Stream/getStreamUser'
import {getStreamLiked} from 'actions/Stream/getStreamLiked'


@preload(({dispatch, parameters, fetchData }) => Promise.all([
    dispatch(fetchData('/streams', 'streams', 'GET', {params: {
        type: 'web-cast',
        ['order[startsAt]']: 'desc',
        createdBy: `${parameters.id}`,
        page: 1,
        limit: 9
    }})),
    dispatch(fetchData('/streams', 'streamLiked', 'GET', {params: {
        type: 'web-cast',
        ['order[startsAt]']: 'desc',
        followedUsers: `${parameters.id}`,
        page: 1,
        limit: 9
    }})),
    dispatch(fetchData('/streams', 'annonceSearch', 'GET', {params: {
        type: 'preview',
        ['order[startsAt]']: 'asc',
        createdBy: `${parameters.id}`,
        page: 1,
        limit: 9
    }})),
    dispatch(fetchData(`/users/${parameters.id}`, 'user')),
]))

@connect((state) => ({
    streams: state.fetchData.streams.response,
    streamLiked : state.fetchData.streamLiked.response,
    annonceSearch: state.fetchData.annonceSearch.response,
    user: state.fetchData.user.response
    }), (dispatch) => ({
    actions: bindActionCreators({
      subscribe,
      unsubscribe,
      watching,
      unwatching,
      getAnnonceUser,
      getAnnonceAll,
      getStreamAll,
      StreamSubscribe,
      StreamUnsubscribe,
      getStreamUser,
      getStreamLiked,
      onModal,
      offModal,
      getUser,
      getUsers
    }, dispatch)
}))

export default class Profile extends Component {
    render() {

        const {streams, streamLiked, annonceSearch, actions, user} = this.props

        return (
            <div>
                <div className='menu-user main-container'>
                    <UserInfo data={user} />
                    <div className='menu-user__channel'>
                        <div className='channel__background'>
                            <img className='logo' src={require('images/channel.jpg')}/>
                        </div>
                        <UserAction user={user} actions={actions}/>
                        <StreamsList streams={streams} actions={actions} user={user}/>
                        <AnnonceList user={user} annonces={annonceSearch} actions={actions}/>
                        <LikedList streams={streamLiked} actions={actions} user={user} type='profile' />
                    </div>
                </div>
            </div>
        )
    }
}
