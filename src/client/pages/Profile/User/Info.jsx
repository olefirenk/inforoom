import React, {Component} from 'react'
import {Link} from 'react-router'
import moment from 'moment'

export default class UserInfo extends Component {
    render() {

        const {data} =this.props

        return (
            <div className='menu-user__info'>
                <div className='menu-user__info__onsite menu-user__info__block'>
                  <p>
                    На сайте<br/>
                    c {moment(data.createdAt).locale('ru').format('LL')} ({moment(data.createdAt, 'YYYYMMDD').locale('ru').fromNow(true)})
                  </p>
                </div>
                {data.about ?
                    <div className='menu-user__info__about menu-user__info__block'>
                        <p>О себе</p>
                        <p>{data.about}</p>
                    </div>:
                    null
                }
                <div className='menu-user__info__socials menu-user__info__block'>
                    <p>Ссылки</p>
                    <div className='socials-icon'>
                        <Link to=''>
                            <i className='sprite sprite-facebook'></i>
                        </Link>
                        <Link to=''>
                            <i className='sprite sprite-twitter'></i>
                        </Link>
                        <Link to=''>
                            <i className='sprite sprite-linkedin'></i>
                        </Link>
                    </div>
                    {data.website ?
                        <p>
                            <i className='sprite sprite-website2'></i>
                            <span className='website'>{data.website}</span>
                        </p>
                        : null
                    }
                </div>
                {((data.phones.length != 0) || data.publicEmail) ?
                    <div className='menu-user__info__contacts menu-user__info__block'>
                        <p>Контакты</p>
                        {data.phones ?
                            data.phones.map((phone, index)=>{
                                return (
                                    <p className='phone__block' key={index}>
                                        <i className='sprite sprite-phone2'></i>
                                        <span className='phone'>{phone}</span>
                                    </p>
                                )
                            }):
                            null
                        }
                        {data.publicEmail ?
                            <p className='email__block'>
                                <i className='sprite sprite-email2'></i>
                                <span className='email'>{data.publicEmail}</span>
                            </p>
                            : null
                        }
                    </div>
                    : null
                }
            </div>
        )
    }
}
