import React, {Component} from 'react'
import SocialButton from 'pages/SocialButton'
import Subscribe from 'components/Common/Buttons/Subscribe'
import noFoto from 'images/icon/registration.png'
import {BASE_URL} from 'constants/App/App.js'

export default class UserAction extends Component {

    render() {

        const {user, actions} =this.props

        return (
            <div className='channel__subscribe'>
                <i><img className='logo' src={user.avatarPhoto ? `${BASE_URL}${user.avatarPhoto.path}`:noFoto}/></i>
                <div className='channel__subscribe__name'>
                    <p className='author'>{user.firstname} {user.lastname}</p>
                    {user.city ?
                        <p className='location'>
                            {user.city.cityName}, {user.city.country ? user.city.country.countryName: null}
                        </p>
                        :<p className='location'></p>
                    }
                    <Subscribe
                      isSubscribe={user.follow}
                      id={user.id}
                      actions={actions}
                      />
                </div>
                <div className='subscribe__info'>
                    <p>
                        <i className='sprite sprite-annonce-subscribe'></i>
                        <span className='subscribe-count'>{user.countFollowerUsers}</span>
                    </p>
                    <p>
                        <i className='sprite sprite-watch'></i>
                        <span className='comments-count'>534</span>
                    </p>
                </div>
                <SocialButton/>
            </div>
        )
    }
}
