import React, {Component} from 'react'
import Annonces from 'components/Common/User/AnnonceOther'

export default class List extends Component {

    render() {
        const {annonces, actions , user } =this.props
        return (
            <div className='channel__annonce'>
                <div className='channel__annonce__head'>
                    <p>Анонсы</p>
                </div>
                <div className='channel__annonce__wrap'>
                    {annonces.totalItems != 0 ?
                        annonces.Items.map((annonce, key) => {
                            return <Annonces
                              key={key}
                              user={user}
                              data={annonce}
                              actions={actions}/>
                      }): <p className='is--available'>На данный момент нет анонсов...</p>
                    }
                </div>
            </div>
        )
    }
}
