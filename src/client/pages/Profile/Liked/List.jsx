import React, {Component} from 'react'
import Translation from 'components/Common/User/StreamSearch'


export default class List extends Component {
    render() {
        const {streams, actions, user } = this.props
        return (
            <div className='channel__liked'>
                <div className='channel__liked__head'>
                    <p>
                        <i className='sprite sprite-liked'></i>
                        Избранное
                    </p>
                </div>
                <div className='userlikedinfo'>

                    <div className='userlikedinfo__about'>
                      {streams.totalItems != 0 ?
                          streams.Items.map((stream, key)=>{
                              return <Translation
                                        key={key}
                                        data={stream}
                                        actions={actions}
                                        idUser={user.id}
                                        type='profile'
                                        />
                                }): <p className='is--available'>На данный момент нет избранного...</p>
                      }
                    </div>
                </div>
            </div>
        )
    }
}
