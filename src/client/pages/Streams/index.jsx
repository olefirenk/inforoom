import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {preload} from 'react-isomorphic-render/redux'
import {connect} from 'react-redux'
import SearchNavigation from 'components/Search/Navigation'
import SearchList from 'components/App/Search/List'
import News from 'components/Common/User/NewsSearch'
import MoreResults from '../../components/Common/Buttons/MoreResults'
import {loadData, loadDataRedux} from 'actions/loadData'
import Immutable from 'immutable'
import {formValueSelector} from 'redux-form'
import Popular from 'components/Sidebar/Popular'
import PopularComments from 'pages/PopularComments'
import Stream from '../../components/Common/User/StreamSearch'

@preload(({dispatch, fetchData, getState }) =>Promise.all([
dispatch(fetchData('/streams', 'streamsPopular', 'GET', {params: {type: 'web-cast', ['order[viewsCount]']: 'desc'}})),
dispatch(fetchData('/streams', 'streamsComments', 'GET', {params: {type: 'web-cast', ['order[viewsCount]']: 'desc', page:1, limit:3}})),
dispatch(fetchData('/streams', 'streamsList', 'GET', {params: { type: 'web-cast', ['order[createdAt]']: 'desc',  page: 1,  limit: 9,
search: formValueSelector('streamsList')(getState(), 'query')
}}))
]))


  // search: formValueSelector('searchList')(getState(), 'query')


@connect((state) => {
    const params = Immutable.fromJS(state.fetchData.streamsList).getIn(['request', 'options', 'params'])
    const search = Immutable.fromJS(state.fetchData.streamsList).getIn(['request', 'options', 'params', 'search'])
    return {
      streamsPopular: state.fetchData.streamsPopular.response ,
      streamsComments: state.fetchData.streamsComments.response,
      streamsList: state.fetchData.streamsList.response ,
      user: state.authentication.user,
      modals: state.modals,
      rubrics: state.fetchData.rubrics,
      // streams: state.fetchData.streams.response
      // search,
      params: params || {},
    }
}, (dispatch) => ({
    actions: bindActionCreators({
      loadData,
      loadDataRedux,
    }, dispatch),
    dispatch
}))

export default class StreamsList extends Component {
  constructor(props) {
      super(props)
      this.state = {
          showResult: props.search !== '' ? true:false
      }
  }

  _showResult = () => {
      this.setState({showResult: true})
  }
    render() {
        const {streamsList,actions, modals,rubrics,user, search, params, dispatch,streamsPopular,streamsComments} = this.props
        return (
            <div className='main-container' >
              <h2 className='allnews-head'>Трансляции</h2>
              <div className='allnews-wrap all-streams-wrap' >
                <div className='stremsMore'>
                  <div className='mytranslation-wrap'>
                    <div className='search__news-wrap search__news-wrap2'>
                      {Object.keys(streamsList).length ?
                        streamsList.Items.map((stream, key)=>{
                          return(
                            <Stream
                              key={key}
                              data={stream}
                              modals={modals}
                              actions={actions}
                            />
                          )

                        })
                        :
                        null
                      }
                      <MoreResults
                        isRender={streamsList && streamsList.totalItems > 8 && streamsList.totalItems !== streamsList.Items.length}
                        params={params}
                        loadMore={actions.loadData}
                        url='/streams'
                        keyData='streamsList'
                        dispatch={dispatch}
                      />
                    </div>
                    <div className='block1'>
                      <Popular streams={streamsPopular}/>
                      <PopularComments streams={streamsComments}/>
                    </div>
                  </div>

                </div>
              </div>
            </div>
        )
    }
}
