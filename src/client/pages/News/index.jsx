import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {preload} from 'react-isomorphic-render/redux'
import {connect} from 'react-redux'
import SearchNavigation from 'components/Search/Navigation'
import SearchList from 'components/App/Search/List'
import News from 'components/Common/User/NewsSearch'
import MoreResults from 'components/Common/Buttons/MoreResults'
import {loadData, loadDataRedux} from 'actions/loadData'
import Immutable from 'immutable'
import {formValueSelector} from 'redux-form'
import Popular from 'components/Sidebar/Popular'
import PopularComments from 'pages/PopularComments'


@preload(({dispatch, fetchData, getState }) =>Promise.all([
dispatch(fetchData('/streams', 'streamsPopular', 'GET', {params: {type: 'web-cast', ['order[viewsCount]']: 'desc'}})),
dispatch(fetchData('/streams', 'streamsComments', 'GET', {params: {type: 'web-cast', ['order[viewsCount]']: 'desc', page:1, limit:3}})),
dispatch(fetchData('/news', 'news', 'GET', {params: {
  ['order[createdAt]']: 'desc',
  page: 1,
  limit: 8,
  search: formValueSelector('searchList')(getState(), 'query')
}}))]))

@connect((state) => {
    const params = Immutable.fromJS(state.fetchData.news).getIn(['request', 'options', 'params'])
    const search = Immutable.fromJS(state.fetchData.news).getIn(['request', 'options', 'params', 'search'])
    return {
      streamsPopular: state.fetchData.streamsPopular.response,
      streamsComments: state.fetchData.streamsComments.response,
      news: state.fetchData.news.response,
      search,
      params: params || {},
    }
}, (dispatch) => ({
    actions: bindActionCreators({
      loadData,
      loadDataRedux,
    }, dispatch),
    dispatch
}))

export default class NewsList extends Component {
  constructor(props) {
      super(props)
      this.state = {
          showResult: props.search !== '' ? true:false
      }
  }

  _showResult = () => {
      this.setState({showResult: true})
  }
    render() {
        const {actions, news, search, params, dispatch,streamsPopular,streamsComments} = this.props
        return (
            <div className='main-container' >
              <h2 className='allnews-head'>Новости</h2>
              <div className='allnews-wrap' >
                <div className='newsinfo'>
                  <div className='mytranslation-wrap'>
                    <div className='search__news-wrap search__news-wrap2'>
                      {Object.keys(news).length != 0 ?
                        news.Items.map((news, key)=>{
                          return <News
                            key={key}
                            data={news}
                          />
                        }):
                        null
                      }
                      <MoreResults
                        isRender={news && news.totalItems > 8 && news.totalItems !== news.Items.length}
                        params={params}
                        loadMore={actions.loadData}
                        url='/news'
                        keyData='news'
                        dispatch={dispatch}
                      />
                    </div>
                    <div className='block1'>
                      <Popular streams={streamsPopular}/>
                      <PopularComments streams={streamsComments}/>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        )
    }
}
