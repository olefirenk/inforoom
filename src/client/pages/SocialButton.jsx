import React, {Component} from 'react'

export default class SocialButton extends Component {

    render() {
        return (
            <div className='button__social'>
                <button className='button__social__fb'>
                    <i className='sprite sprite-icon-fb'></i>
                    Поделиться</button>
                <button className='button__social__tw'>
                    <i className='sprite sprite-icon-tw'></i>
                    Твитнуть</button>
                <button className='button__social__email'>
                    <i className='sprite sprite-icon-email'></i>
                    Email</button>
            </div>
        )
    }
}
