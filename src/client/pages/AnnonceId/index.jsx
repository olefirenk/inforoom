import React, {Component} from 'react'
import {preload} from 'react-isomorphic-render/redux'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {onModal, offModal} from 'actions/App/modals'
import {getAnnonceUser} from 'actions/Stream/getAnnonceUser'
import {getUser} from 'actions/User/getUser'
import {getAll as getUsers} from 'actions/User/getAll'
import {subscribe, unsubscribe}  from 'actions/User/userSubscribe'
import {watching, unwatching}  from 'actions/Stream/userWatching'
import {getAnnonceAll} from 'actions/Stream/getAnnonceAll'
import {getAnnonceId} from 'actions/Stream/getAnnonceId'
import News from 'components/Sidebar/News'
import MainAnnonceId from 'components/AnnonceId'

@preload( async ({dispatch, parameters, getState, fetchData }) => {
    await dispatch(fetchData(`/streams/${parameters.id}`, 'annonceId')),
    await dispatch(fetchData('/streams', 'streamSearch', 'GET', {params: {
        type: 'preview',
        createdBy: `${getState().fetchData.annonceId.response.createdBy.id}`,
        ['order[startsAt]']: 'desc'
    }})),
    await dispatch(fetchData('/streams', 'annonces', 'GET', {params: {
        type: 'preview',
        ['order[startsAt]']: 'desc',
        page: 1,
        limit: 3
    }})),
    await dispatch(fetchData('/news', 'newsLast', 'GET', {params: {
        ['order[createdAt]']: 'desc',
        page: 1,
        limit: 10
    }}))
})


@connect((state) => ({
    streamSearch: state.fetchData.streamSearch.response || {},
    annonces: state.fetchData.annonces.response || {},
    annonceId: state.fetchData.annonceId.response || {},
    newsLast: state.fetchData.newsLast.response || {}
    }), (dispatch) => ({
    actions: bindActionCreators({
      subscribe,
      unsubscribe,
      watching,
      unwatching,
      getAnnonceUser,
      getAnnonceAll,
      getAnnonceId,
      onModal,
      offModal,
      getUser,
      getUsers
    }, dispatch)
}))

export default class AnnonceId extends Component {

    render() {

        const {streamSearch, annonceId, annonces, actions ,newsLast} = this.props

        return (
            <div>
                <div className='head-container'>
                    <div className='block1'>
                        <News news={newsLast}/>
                    </div>
                    <MainAnnonceId streams={streamSearch} annonces={annonces} annonce={annonceId} actions={actions}/>
                </div>
            </div>
        )
    }
}
