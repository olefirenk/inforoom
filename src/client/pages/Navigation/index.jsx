import React from 'react'
import {Link} from 'react-router'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'

@connect((state) => {
    const {news = {news: {response:{}}}} = state.fetchData
    return {
        users: state.fetchData.users.response,
        streams: state.fetchData.streams.response,
        annonce: state.fetchData.annonce.response,
        news: news.response || {},
        form: state.form
    }
}, (dispatch) => ({
    actions: bindActionCreators({
    }, dispatch)
}))

export default class SearchNavigation extends React.Component {

    render() {

        const {streams, users} = this.props

        let count = {
            streams: streams.totalItems ? streams.totalItems: 0,
            news: 0,
            users: users.totalItems ? users.totalItems: 0,
            annonces: 0
        }

        return (
            <div className='search__result'>
                <div className='search__result__list'>
                    <ul>
                        <li>
                            <Link to='/search/translation' activeClassName='active'>Трансляции<sup>{count.streams}</sup>
                            </Link>
                        </li>
                        <li >
                            <Link to='/search/new' activeClassName='active'>Новости<sup>{count.news}</sup>
                            </Link>
                        </li>
                        <li>
                            <Link to='/search/annonce' activeClassName='active'>Анонсы<sup>{count.annonces}</sup>
                            </Link>
                        </li>
                        <li>
                            <Link to='/search/user' activeClassName='active'>Пользователи<sup>{count.users}</sup>
                            </Link>
                        </li>
                    </ul>
                </div>
            </div>
        )
    }
}
