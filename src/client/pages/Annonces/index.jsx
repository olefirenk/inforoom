import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {preload} from 'react-isomorphic-render/redux'
import {connect} from 'react-redux'
import SearchNavigation from 'components/Search/Navigation'
import SearchList from 'components/App/Search/List'
import News from 'components/Common/User/NewsSearch'
import MoreResults from 'components/Common/Buttons/MoreResults'
import {loadData, loadDataRedux} from 'actions/loadData'
import Immutable from 'immutable'
import {formValueSelector} from 'redux-form'
import Popular from 'components/Sidebar/Popular'
import PopularComments from 'pages/PopularComments'
import Stream from '../../components/Common/User/StreamSearch'
import Annonces from '../../components/Common/User/AnnonceOther'



@preload(({dispatch, fetchData, getState }) =>Promise.all([
dispatch(fetchData('/streams', 'streamsPopular', 'GET', {params: {type: 'web-cast', ['order[viewsCount]']: 'desc'}})),
dispatch(fetchData('/streams', 'streamsComments', 'GET', {params: {type: 'web-cast', ['order[viewsCount]']: 'desc', page:1, limit:3}})),
dispatch(fetchData('/streams', 'annoncesList', 'GET', {params: { type: 'preview', ['order[createdAt]']: 'asc',  page: 1,  limit: 9}}))
]))


  // search: formValueSelector('searchList')(getState(), 'query')


@connect((state) => {
    const params = Immutable.fromJS(state.fetchData.annoncesList).getIn(['request', 'options', 'params'])
    // const search = Immutable.fromJS(state.fetchData.news).getIn(['request', 'options', 'params', 'search'])
    return {
      streamsPopular: state.fetchData.streamsPopular.response,
      streamsComments: state.fetchData.streamsComments.response,
      annoncesList: state.fetchData.annoncesList.response,
      user: state.authentication.user,
      // modals: state.modals,
      // rubrics: state.fetchData.rubrics,
      // search,
      params: params || {},
    }
}, (dispatch) => ({
    actions: bindActionCreators({
      loadData,
      loadDataRedux,
    }, dispatch),
    dispatch
}))

export default class AnnoncesList extends Component {
  constructor(props) {
      super(props)
      this.state = {
          showResult: props.search !== '' ? true:false
      }
  }

  _showResult = () => {
      this.setState({showResult: true})
  }
    render() {
        const {annoncesList,actions, modals,rubrics,user, search, params, dispatch,streamsPopular,streamsComments} = this.props
        return (
            <div className='main-container' >
              <h2 className='allnews-head'>Анонсы</h2>
              <div className='allnews-wrap all-annonces-wrap' >
                {Object.keys(annoncesList.Items).length ? null : <p className="search_result_text">На данный момент аннонсов не найдено</p>}
                <div className='annoncesMore'>
                  <div className='mytranslation-wrap'>
                    <div className='search__news-wrap search__news-wrap2'>
                      {Object.keys(annoncesList).length ?
                        annoncesList.Items.map((annonce, key)=>{
                          return(
                            <Annonces
                              key={key}
                              // user={users}
                              data={annonce}
                              actions={actions}
                              type='SearchAnnonce'
                            />
                          )

                        })
                        :
                        null
                      }
                    </div>
                    <div className='block1'>
                      <Popular streams={streamsPopular}/>
                      <PopularComments streams={streamsComments}/>
                    </div>
                  </div>

                  <MoreResults
                    isRender={annoncesList && annoncesList.totalItems > 8 && annoncesList.totalItems !== annoncesList.Items.length}
                    params={params}
                    loadMore={actions.loadData}
                    url='/streams'
                    keyData='annoncesList'
                    dispatch={dispatch}
                  />
                </div>
              </div>
            </div>
        )
    }
}
