import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {preload} from 'react-isomorphic-render/redux'
import {connect} from 'react-redux'
import SearchNavigation from 'components/Search/Navigation'
import SearchList from 'components/App/Search/List'
import User from '../../components/Common/User'
import MoreResults from 'components/Common/Buttons/MoreResults'
import {loadData, loadDataRedux} from 'actions/loadData'
import Immutable from 'immutable'
import {formValueSelector} from 'redux-form'
import Popular from 'components/Sidebar/Popular'
import PopularComments from 'pages/PopularComments'
import Stream from '../../components/Common/User/StreamSearch'

@preload(({dispatch, fetchData, getState }) =>Promise.all([
dispatch(fetchData('/streams', 'streamsPopular', 'GET', {params: {type: 'web-cast', ['order[viewsCount]']: 'desc'}})),
dispatch(fetchData('/streams', 'streamsComments', 'GET', {params: {type: 'web-cast', ['order[viewsCount]']: 'desc', page:1, limit:3}})),
dispatch(fetchData('/users', 'usersList', 'GET', {params: { page: 1,  limit: 12,
  // search: formValueSelector('searchList')(getState(), 'query')
}}))
]))


  // search: formValueSelector('searchList')(getState(), 'query')


@connect((state) => {
    const params = Immutable.fromJS(state.fetchData.usersList).getIn(['request', 'options', 'params'])
    // const search = Immutable.fromJS(state.fetchData.news).getIn(['request', 'options', 'params', 'search'])
    return {
      streamsPopular: state.fetchData.streamsPopular.response ,
      streamsComments: state.fetchData.streamsComments.response,
      usersList: state.fetchData.usersList.response ,
      user: state.authentication.user,
      modals: state.modals,
      rubrics: state.fetchData.rubrics,
      // streams: state.fetchData.streams.response
      // search,
      params: params || {},
    }
}, (dispatch) => ({
    actions: bindActionCreators({
      loadData,
      loadDataRedux,
    }, dispatch),
    dispatch
}))

export default class UsersList extends Component {
  constructor(props) {
      super(props)
      this.state = {
          showResult: props.search !== '' ? true:false
      }
  }

  _showResult = () => {
      this.setState({showResult: true})
  }
    render() {
        const {usersList,actions, modals,rubrics,user, search, params, dispatch,streamsPopular,streamsComments} = this.props
        return (
            <div className='main-container' >
              <h2 className='allnews-head'>Пользователи</h2>
              <div className='allnews-wrap all-users-wrap' >
                <div className='userinfo'>
                  <div className='search__news-wrap search__news-wrap2 search__channel-wrap'>
                    {Object.keys(usersList).length ?
                      usersList.Items.map((user, key)=>{

                        return(
                          <User
                            key={key}
                            data={user}
                            actions={actions}
                          />
                        )

                      })
                        :
                        null
                    }
                  </div>


                  <MoreResults
                    isRender={usersList && usersList.totalItems > 8 && usersList.totalItems !== usersList.Items.length}
                    params={params}
                    loadMore={actions.loadData}
                    url='/users'
                    keyData='usersList'
                    dispatch={dispatch}
                  />

                </div>
                <div className='block1'>
                  <Popular streams={streamsPopular}/>
                  <PopularComments streams={streamsComments}/>
                </div>

              </div>
            </div>
        )
    }
}
