import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {preload} from 'react-isomorphic-render/redux'
import {connect} from 'react-redux'
import {fromJS} from 'immutable'
import {getStreamAll} from 'actions/Stream/getStreamAll'
import {StreamSubscribe,StreamUnsubscribe} from 'actions/Stream/StreamSubscribe'
import {subscribe, unsubscribe}  from 'actions/User/userSubscribe'
import {getAll as getUsers} from 'actions/User/getAll'
import {getStreamAllDesc} from 'actions/Stream/getStreamAllDesc'
import {loadData} from 'actions/loadData'
import Annonce from 'components/Sidebar/Annonce'
import Popular from 'components/Sidebar/Popular'
import PopularComments from 'pages/PopularComments'
import Translation from 'components/User/Translation'
import TranslationList from 'components/Stream/List'
import News from 'components/Sidebar/News'


@preload( ({dispatch, fetchData }) => Promise.all([
    dispatch(fetchData('/streams', 'annonces', 'GET', {params: {type: 'preview', ['order[startsAt]']: 'desc'}})),
    dispatch(fetchData('/streams', 'streamsPopular', 'GET', {params: {type: 'web-cast', ['order[viewsCount]']: 'desc'}})),
    dispatch(fetchData('/streams', 'streamsComments', 'GET', {params: {type: 'web-cast', ['order[viewsCount]']: 'desc', page:1, limit:3}})),
    dispatch(fetchData('/streams', 'streams', 'GET', {params: {type: 'web-cast', ['order[startsAt]']: 'desc', page: 1, limit: 4}})),
    dispatch(fetchData('/news', 'newsLast', 'GET', {params:{['order[createdAt]']: 'desc', page: 1, limit: 10}})),
    dispatch(fetchData('/rubrics', 'rubrics', 'GET'))
]))


@connect((state) => {
    const params = fromJS(state.fetchData.streams).getIn(['request', 'options', 'params'])
    const streams = state.fetchData.streams.response
    let streamFirst = []
    let streamList = []
    if (Object.keys(streams).length != 0 && streams.Items.length != 0) {
        streamFirst = streams.Items.slice(0,1)[0]
        streamList = streams.Items.slice(1)
    }
    return {
        streamFirst,
        streamList,
        annonces: state.fetchData.annonces.response,
        newsLast: state.fetchData.newsLast.response,
        streamsPopular: state.fetchData.streamsPopular.response,
        streamsComments: state.fetchData.streamsComments.response,
        params: params || {}
    }
}, (dispatch) => ({
    actions: bindActionCreators({
      getStreamAll,
      StreamSubscribe,
      StreamUnsubscribe,
      subscribe,
      unsubscribe,
      getUsers,
      getStreamAllDesc,
      loadData
  }, dispatch),
  dispatch
}))

export default class Main extends Component {

    render() {
      const {annonces, streamsPopular, streamFirst, streamList, streamsComments, actions, newsLast, params, dispatch} = this.props
        return (
            <div>
              <div className='head-container'>
                <div className='block1'>
                  <News news={newsLast}/>
                </div>
                <div className='block2'>
                  <Translation data={streamFirst} actions={actions}/>
                  <TranslationList data={streamList} actions={actions} params={params} dispatch={dispatch}/>
                </div>
                <div className='block3'>
                  <Annonce annonces={annonces}/>
                  <Popular streams={streamsPopular}/>
                  <PopularComments streams={streamsComments}/>
                </div>
              </div>
            </div>
        )
    }
}
