import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {preload} from 'react-isomorphic-render/redux'
import {connect} from 'react-redux'
import {getAll as getUsers} from 'actions/User/getAll'
import {loadData, loadDataRedux} from 'actions/loadData'
import {subscribe, unsubscribe}  from 'actions/User/userSubscribe'
import User from 'components/Common/User'
import SearchNavigation from '../../../components/Search/Navigation'
import SearchList from 'components/App/Search/List'
import Immutable from 'immutable'
import MoreResults from 'components/Common/Buttons/MoreResults'
import {formValueSelector} from 'redux-form'

// @preload(({dispatch, fetchData, getState }) => dispatch(fetchData('/users', 'users', 'GET', {params: {
//     page: 1,
//     limit: 12,
//     search: formValueSelector('searchList')(getState(), 'query')
// }})))
@preload(({dispatch, fetchData, getState }) =>Promise.all([
  dispatch(fetchData('/users', 'users', 'GET', {params: {
      page: 1,
      limit: '0',
      search: formValueSelector('searchList')(getState(), 'query')
  }})),
dispatch(fetchData('/streams', 'streamsCount', 'GET', {params: { type: 'web-cast',limit: '0',
search: formValueSelector('searchList')(getState(), 'query')
}})),
dispatch(fetchData('/news', 'newsCount', 'GET', {params: { type: 'web-cast',limit: '0',
search: formValueSelector('searchList')(getState(), 'query')
}})),
dispatch(fetchData('/users', 'usersCount', 'GET', {params: {  limit: '0',
search: formValueSelector('searchList')(getState(), 'query')
}})),
dispatch(fetchData('/streams', 'annoncesCount', 'GET', {params: { type: 'preview', limit: '0',
search: formValueSelector('searchList')(getState(), 'query')
}})),
]))
@connect((state) => {
    const params = Immutable.fromJS(state.fetchData.users).getIn(['request', 'options', 'params'])
    const search = Immutable.fromJS(state.fetchData.users).getIn(['request', 'options', 'params', 'search'])
    return {
      streamsCount:  state.fetchData.streamsCount.response ,
      newsCount:  state.fetchData.newsCount.response ,
      usersCount:  state.fetchData.usersCount.response ,
      annoncesCount: state.fetchData.annoncesCount.response ,
        modals: state.modals,
        users: state.fetchData.users.response,
        search,
        params: params || {},
    }
}, (dispatch) => ({
    actions: bindActionCreators({
        subscribe,
        unsubscribe,
        getUsers,
        loadData,
        loadDataRedux
    }, dispatch),
    dispatch
}))

export default class UserList extends Component {

    constructor(props) {
        super(props)
        this.state = {
            showResult: props.search !== '' ? true:false
        }
    }

    _showResult = () => {
        this.setState({showResult: true})
    }
    updateData=  (form)=>{
      const {actions, params} = this.props
      return  Promise.all([
        actions.loadDataRedux({url: '/users', key: 'users'},
        {type: params.get('type'),page: 1,  limit: params.get('limit')},form),

        actions.loadDataRedux({url: '/streams', key: 'annoncesCount'},
        {type: 'preview',limit: '0'},form),
        actions.loadDataRedux({url: '/news', key: 'newsCount'},
        {limit: '0'},form),
        actions.loadDataRedux({url: '/streams', key: 'streamsCount'},
        { type: 'web-cast',limit: '0'},form),
        actions.loadDataRedux({url: '/users', key: 'usersCount'},
        { limit: '0'},form),
      ])
    }
    render() {

        const {newsCount,streamsCount,annoncesCount,usersCount, users, actions, search, params, dispatch} = this.props

        return (
            <div className='main-container'>
              <SearchList
                onSubmit={this.updateData}
                onSubmitSuccess={this._showResult}
                initialValues={{query: search}}
              />
              <p className='search__result__head'>Результаты поиска</p>
              <div className='search__result-wrap'>
                <SearchNavigation
                  news={newsCount}
                  streams={streamsCount}
                  annonces={annoncesCount}
                  users={users}
                />
                {this.state.showResult && search
                  ? <span>
                    {users.totalItems === 0
                      ? <p className='search__result__text'>По данному запросу нет ни одного результата.</p>
                      :  <p className='search__result__text'>Найдено записей по запросу «{search}»: {users.totalItems}</p>
                    }
                  </span>
                      : null
                }
                <div className='userinfo'>
                  <div className='search__channel-wrap'>
                    {users ?
                      users.Items.map((user,key)=>{
                        return <User
                          key={key}
                          data={user}
                          actions={actions}
                        />
                      }):
                                null
                    }
                  </div>
                    </div>
                    <MoreResults
                        isRender={users && users.totalItems > 12 && users.totalItems !== users.Items.length}
                        params={params}
                        loadMore={actions.loadData}
                        dispatch={dispatch}
                        url='/users'
                        keyData='users'
                    />
                </div>
            </div>
        )
    }
}
