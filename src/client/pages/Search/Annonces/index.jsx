import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {preload} from 'react-isomorphic-render/redux'
import {connect} from 'react-redux'
import {loadData, loadDataRedux} from 'actions/loadData'
import {watching, unwatching}  from 'actions/Stream/userWatching'
import {getAnnonceAll} from 'actions/Stream/getAnnonceAll'
import {getAnnonceUser} from 'actions/Stream/getAnnonceUser'
import Annonces from 'components/Common/User/AnnonceOther'
import SearchNavigation from 'components/Search/Navigation'
import SearchList from 'components/App/Search/List'
import MoreResults from 'components/Common/Buttons/MoreResults'
import Immutable from 'immutable'
import {formValueSelector} from 'redux-form'

@preload(({dispatch, fetchData, getState }) =>Promise.all([
  dispatch(fetchData('/streams', 'annonces', 'GET', {params: {
    type: 'preview',
    ['order[startsAt]']: 'asc',
    page: 1,
    limit: 8,
    search: formValueSelector('searchList')(getState(), 'query')
}})),
dispatch(fetchData('/streams', 'streamsCount', 'GET', {params: { type: 'web-cast',limit: '0',
search: formValueSelector('searchList')(getState(), 'query')
}})),
dispatch(fetchData('/news', 'newsCount', 'GET', {params: { type: 'web-cast',limit: '0',
search: formValueSelector('searchList')(getState(), 'query')
}})),
dispatch(fetchData('/users', 'usersCount', 'GET', {params: { limit: '0',
search: formValueSelector('searchList')(getState(), 'query')
}})),
dispatch(fetchData('/streams', 'annoncesCount', 'GET', {params: { type: 'preview', limit: '0',
search: formValueSelector('searchList')(getState(), 'query')
}})),
]))

@connect(state => {
    const params = Immutable.fromJS(state.fetchData.annonces).getIn(['request', 'options', 'params'])
    const search = Immutable.fromJS(state.fetchData.annonces).getIn(['request', 'options', 'params', 'search'])
    return {
      streamsCount:  state.fetchData.streamsCount.response ,
      newsCount:  state.fetchData.newsCount.response ,
      usersCount:  state.fetchData.usersCount.response ,
      annoncesCount: state.fetchData.annoncesCount.response ,
        annonces: state.fetchData.annonces.response,
        search,
        params: params || {},
    }
}, (dispatch) => ({
    actions: bindActionCreators({
        loadData,
        loadDataRedux,
        watching,
        unwatching,
        getAnnonceUser,
        getAnnonceAll
    }, dispatch),
    dispatch
}))

export default class SearchAnnonce extends Component {

    constructor(props) {
        super(props)
        this.state = {
            showResult: props.search !== '' ? true:false
        }
    }

    _showResult = () => {
        this.setState({showResult: true})
    }
    updateData=  (form)=>{
      const {actions, params} = this.props
      return Promise.all([
        actions.loadDataRedux({url: '/streams', key: 'annonces'},
        {type: params.get('type'),page: 1,  limit: params.get('limit')},form),

        actions.loadDataRedux({url: '/streams', key: 'annoncesCount'},
        {type: 'preview',limit: '0'},form),
        actions.loadDataRedux({url: '/news', key: 'newsCount'},
        {limit: '0'},form),
        actions.loadDataRedux({url: '/streams', key: 'streamsCount'},
        { type: 'web-cast',limit: '0'},form),
        actions.loadDataRedux({url: '/users', key: 'usersCount'},
        { limit: '0'},form),
      ])
    }
    render() {

        const {newsCount,streamsCount,annoncesCount,usersCount,actions, users, annonces, search, params, dispatch} = this.props

        return (
            <div className='main-container'>
              <SearchList
                onSubmit={this.updateData}
                onSubmitSuccess={this._showResult}
                initialValues={{query: search}}/>
              <p className='search__result__head'>Результаты поиска</p>
              <div className='search__result-wrap'>
                <SearchNavigation
                  news={newsCount}
                  streams={streamsCount}
                  annonces={annoncesCount}
                  users={usersCount}
                />
                {this.state.showResult && search !== ''
                  ? <span>
                    {annonces.totalItems === 0
                      ? <p className='search__result__text'>По данному запросу нет ни одного результата.</p>
                      :  <p className='search__result__text'>Найдено записей по запросу «{search}»: {annonces.totalItems}</p>
                    }
                  </span>
                      : null
                }
                <div className='userinfo'>
                  <div className='mytranslation-wrap'>
                    {annonces ?
                      annonces.Items.map((annonce, key)=>{
                        return <Annonces
                          key={key}
                          user={users}
                          data={annonce}
                          actions={actions}
                          type='SearchAnnonce'
                        />
                      }):
                                null
                    }
                  </div>
                </div>
                <MoreResults
                  isRender={annonces && annonces.totalItems > 8 && annonces.totalItems !== annonces.Items.length}
                  params={params}
                  loadMore={actions.loadData}
                  url='/streams'
                        keyData='annonces'
                        dispatch={dispatch}
                    />
                </div>
            </div>
        )
    }
}
