import React, {Component} from 'react'
import {formValueSelector} from 'redux-form'
import {preload} from 'react-isomorphic-render/redux'

// @preload(({dispatch, fetchData, getState }) =>Promise.all([
//
// dispatch(fetchData('/streams', 'streamsCount', 'GET', {params: { type: 'web-cast',limit: 0,
// search: formValueSelector('searchList')(getState(), 'query')
// }})),
// dispatch(fetchData('/news', 'newsCount', 'GET', {params: { type: 'web-cast',limit: 0,
// search: formValueSelector('searchList')(getState(), 'query')
// }})),
// dispatch(fetchData('/users', 'usersCount', 'GET', {params: { type: 'web-cast', limit: 0,
// search: formValueSelector('searchList')(getState(), 'query')
// }})),
// dispatch(fetchData('/streams', 'annoncesCount', 'GET', {params: { type: 'preview', limit: 0,
// search: formValueSelector('searchList')(getState(), 'query')
// }})),
// ]))
export default class Search extends Component {
    render(){
        return (
            <div className='search'>
              {this.props.children}
            </div>
        )
    }
}
