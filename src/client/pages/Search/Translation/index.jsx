import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {loadData, loadDataRedux} from '../../../actions/loadData'
import {StreamSubscribe,StreamUnsubscribe} from 'actions/Stream/StreamSubscribe'
import {getStreamAll} from 'actions/Stream/getStreamAll'
import MoreResults from 'components/Common/Buttons/MoreResults'
import Translation from 'components/Common/User/StreamSearch'
import SearchNavigation from '../../../components/Search/Navigation'
import SearchList from '../../../components/App/Search/List'
import {fromJS} from 'immutable'
import {formValueSelector} from 'redux-form'
import {preload} from 'react-isomorphic-render/redux'

@preload(({dispatch, fetchData, getState }) =>Promise.all([
  dispatch(fetchData('/streams', 'streams', 'GET', {params: {
    type: 'web-cast',
    ['order[startsAt]']: 'desc',
    page: 1,
    limit: 8,
    search: formValueSelector('searchList')(getState(), 'query')
}})),
dispatch(fetchData('/streams', 'streamsCount', 'GET', {params: { type: 'web-cast',limit: '0',
search: formValueSelector('searchList')(getState(), 'query')
}})),
dispatch(fetchData('/news', 'newsCount', 'GET', {params: { type: 'web-cast',limit: '0',
search: formValueSelector('searchList')(getState(), 'query')
}})),
dispatch(fetchData('/users', 'usersCount', 'GET', {params: { type: 'web-cast', limit: '0',
search: formValueSelector('searchList')(getState(), 'query')
}})),
dispatch(fetchData('/streams', 'annoncesCount', 'GET', {params: { type: 'preview', limit: '0',
search: formValueSelector('searchList')(getState(), 'query')
}})),
]))

@connect((state) => {
  const params = fromJS(state.fetchData.streams).getIn(['request', 'options', 'params'])
  const search = fromJS(state.fetchData.streams).getIn(['request', 'options', 'params', 'search'])

  return {
    streamsCount:  state.fetchData.streamsCount.response ,
    newsCount:  state.fetchData.newsCount.response ,
    usersCount:  state.fetchData.usersCount.response ,
    annoncesCount: state.fetchData.annoncesCount.response ,
        modals: state.modals,
        streams: state.fetchData.streams.response,
        params: params || {},
        search,
  }
}, (dispatch) => ({
    actions: bindActionCreators({
        loadData,
        loadDataRedux,
        StreamSubscribe,
        StreamUnsubscribe,
        getStreamAll
    }, dispatch),
    dispatch
}))

export default class SearchTranslation extends Component {

    constructor(props) {
        super(props)
        this.state = {
            showResult: props.search !== '' ? true:false
        }
    }

    _showResult = () => {
        this.setState({showResult: true})
    }
updateData=  (form)=>{
  const {actions, params} = this.props
  return Promise.all([
    actions.loadDataRedux({url: '/streams', key: 'streams'},
    {type: params.get('type'),page: 1,  limit: params.get('limit')},form),

    actions.loadDataRedux({url: '/streams', key: 'annoncesCount'},
    {type: 'preview',limit: '0'},form),
    actions.loadDataRedux({url: '/news', key: 'newsCount'},
    {limit: '0'},form),
    actions.loadDataRedux({url: '/streams', key: 'streamsCount'},
    { type: 'web-cast',limit: '0'},form),
    actions.loadDataRedux({url: '/users', key: 'usersCount'},
    { limit: '0'},form),
  ])

  // actions.loadDataRedux.bind(
  //         null,
  //   {url: '/streams', key: 'streams'},
  //   {
  //     type: params.get('type'),
  //             page: 1,
  //     limit: params.get('limit')
  //   }
  // )
}
    render() {

        const {streamsCount,newsCount,usersCount,annoncesCount,actions, streams, modals, search, params, dispatch} = this.props

        return (
            <div className='main-container'>
              <SearchList
                onSubmit={this.updateData}
                onSubmitSuccess={this._showResult}
                initialValues={{query: search}}
              />
              <p className='search__result__head'>Результаты поиска</p>
              <div className='search__result-wrap'>
                <SearchNavigation
                  streams={streamsCount}
                  news={newsCount}
                  annonces={annoncesCount}
                  users={usersCount}
                />
                {this.state.showResult && search !== ''
                  ? <span>
                    {streams.totalItems === 0
                      ? <p className='search__result__text'>По данному запросу нет ни одного результата.</p>
                      :  <p className='search__result__text'>Найдено записей по запросу «{search}»: {streams.totalItems}</p>
                    }
                  </span>
                      : null
                }
                <div className='userinfo'>
                  <div className='mytranslation-wrap'>
                    {streams ?
                      streams.Items.map((stream, key)=>{
                        return <Translation
                          key={key}
                          data={stream}
                          modals={modals}
                          actions={actions}
                        />
                      }):
                                null
                    }
                  </div>
                </div>
                <MoreResults
                        isRender={streams && streams.totalItems > 8 && streams.totalItems !== streams.Items.length}
                        params={params}
                        loadMore={actions.loadData}
                        url='/streams'
                        keyData='streams'
                        dispatch={dispatch}
                    />
                </div>
            </div>
        )
    }
}
