import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {preload} from 'react-isomorphic-render/redux'
import {connect} from 'react-redux'
import SearchNavigation from 'components/Search/Navigation'
import SearchList from 'components/App/Search/List'
import News from 'components/Common/User/NewsSearch'
import MoreResults from 'components/Common/Buttons/MoreResults'
import {loadData, loadDataRedux} from 'actions/loadData'
import Immutable from 'immutable'
import {formValueSelector} from 'redux-form'

// @preload(({dispatch, fetchData, getState }) =>
// dispatch(fetchData('/news', 'news', 'GET', {params: {
//   ['order[createdAt]']: 'desc',
//   page: 1,
//   limit: 8,
//   search: formValueSelector('searchList')(getState(), 'query')
// }}))
// )

@preload(({dispatch, fetchData, getState }) =>Promise.all([
  dispatch(fetchData('/news', 'news', 'GET', {params: {
    ['order[createdAt]']: 'desc',
    page: 1,
    limit: 8,
    search: formValueSelector('searchList')(getState(), 'query')
  }})),
dispatch(fetchData('/streams', 'streamsCount', 'GET', {params: { type: 'web-cast',limit: '0',
search: formValueSelector('searchList')(getState(), 'query')
}})),
dispatch(fetchData('/news', 'newsCount', 'GET', {params: { type: 'web-cast',limit: '0',
search: formValueSelector('searchList')(getState(), 'query')
}})),
dispatch(fetchData('/users', 'usersCount', 'GET', {params: { type: 'web-cast', limit: '0',
search: formValueSelector('searchList')(getState(), 'query')
}})),
dispatch(fetchData('/streams', 'annoncesCount', 'GET', {params: { type: 'preview', limit: '0',
search: formValueSelector('searchList')(getState(), 'query')
}})),
]))

@connect((state) => {
    const params = Immutable.fromJS(state.fetchData.news).getIn(['request', 'options', 'params'])
    const search = Immutable.fromJS(state.fetchData.news).getIn(['request', 'options', 'params', 'search'])
    return {
      streamsCount:  state.fetchData.streamsCount.response ,
      usersCount:  state.fetchData.usersCount.response ,
      annoncesCount: state.fetchData.annoncesCount.response ,
      newsCount: state.fetchData.newsCount.response ,
        news: state.fetchData.news.response,
        search,
        params: params || {},
    }
}, (dispatch) => ({
    actions: bindActionCreators({
      loadData,
      loadDataRedux,
    }, dispatch),
    dispatch
}))

export default class SearchNews extends Component {
  constructor(props) {
      super(props)
      this.state = {
          showResult: props.search !== '' ? true:false
      }
  }

  _showResult = () => {
      this.setState({showResult: true})
  }
  updateData= (form)=>{
    const {actions, params} = this.props
    return  Promise.all([
      actions.loadDataRedux({url: '/news', key: 'news'},
      {type: params.get('type'),page: 1,  limit: params.get('limit')},form),

      actions.loadDataRedux({url: '/streams', key: 'annoncesCount'},
      {type: 'preview',limit: '0'},form),
      actions.loadDataRedux({url: '/news', key: 'newsCount'},
      {limit: '0'},form),
      actions.loadDataRedux({url: '/streams', key: 'streamsCount'},
      { type: 'web-cast',limit: '0'},form),
      actions.loadDataRedux({url: '/users', key: 'usersCount'},
      { limit: '0'},form),
    ])
  }
    render() {

        const {newsCount,streamsCount,annoncesCount,usersCount,actions, news, search, params, dispatch} = this.props

        return (
            <div className='main-container'>
              <SearchList
                onSubmit={this.updateData}
                onSubmitSuccess={this._showResult}
                initialValues={{query: search}}/>
              <p className='search__result__head'>Результаты поиска</p>
              <div className='search__result-wrap'>
                <SearchNavigation
                  news={newsCount}
                  streams={streamsCount}
                  annonces={annoncesCount}
                  users={usersCount}
                />
                {this.state.showResult && search !== ''
                  ? <span>
                    {news.totalItems === 0
                      ? <p className='search__result__text'>По данному запросу нет ни одного результата.</p>
                      :  <p className='search__result__text'>Найдено записей по запросу «{search}»: {news.totalItems}</p>
                    }
                  </span>
                      : null
                }
                <div className='userinfo'>
                  <div className='mytranslation-wrap'>
                    <div className='search__news-wrap'>
                      {Object.keys(news).length != 0 ?
                        news.Items.map((news, key)=>{
                          return <News
                            key={key}
                            data={news}
                          />
                        }):
                                  null
                              }
                            </div>
                        </div>
                    </div>
                    <MoreResults
                        isRender={news && news.totalItems > 8 && news.totalItems !== news.Items.length}
                        params={params}
                        loadMore={actions.loadData}
                        url='/news'
                        keyData='news'
                        dispatch={dispatch}
                    />
                </div>
            </div>
        )
    }
}
