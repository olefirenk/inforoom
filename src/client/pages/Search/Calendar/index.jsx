import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {preload} from 'react-isomorphic-render/redux'
import {connect} from 'react-redux'
import DayPicker from 'react-day-picker'
import moment from 'moment'
import * as calendarActions from '../../../actions/Calendar/actionsCalendar'
import StreamsList from '../../../components/User/StreamsList'
// import {loadStreamsForDay,loadCalendarData} from '../../../actions/Calendar/actionsCalendar'

@preload(({dispatch, fetchData, getState ,parameters}) =>Promise.all([
  dispatch(fetchData('/streams/date', 'streamsByChosenMonth', 'GET', {params: {
     ['startsAt[after]']: encodeURIComponent(moment().startOf('month').format()),
     ['startsAt[before]']: encodeURIComponent(moment().endOf('month').format()),
  }})),
  dispatch(fetchData('/streams', 'streamsCalendar', 'GET', {params: {
      type: 'web-cast',
       ['startsAt[after]']: encodeURIComponent(moment().startOf('day').format()),
       ['startsAt[before]']: encodeURIComponent(moment().endOf('day').format()),
      ['order[startsAt]']: 'asc',
      page:1,
      limit:10,
  }}))
]))

@connect(state => ({
// streamsCalendar:state.fetchData.streamsCalendar.response,
streamsByChosenMonth:state.fetchData.streamsByChosenMonth.response.Items,
}),
(dispatch) => ({
    actions: bindActionCreators(
      calendarActions
    , dispatch)
})
)

export default class Calendar extends Component {
    constructor(props) {
        super(props)
        this.state={
        selectedDay: new Date(),
        day: moment()
      }
    }

      componentWillReceiveProps= (next)=>{
        console.log('componentWillReceiveProps works')
      }

      handleDayClick= (day, { disabled, selected })=> {
    //     console.log(day,disabled,selected)
    // if (selected) {
    //   this.loadStreamsForDay(day)
    //   this.setState({
    //     selectedDay: selected ? null : day,
    //     day
    //   })
    // }
    this.loadStreamsForDay(day)
    this.setState({
      selectedDay: selected ? null : day,
      day
    })

  }

  loadStreamsForDay= (day)=>{
        this.props.actions.loadStreamsForDay(day)
  }
  monthChangeHandler =  (month)=>{
    this.props.actions.loadCalendarData(month)
  }

    render() {
        const {
          actions,
          users,
          annonces,
          search,
          params,
          dispatch,
          // streamsCalendar:{Items},
          streamsByChosenMonth,
        } = this.props

        const {
          day,
        } =this.state

        let selectedDays=streamsByChosenMonth.map(item=>new Date(item))
        return (
          <div className="head-container">
            <div className='calendar'>
              <div className='calendar__sidebar'>
                <DayPicker
                  initialMonth={ new Date(moment()) }
                  selectedDays={ selectedDays }
                  onDayClick={ this.handleDayClick }
                  onMonthChange={this.monthChangeHandler}
                />
              </div>
              <StreamsList
                day={day}
              />
            </div>
          </div>
            )
    }
}
