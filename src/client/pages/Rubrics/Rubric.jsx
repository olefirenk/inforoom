import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {preload} from 'react-isomorphic-render/redux'
import {connect} from 'react-redux'
import News from 'components/Common/User/NewsSearch'
import Translation from '../../components/Common/User/StreamOther'
import  {getStreamByRubric} from '../../actions/Stream/getStreamByRubric'
import  {getNewsByRubric} from '../../actions/News/getNewsByRubric'
import RubricsNews from '../../components/Sidebar/RubricsNews'

@preload(({dispatch, fetchData , parameters}) =>Promise.all([

dispatch(fetchData('/streams', 'streamsByRubric', 'GET', {params: {
    type: 'web-cast',
    ['order[startsAt]']: 'desc',
    rubric: parameters.id,
}})),

dispatch(fetchData('/news', 'newsByRubric', 'GET', {params: {
  ['order[createdAt]']: 'desc',
  page: 1,
  limit: 10,
rubric: parameters.id
}}))]))
@connect(state => ({
    streamsByRubric: state.fetchData.streamsByRubric && state.fetchData.streamsByRubric.response,
    newsByRubric: state.fetchData.newsByRubric && state.fetchData.newsByRubric.response,
}), (dispatch) => ({
    actions: bindActionCreators({
    }, dispatch)
}))

export default class Rubric extends Component {

    render() {
        const {streamsByRubric:{Items},actions,newsByRubric} = this.props
          return (
          <div className='search__news-wrap search__news-wrap2'>
            <div className='userinfo' style={{display:'flex', justifyContent:'space-between'}}>
              <div className='mytranslation-wrap'>
                {Items && Items.length &&
                  Items.map((stream, key)=>{
                    return(
                      <Translation
                        key = {key}
                        idUser = {stream.createdBy.id}
                        actions = {actions}
                        data = {stream}
                      />
                    )
                  })
                }
              </div>
              <div className="block1">
                <RubricsNews news={newsByRubric} type='no-title'/>
              </div>
            </div>
        </div>

        )
    }
}
