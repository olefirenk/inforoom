import React, {Component} from 'react'

export default class Rubrics extends Component {
    render() {
        return (
            <div className='head-container'>
              <div className='newsid-main'>
                <div className='newsid-wrap newsid-wrap2'>
                  {this.props.children}
                </div>
              </div>
            </div>
        )
    }
}
