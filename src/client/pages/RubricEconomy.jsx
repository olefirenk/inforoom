import React, {Component} from 'react'
import {preload} from 'react-isomorphic-render/redux'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'

import {getAnnonceSearch} from 'actions/Stream/getAnnonceSearch'
import {watching, unwatching}  from 'actions/Stream/userWatching'
import {getAnnonceAll} from 'actions/Stream/getAnnonceAll'
import {getAnnonceUser} from 'actions/Stream/getAnnonceUser'
import News from 'components/Sidebar/News'
import MainAnnonces from 'components/Rubrics/Economy'

@preload(({dispatch, fetchData }) => Promise.all([
    dispatch(fetchData('/streams?type=preview', 'annonces')),
    dispatch(fetchData('/users', 'users')),
    dispatch(fetchData('/news', 'news', 'GET'))
]))

@connect((state) => ({
    annonces: state.fetchData.annonces.response,
    form: state.form,
    users: state.fetchData.users.response,
    news: state.fetchData.news.response || {}
    }), (dispatch) => ({
    actions: bindActionCreators({
        getAnnonceSearch,
        watching,
        unwatching,
        getAnnonceUser,
        getAnnonceAll
    }, dispatch)
}))

export default class Economy extends Component {

    render() {
        const {actions, users, annonces, news} = this.props
        return (
            <div>
                <div className='head-container'>
                    <div className='block1'>
                        <News news={news}/>
                    </div>
                    <MainAnnonces
                        users={users}
                        annonces={annonces}
                        actions={actions}
                        />
                </div>
            </div>
        )
    }
}
