import React, {Component} from 'react'
import {Link} from 'react-router'
import StreamPlayer from 'components/Common/StreamPlayer'

export default class PopularComments extends Component {
    render() {
        const {streams} = this.props
        return (
            <div className='popular'>
                <h2>
                    <i><img src={require('images/icon/icon-comments2.png')}/></i>
                    Самые комментируемые</h2>
                <div className='popular-wrap'>
                    <div className='populars'>
                        {streams
                            ? streams.Items.map((item,key) =>{
                                return (
                                    <div key={key} className='popular'>
                                        <div className='popular-video'>
                                            <StreamPlayer key={item.id} id={item.id} url={item.streamUrl} streams={item}/>
                                        </div>
                                        <div className='popular-describe'>
                                            <Link to={`stream/${item.id}`} className='about'>{item.title}</Link>
                                            <p>
                                                <Link to={`/id${item.createdBy.id}`}>
                                                    <span className='author'>{item.createdBy.fullName}</span>
                                                </Link>
                                                <span className='comments'>
                                                    <i><img src={require('images/icon/icon-comments.png')}/></i>
                                                    {item.thread.countComments}
                                                </span>
                                            </p>
                                        </div>
                                    </div>
                                )
                            })
                            : null
                        }
                    </div>
                </div>
            </div>
        )
    }
}
