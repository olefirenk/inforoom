import React, {Component} from 'react'
import {goto} from 'react-isomorphic-render/redux'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {onModal, offModal} from 'actions/App/modals'
import {getAll as getRubrics} from 'actions/Rubric/getAll'
import {create} from 'actions/Stream/create'
import {loadPhoto} from 'actions/Photos/loadPhoto'
import {getStreamUser} from 'actions/Stream/getStreamUser'
import {getAnnonceUser} from 'actions/Stream/getAnnonceUser'
import HotLive from 'components/Stream/HotLive/Create'
import Annonce from 'components/Stream/Annonce/Create'

@connect((state) => ({user: state.authentication.user,
                      modals: state.modals,
                      rubrics: state.fetchData.rubrics.response || {}
}), (dispatch) => ({
    actions: bindActionCreators({
        create,
        onModal,
        offModal,
        getRubrics,
        loadPhoto,
        goto,
        getStreamUser,
        getAnnonceUser
    }, dispatch)
}))

export default class StreamForm extends Component {

    _SubmitSuccessHotLive = () => {
        this.props.actions.onModal('streamLive')
        this.props.actions.offModal('stream')
    }

    _SubmitSuccessAnnonce = () => {
        this.props.actions.offModal('stream')
        this.props.actions.onModal('streamApproved')
    }

    render() {
        const {off, actions, rubrics} = this.props

        return (
            <div className='start-translation'>
                <div className='icon__close' onClick={() => {
                    off('shadowblock'),
                    off('stream')
                }}>
                    <i className='sprite sprite-close'></i>
                </div>
                <p className='start-translation__head'>Выберите тип трансляции</p>
                <div className='start-translation-wrap'>
                    <HotLive
                        rubrics={rubrics}
                        initialValues={{
                            title: '',
                            description: '',
                            startsAt: '',
                            rubric: rubrics ? `/api/rubrics/${rubrics.Items[0].id}`:null
                        }}
                        onSubmit={actions.create}
                        actions={actions}
                        onSubmitSuccess={this._SubmitSuccessHotLive}/>
                    <Annonce
                        loadPhoto={actions.loadPhoto}
                        rubrics={rubrics}
                        initialValues={{
                            title: '',
                            description: '',
                            rubric: rubrics ? `/api/rubrics/${rubrics.Items[0].id}`:null,
                            startsAt: ''
                        }}
                        onSubmit={actions.create}
                        onSubmitSuccess={this._SubmitSuccessAnnonce}/>
                </div>
            </div>
        )
    }
}
