import React, {Component} from 'react'
import Navigation from 'components/User/Navigation'
import Balance from 'components/User/Balance'

export default class User extends Component {

    render() {
        return (
            <div className='main-container userpage-flex'>
                <Navigation/>
                <div className='userprofile-wrap'>
                    <Balance/> {this.props.children}
                </div>
            </div>
        )
    }
}
