import React, { Component } from 'react'
import {bindActionCreators} from 'redux'
import {preload} from 'react-isomorphic-render/redux'
import {connect} from 'react-redux'

import {onModal, offModal} from 'actions/App/modals'
import {getAll as getRubrics} from 'actions/Rubric/getAll'
import Translation from 'components/Common/Translation'
import {edit} from 'actions/Stream/edit'
import {getStreamUser} from 'actions/Stream/getStreamUser'

@preload(({dispatch, getState, fetchData}) => dispatch(fetchData(`/streams?type=web-cast&createdBy=${getState().authentication.user.id}`, 'streams')))

@connect((state) => ({
    user: state.authentication.user,
    modals: state.modals,
    rubrics: state.fetchData.rubrics,
    streams: state.fetchData.streams.response
}), (dispatch) => ({
    actions: bindActionCreators({
        edit,
        getRubrics,
        getStreamUser,
        onModal,
        offModal
    }, dispatch)
}))

export default class UserTranslation extends Component {

    componentDidMount() {
        this.props.actions.getRubrics()
    }

    render() {

        const {streams, actions, modals, user} = this.props

        let rubrics
        if (this.props.rubrics) {
            rubrics = this.props.rubrics.response
        }

        return (
            <div className='userinfo'>
              <div className='triangle triangle-translation'></div>
              <div className='mytranslation-wrap'>
                {streams
                  ? streams.Items.map((stream,key) => {
                    return <Translation
                      key={key}
                      rubrics={rubrics}
                      modals={modals}
                      actions={actions}
                      data={stream}
                      idUser={user.id}
                      off={actions.offModal}
                      on={actions.onModal}/>
                  })
                        : null
                    }
                </div>
            </div>
        )
    }
}
