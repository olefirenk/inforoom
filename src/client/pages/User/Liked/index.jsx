import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {preload} from 'react-isomorphic-render/redux'
import {connect} from 'react-redux'
import {getStreamAll} from 'actions/Stream/getStreamAll'
import {StreamSubscribe,StreamUnsubscribe} from 'actions/Stream/StreamSubscribe'
import {StreamLiked} from 'actions/Stream/StreamLiked'
import {getStreamLiked} from 'actions/Stream/getStreamLiked'
import Translation from 'components/Common/User/StreamSearch'


@preload(({dispatch,getState, fetchData}) => dispatch(fetchData('/streams', 'streamLiked', 'GET', {params: {
    type: 'web-cast',
    ['order[startsAt]']: 'asc',
    followedUsers: `${getState().authentication.user.id}`
}})))

@connect((state) => ({
                    modals: state.modals,
                    streamLiked: state.fetchData.streamLiked.response,
                    user: state.authentication.user
                }), (dispatch) => ({
    actions: bindActionCreators({
        getStreamAll,
        getStreamLiked,
        StreamLiked,
        StreamSubscribe,
        StreamUnsubscribe
    }, dispatch)
}))


export default class UserLiked extends Component {

    render() {
      const {streamLiked, modals , actions, user} = this.props

        return (
            <div className='userinfo'>
                <div className='triangle triangle-liked'></div>
                <div className='userlikedinfo-wrap'>
                    {streamLiked
                        ? streamLiked.Items.map((stream) => {
                            return < Translation
                                        key={stream.id}
                                        data={stream}
                                        actions={actions}
                                        modals={modals}
                                        idUser={user.id}
                                        type='button-delete'
                                        />
                                    })
                        : null
                    }
                </div>
            </div>
        )
    }
}
