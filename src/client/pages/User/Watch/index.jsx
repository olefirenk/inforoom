import React from 'react'
import {bindActionCreators} from 'redux'
import {preload} from 'react-isomorphic-render/redux'
import {connect} from 'react-redux'
import Annonce from 'components/Common/Annonce'

@preload(({dispatch, getState, fetchData}) => Promise.all([
    dispatch(fetchData('/streams', 'annoncesLiked', 'GET', {params: {
        type: 'preview',
        followedUsers: `${getState().authentication.user.id}`
    }})),
    dispatch(fetchData('/rubrics', 'rubrics'))
]))

@connect((state) => ({
  modals: state.modals,
  rubrics: state.fetchData.rubrics.response,
  annoncesLiked: state.fetchData.annoncesLiked.response,
  annonceForm: state.form.AnnonceEdit,
  user: state.authentication.user
}), (dispatch) => ({
    actions: bindActionCreators({
    }, dispatch)
}))

export default class UserWatch extends React.Component {

    render() {
        const {annoncesLiked, actions, modals, rubrics, annonceForm, user} = this.props
        return (
            <div className='userinfo'>
                <div className='triangle triangle-watch'></div>
                <div className='myannonce-wrap'>
                  {annoncesLiked
                      ? annoncesLiked.Items.map((annonce, key) => {
                   return <Annonce type='edit'
                            key={key}
                            idUser={user.id}
                            data={annonce}
                            annonceForm={annonceForm}
                            on={actions.onModal}
                            actions={actions}
                            off={actions.offModal}
                            rubrics={rubrics}
                            off={actions.offModal}
                            modals={modals}/>
                        })
                        :null
                      }
                </div>
            </div>
        )
    }
}
