import React, { Component } from 'react'
import {bindActionCreators} from 'redux'
import {preload} from 'react-isomorphic-render/redux'
import {connect} from 'react-redux'
import {onModal, offModal} from 'actions/App/modals'
import {getAll as getRubrics} from 'actions/Rubric/getAll'
import {edit} from 'actions/Stream/edit'
import {getStreamUser} from 'actions/Stream/getStreamUser'
import {subscribe, unsubscribe}  from 'actions/User/userSubscribe'
import {getUser} from 'actions/User/getUser'
import {getAll as getUsers} from 'actions/User/getAll'
import {usersSubscribe} from 'actions/User/usersSubscribe'
import {StreamSubscribe,StreamUnsubscribe} from 'actions/Stream/StreamSubscribe'
import {getStreamAll} from 'actions/Stream/getStreamAll'

import SubscribeUser from 'components/Common/SubscribeUser'

@preload(({dispatch, fetchData}) => Promise.all([
    dispatch(fetchData('/streams?type=web-cast', 'streams')),
    dispatch(fetchData('/users?followerUsers=true', 'users')),
    dispatch(fetchData('/rubrics', 'rubrics')),
]))

@connect((state) => ({
    users: state.fetchData.users.response,
    modals: state.modals,
    rubrics: state.fetchData.rubrics.response,
    streams: state.fetchData.streams.response
}), (dispatch) => ({
    actions: bindActionCreators({
        edit,
        getRubrics,
        getStreamUser,
        StreamSubscribe,
        StreamUnsubscribe,
        getStreamAll,
        subscribe,
        unsubscribe,
        usersSubscribe,
        onModal,
        getUser,
        getUsers,
        offModal
    }, dispatch)
}))

export default class UserSubscribe extends Component {

    render() {

        const {actions, streams, users} = this.props

        return (
            <div className='userinfo'>
                <div className='triangle triangle-subscribe'></div>
                <div className='mysubscribe-wrap'>
                    {users
                        ? users.Items.map((user, key) => {
                            return <SubscribeUser
                                        key = {key}
                                        data = {user}
                                        actions = {actions}
                                        streams = {streams}
                                    />
                        })
                        :null
                    }
                </div>
            </div>
        )
    }
}
