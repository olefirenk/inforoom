import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {preload} from 'react-isomorphic-render/redux'
import {connect} from 'react-redux'
import {onModal, offModal} from 'actions/App/modals'
import {getAll as getRubrics} from 'actions/Rubric/getAll'
import {edit} from 'actions/Stream/edit'
import {getAnnonceUser} from 'actions/Stream/getAnnonceUser'
import {loadPhoto} from 'actions/Photos/loadPhoto'
import Annonce from 'components/Common/Annonce'

@preload(({dispatch, getState, fetchData }) => Promise.all([
    dispatch(fetchData(`/streams?type=preview&createdBy=${getState().authentication.user.id}`, 'annonceSearch')),
    dispatch(fetchData('/rubrics', 'rubrics'))
]))

@connect((state) => ({
  modals: state.modals,
  rubrics: state.fetchData.rubrics.response,
  annonceSearch: state.fetchData.annonceSearch.response,
  annonceForm: state.form.AnnonceEdit,
  user: state.authentication.user
}), (dispatch) => ({
    actions: bindActionCreators({
        getRubrics,
        onModal,
        offModal,
        edit,
        getAnnonceUser,
        loadPhoto
    }, dispatch)
}))

export default class UserAnnonce extends Component {

    render() {
        const {annonceSearch, actions, modals, rubrics, annonceForm, user} = this.props
        return (
            <div className='userinfo'>
              <div className='triangle triangle-annonce'></div>
              <div className='myannonce-wrap'>
                {annonceSearch
                  ? annonceSearch.Items.map((annonce, key) => {
                    return <Annonce
                        key={key}
                        idUser={user.id}
                        data={annonce}
                        annonceForm={annonceForm}
                        on={actions.onModal}
                        actions={actions}
                        off={actions.offModal}
                        rubrics={rubrics}
                        off={actions.offModal}
                        modals={modals}/>
                    })
                    :null
                  }
                </div>
            </div>
        )
    }
}
