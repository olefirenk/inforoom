import React from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {edit} from 'actions/User/edit'
import {loadPhoto} from 'actions/Photos/loadPhoto.js'
import {getAccount} from 'actions/Security/Security'
import UserForm from 'components/User/Form/Edit'
import moment from 'moment'

@connect((state) => ({user: state.authentication.user}), (dispatch) => ({
    actions: bindActionCreators({
        edit,
        loadPhoto,
        getAccount
    }, dispatch)
}))

export default class UserProfile extends React.Component {

    render() {
        const {user, actions} = this.props

        return (<UserForm user={user} initialValues={{
            ...user,
            about: user.about ? user.about: '',
            avatarPhoto: user.avatarPhoto ? `/api/images/${user.avatarPhoto.id}`:null,
            birthday: user.birthday ? moment(user.birthday).locale('ru').format('DD/MM/YYYY'):null,
            phones: (user.phones.length > 0) ? user.phones:[null]
        }} actions={actions} onSubmit={actions.edit.bind(null, user.id)} onSubmitSuccess={actions.getAccount}/>)
    }
}
