import React, {Component} from 'react'
import {preload} from 'react-isomorphic-render/redux'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'

import News from 'components/Sidebar/News'
import MainNewsId from 'components/NewsId'

@preload( ({dispatch, parameters, fetchData }) => Promise.all([
    dispatch(fetchData(`/news/${parameters.id}`, 'newsid')),
    dispatch(fetchData('/news', 'news', 'GET'))
]))

@connect((state) => ({
    newsid: state.fetchData.newsid.response,
    news: state.fetchData.news.response,
    }), (dispatch) => ({
    actions: bindActionCreators({

    }, dispatch)
}))

export default class Newsid extends Component {

    render() {
        const {news, newsid} =this.props
        return (
            <div className='head-container'>
              <div className='newsid-main'>
                <h2>Новости</h2>
                <div className='newsid-wrap'>
                  <MainNewsId data={newsid}/>
                  <div className='block1'>
                    <News news={news} type='no-title'/>
                  </div>
                </div>
              </div>
            </div>
        )
    }
}
